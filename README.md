# README #

For the background to the purpose of this non v4l compliant 4.3.6 based kernel and it's advantages and limitations, see this discussion thread:

http://rickcaylor.websitetoolbox.com/post/dragon-flames-not-tails-nor-tales-7890469?highlight=pendragon+drivers&trail=15

This is the v4l-pendragon tree with the addition of driver support for the Technotrend S2 6400 "full featured" card, using pendragon's stv0900 driver instead of the stv090x one.

It has been tested using only one tuner and although both tuners should work, there are no guarantees.

Other saa716x based cards using the stv090x driver will require modification to saa716x_budget.c - look at saa716x_ff_main.c, functions: 

saa716x_s26400_frontend_attach() and stv0900_config tt6400_stv0900_config()

for a place to start.

Discussion around the modifications made to saa716x_ff_main.c can be found here:

http://rickcaylor.websitetoolbox.com/post/v4lpendragon-8493821?&trail=15

Many thanks to pendragon for his driver and advice on modifying the saa716x_ff driver, Major Tom for adding pendragon's driver changes and Midwestmac for getting this repo up. 


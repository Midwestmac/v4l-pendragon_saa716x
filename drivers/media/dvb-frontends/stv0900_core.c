/*
 * stv0900_core.c
 *
 * Driver for ST STV0900 satellite demodulator IC.
 *
 * Copyright (C) ST Microelectronics.
 * Copyright (C) 2009 NetUP Inc.
 * Copyright (C) 2009 Igor M. Liplianin <liplianin@netup.ru>
 * Copyright (C) 2009-2016 Pendragon Sound
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <linux/module.h>
#include <linux/string.h>
#include <linux/slab.h>
#include <linux/i2c.h>

#include "stv0900.h"
#include "stv0900_reg.h"
#include "stv0900_priv.h"
#include "stv0900_init.h"

int stvdebug = 1;
module_param_named(debug, stvdebug, int, 0644);

/* internal params node */
struct stv0900_inode {
	/* pointer for internal params, one for each pair of demods */
	struct stv0900_chip		*chip;
	struct stv0900_inode	*next;
};

/* first internal params */
static struct stv0900_inode *stv0900_first_inode = 0;

/* find chip by i2c adapter and i2c address */
static struct stv0900_inode *find_inode(struct i2c_adapter *i2c, u8 i2c_addr)
{
	struct stv0900_inode *inode = stv0900_first_inode;

	if (inode) {
		/* Search of the last stv0900 chip or find it by i2c adapter and i2c address */
		while (inode && (inode->chip->i2c != i2c || inode->chip->i2c_addr != i2c_addr))
			inode = inode->next;
	}

	return inode;
}

/* deallocating chip */
static void remove_inode(struct stv0900_chip *chip)
{
	struct stv0900_inode *prev_node = stv0900_first_inode;
	struct stv0900_inode *del_node  = find_inode(chip->i2c, chip->i2c_addr);

	if (del_node) {
		if (del_node == stv0900_first_inode)
			stv0900_first_inode = del_node->next;
		else {
			while (prev_node->next != del_node)
				prev_node = prev_node->next;

			if (!del_node->next)
				prev_node->next = 0;
			else
				prev_node->next = prev_node->next->next;
		}

		kfree(del_node);
	}
}

/* allocating new chip */
static struct stv0900_inode *append_chip(struct stv0900_chip *chip)
{
	struct stv0900_inode *new_node = stv0900_first_inode;

	if (!new_node) {
		new_node = kmalloc(sizeof(struct stv0900_inode), GFP_KERNEL);
		stv0900_first_inode = new_node;
	} else {
		while (new_node->next)
			new_node = new_node->next;

		new_node->next = kmalloc(sizeof(struct stv0900_inode), GFP_KERNEL);
		
		if (new_node->next)
			new_node = new_node->next;
		else
			new_node = 0;
	}

	if (new_node) {
		new_node->chip = chip;
		new_node->next = 0;
	}

	return new_node;
}

s32 ge2comp(s32 a, s32 width)
{
	if (width == 32)
		return a;
	else
		return (a >= (1 << (width - 1))) ? (a - (1 << width)) : a;
}

void stv0900_write_reg(struct stv0900_demod *demod, u16 reg, u8 data)
{
	u8 buf[3];
	int ret;
	struct i2c_msg i2cmsg = {
		.addr  = demod->i2c_addr,
		.flags = 0,
		.len   = 3,
		.buf   = buf,
	};

	buf[0] = MSB(reg);
	buf[1] = LSB(reg);
	buf[2] = data;

	ret = i2c_transfer(demod->i2c, &i2cmsg, 1);
	if (ret != 1)
		dprintk("%s: i2c error %d\n", __func__, ret);
}

void stv0900_write_regs(struct stv0900_demod *demod, u16 reg, u8 *data, u8 len)
{
	u8 buf[64];
	int ret;
	struct i2c_msg i2cmsg = {
		.addr  = demod->i2c_addr,
		.flags = 0,
		.len   = len + 2,
		.buf   = buf,
	};
	
	buf[0] = MSB(reg);
	buf[1] = LSB(reg);
	memcpy(buf + 2, data, len);
	
	ret = i2c_transfer(demod->i2c, &i2cmsg, 1);
	if (ret != 1)
		dprintk("%s: i2c error %d\n", __func__, ret);
}

void stv0900_write_be(struct stv0900_demod *demod, u16 reg, u32 data, u8 len)
{
	int i;
	u8 buf[4];
	
	for (i = len - 1; i >= 0; i--) {
		buf[i] = data & 0xff;
		data >>= 8;
	}

	stv0900_write_regs(demod, reg, buf, len);
}

void stv0900_write_be64(struct stv0900_demod *demod, u16 reg, u64 data, u8 len)
{
	int i;
	u8 buf[8];
	
	for (i = len - 1; i >= 0; i--) {
		buf[i] = data & 0xff;
		data >>= 8;
	}
	
	stv0900_write_regs(demod, reg, buf, len);
}

u8 stv0900_read_reg(struct stv0900_demod *demod, u16 reg)
{
	int ret;
	u8 b0[] = { MSB(reg), LSB(reg) };
	u8 buf = 0;
	struct i2c_msg msg[] = {
		{
			.addr  = demod->i2c_addr,
			.flags = 0,
			.buf   = b0,
			.len   = 2,
		}, {
			.addr  = demod->i2c_addr,
			.flags = I2C_M_RD,
			.buf   = &buf,
			.len   = 1,
		},
	};

	ret = i2c_transfer(demod->i2c, msg, 2);
	if (ret != 2)
		dprintk("%s: i2c error %d, reg[0x%02x]\n", __func__, ret, reg);

	return buf;
}

void stv0900_read_regs(struct stv0900_demod *demod, u16 reg, u8 *data, u8 len)
{
	int ret;
	u8 b0[] = { MSB(reg), LSB(reg) };
	struct i2c_msg msg[] = {
		{
			.addr  = demod->i2c_addr,
			.flags = 0,
			.buf   = b0,
			.len   = 2,
		}, {
			.addr  = demod->i2c_addr,
			.flags = I2C_M_RD,
			.buf   = data,
			.len   = len,
		},
	};
	
	ret = i2c_transfer(demod->i2c, msg, 2);
	if (ret != 2)
		dprintk("%s: i2c error %d, reg[0x%02x]\n", __func__, ret, reg);
}

u32 stv0900_read_be(struct stv0900_demod *demod, u16 reg, u8 len)
{
	int i;
	u64 result = 0;
	u8 buf[4];
	
	stv0900_read_regs(demod, reg, buf, len);
	
	for (i = 0; i < len; i++)
		result = (result << 8) | buf[i];
	
	return result;
}

u64 stv0900_read_be64(struct stv0900_demod *demod, u16 reg, u8 len)
{
	int i;
	u64 result = 0;
	u8 buf[8];
	
	stv0900_read_regs(demod, reg, buf, len);
	
	for (i = 0; i < len; i++)
		result = (result << 8) | buf[i];
	
	return result;
}

static void extract_mask_pos(u32 label, u8 *mask, u8 *pos)
{
	u8 position = 0, i = 0;

	(*mask) = label & 0xff;

	while ((position == 0) && (i < 8)) {
		position = ((*mask) >> i) & 0x01;
		i++;
	}

	(*pos) = (i - 1);
}

void stv0900_write_bits(struct stv0900_demod *demod, u32 label, u8 val)
{
	u8 reg, mask, pos;

	reg = stv0900_read_reg(demod, (label >> 16) & 0xffff);
	extract_mask_pos(label, &mask, &pos);

	val = mask & (val << pos);

	reg = (reg & (~mask)) | val;
	stv0900_write_reg(demod, (label >> 16) & 0xffff, reg);

}

u8 stv0900_get_bits(struct stv0900_demod *demod, u32 label)
{
	u8 val = 0xff;
	u8 mask, pos;

	extract_mask_pos(label, &mask, &pos);

	val = stv0900_read_reg(demod, label >> 16);
	val = (val & mask) >> pos;

	return val;
}

int stv0900_check_early_exit(struct stv0900_demod *demod)
{
	struct dvb_frontend *fe = &demod->frontend;
	struct dtv_frontend_properties *c = &fe->dtv_property_cache;
	int cancel = c->cancel;
	
	if (cancel)
		c->cancel = 0;
	
	return cancel || pid_task(fe->pid, PIDTYPE_PID) == NULL || c->state == DTV_RETUNE;
}

void stv0900_mutex_lock(struct stv0900_demod *demod)
{
	if (demod->chip->mutex_present) {
		if (!demod->mutex_count++)
			mutex_lock(&demod->chip->demod_mutex);
	}
}

void stv0900_mutex_unlock(struct stv0900_demod *demod)
{
	if (demod->chip->mutex_present) {
		if (!--demod->mutex_count)
			mutex_unlock(&demod->chip->demod_mutex);
	}
}

static int stv0900_check_dead_demod(struct stv0900_demod *demod)
{
	return demod->chip_id != stv0900_read_reg(demod, R0900_MID);
}

static enum stv0900_error stv0900_initialize(struct stv0900_demod *demod)
{
	struct stv0900_chip *chip = demod->chip;
	s32 i;

	if (!chip)
		return STV0900_INVALID_HANDLE;

	chip->chip_id = stv0900_read_reg(demod, R0900_MID);
	
	for (i = 0; i < 2; i++)
		if (chip->demod[i])
			chip->demod[i]->chip_id = chip->chip_id;

	dprintk("%s chip_id 0x%2.2x errs %d\n", __func__, chip->chip_id, chip->errs);
	
	if (chip->errs != STV0900_NO_ERROR)
		return chip->errs;

	/*Startup sequence*/
	stv0900_write_reg(demod, R0900_P1_DMDISTATE, 0x5c);
	stv0900_write_reg(demod, R0900_P2_DMDISTATE, 0x5c);
	
	usleep(3000);
	
	stv0900_write_reg(demod, R0900_P1_TNRCFG, 0x6c);
	stv0900_write_reg(demod, R0900_P2_TNRCFG, 0x6f);
	stv0900_write_reg(demod, R0900_P1_I2CRPT, 0x20);
	stv0900_write_reg(demod, R0900_P2_I2CRPT, 0x20);
	stv0900_write_reg(demod, R0900_NCOARSE, 0x13);
	
	usleep(3000);
	
	stv0900_write_reg(demod, R0900_I2CCFG, 0x08);

	switch (chip->clkmode) {
		case 0:
		case 2:
			stv0900_write_reg(demod, R0900_SYNTCTRL, 0x20 | chip->clkmode);
			break;
		default:
			/* preserve SELOSCI bit */
			i = 0x02 & stv0900_read_reg(demod, R0900_SYNTCTRL);
			stv0900_write_reg(demod, R0900_SYNTCTRL, 0x20 | i);
			break;
	}

	usleep(3000);
	
	for (i = 0; i < 181; i++)
		stv0900_write_reg(demod, STV0900_InitVal[i][0], STV0900_InitVal[i][1]);

	if (chip->chip_id >= 0x20) {
		stv0900_write_reg(demod, R0900_TSGENERAL, 0x0c);
		for (i = 0; i < 32; i++)
			stv0900_write_reg(demod, STV0900_Cut20_AddOnVal[i][0], STV0900_Cut20_AddOnVal[i][1]);
	}

	stv0900_write_reg(demod, R0900_P1_FSPYCFG, 0x6c);
	stv0900_write_reg(demod, R0900_P2_FSPYCFG, 0x6c);

	stv0900_write_reg(demod, R0900_P1_PDELCTRL2, 0x01);
	stv0900_write_reg(demod, R0900_P2_PDELCTRL2, 0x21);

	stv0900_write_reg(demod, R0900_P1_PDELCTRL3, 0x20);
	stv0900_write_reg(demod, R0900_P2_PDELCTRL3, 0x20);

	stv0900_write_reg(demod, R0900_TSTRES0, 0x80);
	stv0900_write_reg(demod, R0900_TSTRES0, 0x00);

	return STV0900_NO_ERROR;
}

static u32 stv0900_get_mclk_freq(struct stv0900_demod *demod, u32 ext_clk)
{
	u32 mclk, div, ad_div;

	div    = stv0900_get_bits(demod, F0900_M_DIV);
	ad_div = ((stv0900_get_bits(demod, F0900_SELX1RATIO) == 1) ? 4 : 6);

	mclk = (div + 1) * ext_clk / ad_div;

//	dprintk("%s: Calculated Mclk = %d\n", __func__, mclk);

	return mclk;
}

static enum stv0900_error stv0900_set_mclk(struct stv0900_demod *demod, u32 mclk)
{
	struct stv0900_chip *chip = demod->chip;

	u32 m_div, clk_sel, i;

	if (!chip)
		return STV0900_INVALID_HANDLE;

	if (chip->errs)
		return STV0900_I2C_ERROR;

	dprintk("%s: Mclk set to %d, Quartz = %d\n", __func__, mclk, chip->quartz);

	clk_sel = ((stv0900_get_bits(demod, F0900_SELX1RATIO) == 1) ? 4 : 6);
	m_div   = (clk_sel * mclk / chip->quartz) - 1;
	
	stv0900_write_bits(demod, F0900_M_DIV, m_div);
	
	chip->mclk = stv0900_get_mclk_freq(demod, chip->quartz);

	for (i = 0; i < 2; i++)
		if (chip->demod[i])
			chip->demod[i]->mclk = chip->mclk;
	
	/*Set the DiseqC frequency to 22KHz */
	/*
		Formula:
		DiseqC_TX_Freq= MasterClock/(32*F22TX_Reg)
		DiseqC_RX_Freq= MasterClock/(32*F22RX_Reg)
	*/
	m_div = chip->mclk / 704000;
	stv0900_write_reg(demod, R0900_P1_F22TX, m_div);
	stv0900_write_reg(demod, R0900_P1_F22RX, m_div);

	stv0900_write_reg(demod, R0900_P2_F22TX, m_div);
	stv0900_write_reg(demod, R0900_P2_F22RX, m_div);

	return chip->errs ? STV0900_I2C_ERROR : STV0900_NO_ERROR;
}

static u32 stv0900_get_err_count(struct stv0900_demod *demod, int cntr)
{
	return stv0900_read_be(demod, cntr ? ERRCNT22 : ERRCNT12, 3) & 0x7fffff;
}

static int stv0900_i2c_gate_ctrl(struct dvb_frontend *fe, int enable)
{
	struct stv0900_demod *demod = fe->demodulator_priv;
	stv0900_mutex_lock(demod);
	stv0900_write_bits(demod, I2CT_ON, enable);
	stv0900_mutex_unlock(demod);
	return 0;
}

static void stv0900_set_ts_parallel_serial(struct stv0900_demod *demod)
{
	const struct stv0900_config *config = demod->config;
	struct stv0900_chip *chip = demod->chip;
	u32 speed;

	dprintk("%s path1 %d path2 %d\n", __func__, config->path1_mode, config->path2_mode);

	if (demod->chip_id >= 0x20) {
		switch (config->path1_mode) {
			case STV0900_PARALLEL_PUNCT_CLOCK:
			case STV0900_DVBCI_CLOCK:
				switch (config->path2_mode) {
					case STV0900_SERIAL_PUNCT_CLOCK:
					case STV0900_SERIAL_CONT_CLOCK:
					default:
						stv0900_write_reg(demod, R0900_TSGENERAL, 0x00);
						break;
					case STV0900_PARALLEL_PUNCT_CLOCK:
					case STV0900_DVBCI_CLOCK:
						stv0900_write_reg(demod, R0900_TSGENERAL, 0x06);
						break;
				}
				break;
			case STV0900_SERIAL_PUNCT_CLOCK:
			case STV0900_SERIAL_CONT_CLOCK:
			default:
				switch (config->path2_mode) {
					case STV0900_SERIAL_PUNCT_CLOCK:
					case STV0900_SERIAL_CONT_CLOCK:
					default:
						stv0900_write_reg(demod, R0900_TSGENERAL, 0x0C);
						break;
					case STV0900_PARALLEL_PUNCT_CLOCK:
					case STV0900_DVBCI_CLOCK:
						stv0900_write_reg(demod, R0900_TSGENERAL, 0x0A);
						break;
				}
				break;
		}
	} else {
		switch (config->path1_mode) {
			case STV0900_PARALLEL_PUNCT_CLOCK:
			case STV0900_DVBCI_CLOCK:
				switch (config->path2_mode) {
					case STV0900_SERIAL_PUNCT_CLOCK:
					case STV0900_SERIAL_CONT_CLOCK:
					default:
						stv0900_write_reg(demod, R0900_TSGENERAL1X, 0x10);
						break;
					case STV0900_PARALLEL_PUNCT_CLOCK:
					case STV0900_DVBCI_CLOCK:
						stv0900_write_reg(demod, R0900_TSGENERAL1X, 0x16);
						break;
				}
				break;
			case STV0900_SERIAL_PUNCT_CLOCK:
			case STV0900_SERIAL_CONT_CLOCK:
			default:
				switch (config->path2_mode) {
					case STV0900_SERIAL_PUNCT_CLOCK:
					case STV0900_SERIAL_CONT_CLOCK:
					default:
						stv0900_write_reg(demod, R0900_TSGENERAL1X, 0x14);
						break;
					case STV0900_PARALLEL_PUNCT_CLOCK:
					case STV0900_DVBCI_CLOCK:
						stv0900_write_reg(demod, R0900_TSGENERAL1X, 0x12);
						break;
					}

				break;
		}
	}

	switch (config->path1_mode) {
		case STV0900_PARALLEL_PUNCT_CLOCK:
			stv0900_write_bits(demod, F0900_P1_TSFIFO_SERIAL, 0x00);
			stv0900_write_bits(demod, F0900_P1_TSFIFO_DVBCI, 0x00);
			break;
		case STV0900_DVBCI_CLOCK:
			stv0900_write_bits(demod, F0900_P1_TSFIFO_SERIAL, 0x00);
			stv0900_write_bits(demod, F0900_P1_TSFIFO_DVBCI, 0x01);
			break;
		case STV0900_SERIAL_PUNCT_CLOCK:
			stv0900_write_bits(demod, F0900_P1_TSFIFO_SERIAL, 0x01);
			stv0900_write_bits(demod, F0900_P1_TSFIFO_DVBCI, 0x00);
			break;
		case STV0900_SERIAL_CONT_CLOCK:
			stv0900_write_bits(demod, F0900_P1_TSFIFO_SERIAL, 0x01);
			stv0900_write_bits(demod, F0900_P1_TSFIFO_DVBCI, 0x01);
			break;
		default:
			break;
	}

	switch (config->path2_mode) {
		case STV0900_PARALLEL_PUNCT_CLOCK:
			stv0900_write_bits(demod, F0900_P2_TSFIFO_SERIAL, 0x00);
			stv0900_write_bits(demod, F0900_P2_TSFIFO_DVBCI, 0x00);
			break;
		case STV0900_DVBCI_CLOCK:
			stv0900_write_bits(demod, F0900_P2_TSFIFO_SERIAL, 0x00);
			stv0900_write_bits(demod, F0900_P2_TSFIFO_DVBCI, 0x01);
			break;
		case STV0900_SERIAL_PUNCT_CLOCK:
			stv0900_write_bits(demod, F0900_P2_TSFIFO_SERIAL, 0x01);
			stv0900_write_bits(demod, F0900_P2_TSFIFO_DVBCI, 0x00);
			break;
		case STV0900_SERIAL_CONT_CLOCK:
			stv0900_write_bits(demod, F0900_P2_TSFIFO_SERIAL, 0x01);
			stv0900_write_bits(demod, F0900_P2_TSFIFO_DVBCI, 0x01);
			break;
		default:
			break;
	}

	if (config->path3_mode == STV0900_SERIAL_CONT_CLOCK) {
		if (config->path3_clk > 0) {
			speed = chip->mclk / (config->path3_clk >> 5);
			
			if (speed < 0x20)
				speed = 0x20;
			else if (speed > 0xff)
				speed = 0xff;
			
			stv0900_write_bits(demod, TSFIFO_MANSPEED, 0x3);
			stv0900_write_reg(demod, TSSPEED, speed);			
		}
		
		stv0900_write_reg(demod, R0900_TSGENERAL, 0x0);
		stv0900_write_bits(demod, TSFIFO_SERIAL, 0x01);
		stv0900_write_bits(demod, TSFIFO_DVBCI, 0x00);
	}
	
	if (config->path1_clk > 0) {
		switch (config->path1_mode) {
			case STV0900_PARALLEL_PUNCT_CLOCK:
			case STV0900_DVBCI_CLOCK:
			default:
				speed = chip->mclk / (config->path1_clk >> 2);
				
				if (speed < 0x08)
					speed = 0x08;
				else if (speed > 0xff)
					speed = 0xff;
				break;
				
			case STV0900_SERIAL_PUNCT_CLOCK:
			case STV0900_SERIAL_CONT_CLOCK:
				speed = chip->mclk / (config->path1_clk >> 5);
				
				if (speed < 0x20)
					speed = 0x20;
				else if (speed > 0xff)
					speed = 0xff;
				break;
		}
		
		stv0900_write_bits(demod, F0900_P1_TSFIFO_MANSPEED, 0x3);
		stv0900_write_reg(demod, R0900_P1_TSSPEED, speed);
	}
	
	if (config->path2_clk > 0) {
		switch (config->path2_mode) {
			case STV0900_PARALLEL_PUNCT_CLOCK:
			case STV0900_DVBCI_CLOCK:
			default:
				speed = chip->mclk / (config->path2_clk >> 2);
				
				if (speed < 0x08)
					speed = 0x08;
				else if (speed > 0xff)
					speed = 0xff;
				break;
				
			case STV0900_SERIAL_PUNCT_CLOCK:
			case STV0900_SERIAL_CONT_CLOCK:
				speed = chip->mclk / (config->path2_clk >> 5);
				
				if (speed < 0x20)
					speed = 0x20;
				else if (speed > 0xff)
					speed = 0xff;
				break;
		}
		
		stv0900_write_bits(demod, F0900_P2_TSFIFO_MANSPEED, 0x3);
		stv0900_write_reg(demod, R0900_P2_TSSPEED, speed);
	}
	
	stv0900_write_bits(demod, F0900_P2_RST_HWARE, 1);
	stv0900_write_bits(demod, F0900_P2_RST_HWARE, 0);
	stv0900_write_bits(demod, F0900_P1_RST_HWARE, 1);
	stv0900_write_bits(demod, F0900_P1_RST_HWARE, 0);
}

/* estimates gain offset from frequeny tilt in dB * 1024 */
static s32 stv0900_get_tilt_gain(u64 Frequency)
{
	return (4 << 10) * (1550000000LL - (s64) Frequency) / 1200000000LL;
}

static void stv0900_set_bbgain(struct stv0900_demod *demod, u64 frequency, u32 bandwidth)
{
	u8 setgain, bbgain;
	
	if (bandwidth >= 27)
		setgain = 8;
	else if (bandwidth > 13)
		setgain = 12;
	else
		setgain = 14;
	
	bbgain = setgain;
	
	switch (demod->tuner_type) {
		case STV0900_TUNER_STB6100:
			/* stv090* appears to select bb vga normal gain mode, 2 Vpp */
			break;
			
		case STV0900_TUNER_STV6110:
			/* adc is always set to 1 Vpp for stv6110 */ 
			setgain -= 2;
			bbgain  -= 8;
			break;
	}
	
	demod->bbgain = (bbgain << 10) + stv0900_get_tilt_gain(frequency);
	
	if (demod->prev_gain != bbgain) {
		stv0900_write_bits(demod, TUN_GAIN, (setgain >> 1) + 7);
		demod->prev_gain = bbgain;
	}
		
	/* have caller do TNRLD */
}

static void stv0900_wait_tuner(struct stv0900_demod *demod)
{	
	int delay;
	
	if (demod->tuner_auto) {
		for (delay = 0; delay < 20 && !(stv0900_read_reg(demod, TNRLD) & 0x1); delay++)
			usleep(1000);
	}
	else {
		delay = demod->tuner_type == STV0900_TUNER_STB6100 ? 10 : 15;
		usleep(delay * 1000);
	}
	
	dprintk("%s: time %d\n", __func__, delay);
}

void stv0900_set_tuner(struct stv0900_demod *demod, u64 frequency, u32 bandwidth)
{
	struct dvb_frontend *fe = &demod->frontend;
	struct tuner_state *tuner = &fe->tuner;
	struct dvb_tuner_ops *ops = &fe->ops.tuner_ops;
	
	u64 round;
	u32 tunerFrequency, tunerBandwidth;
	u8 reg[3];

	dprintk("%s: freq %llu bw %u\n", __func__, frequency, bandwidth);
	
	tuner->frequency_cmd = frequency;
	tuner->bandwidth_cmd = bandwidth;

	if (demod->tuner_auto) {
		/* Formula:	Tuner_frequency_reg = Frequency(MHz)*64 */
		tunerFrequency = (frequency * 64 + 500000) / 1000000;
		tunerBandwidth = (bandwidth + 1999999) / 2000000;
		
		stv0900_set_bbgain(demod, frequency, tunerBandwidth);
		
		reg[0] = (tunerFrequency >> 10) & 0xff;
		reg[1] = (tunerFrequency >>  2) & 0xff;
		reg[2] = ((tunerFrequency & 0x3) << 6) | (tunerBandwidth & 0x3f);
		
		stv0900_write_regs(demod, TNRRF1, reg, 3);
		/* Tuner Write trig */
		stv0900_write_reg(demod, TNRLD, 1);
		
		stv0900_wait_tuner(demod);

		round = stv0900_read_be(demod, TNROBSL, 2) & 0x3ff;
		tuner->frequency = (((u64) tunerFrequency * 1000000) >> 6) - ((round * 1000000) >> 11);
		tuner->bandwidth = (stv0900_read_reg(demod, TNRBW) & 0x3f) * 2000000;
	}
	else {
		stv0900_i2c_gate_ctrl(fe, 1);
		
		if (ops->set_params)
			ops->set_params(fe);
		else {
			if (ops->set_frequency) {
				if (ops->set_frequency(fe, (frequency + 500) / 1000) < 0)
					dprintk("%s: Invalid parameter\n", __func__);
			}
			
			if (ops->set_bandwidth) {
				if (ops->set_bandwidth(fe, bandwidth) < 0)
					dprintk("%s: Invalid parameter\n", __func__);
			}
		}
		
		stv0900_i2c_gate_ctrl(fe, 0);
		stv0900_wait_tuner(demod);
	}
}

void stv0900_set_bandwidth(struct stv0900_demod *demod, u32 bandwidth)
{
	struct dvb_frontend *fe = &demod->frontend;
	struct tuner_state *tuner = &fe->tuner;
	struct dvb_tuner_ops *ops = &fe->ops.tuner_ops;
	
	tuner->bandwidth_cmd = bandwidth;
	
	if (demod->tuner_auto)
		stv0900_set_tuner(demod, tuner->frequency_cmd, bandwidth);
	else {
		dprintk("%s: bw %d\n", __func__, bandwidth);

		stv0900_i2c_gate_ctrl(fe, 1);
		
		if (ops->set_bandwidth) {
			if (ops->set_bandwidth(fe, bandwidth) < 0)
				dprintk("%s: Invalid parameter\n", __func__);
		}
		
		stv0900_i2c_gate_ctrl(fe, 0);
	}
}

/* estimates agc2 gain offset in dB * 1024 for spectrum scans */
s32 stv0900_get_agc2_offset(struct stv0900_demod *demod)
{
	s32 agc2_gain = 0, offs, imin, imax, i;
	const struct stv0900_point *level = stv0900_agc2_offset;
	
	for (i = 0; i < 4; i++)
		agc2_gain += stv0900_read_be(demod, AGC2I1, 2);
	
	agc2_gain >>= 2;

	imin = 0;
	imax = sizeof(stv0900_agc2_offset) / sizeof(struct stv0900_point) - 1;
	
	for (;;) {
		i = (imax + imin) >> 1;
		
		if (agc2_gain > level[i].regval)
			imax = i - 1;
		else if (agc2_gain < level[i + 1].regval)
			imin = i + 1;
		else
			break;
	}
	
	offs = (level[i + 1].realval - level[i].realval) * ((agc2_gain - level[i].regval) << 10) /
		   (level[i + 1].regval  - level[i].regval ) + (level[i].realval << 10);
	
	return offs;
}

/* estimates signal level in db * 1024 */
s32 stv0900_get_signal_level(struct stv0900_demod *demod)
{
	const struct stv0900_point *table = 0, *iq = 0;
	s32 agc_gain, iq_power = 0, imin, imax, i, level, tsize, iqsize;
	u8 iq_reg[2];

	switch (demod->tuner_type) {
		case STV0900_TUNER_STB6100:
			table  = stv0900_level_6100;
			tsize  = sizeof(stv0900_level_6100) / sizeof(struct stv0900_point);
			iq     = stv0900_iq_6100;
			iqsize = sizeof(stv0900_iq_6100) / sizeof(struct stv0900_point);
			break;
			
		case STV0900_TUNER_STV6110:
			table  = stv0900_level_6110;
			tsize  = sizeof(stv0900_level_6110) / sizeof(struct stv0900_point);
			iq     = stv0900_iq_6110;
			iqsize = sizeof(stv0900_iq_6110) / sizeof(struct stv0900_point);
			break;
	}
	
	if (!table || !iq)
		return 0;

	agc_gain = stv0900_read_be(demod, AGCIQIN1, 2);

	imin = 0;
	imax = tsize - 1;
	
	for (;;) {
		i = (imax + imin) >> 1;
		
		if (agc_gain > table[i].regval)
			imax = i - 1;
		else if (agc_gain < table[i + 1].regval)
			imin = i + 1;
		else
			break;
	}

	/* primary path is if agc loop is closed */
	if (i < tsize - 2)
		level = (table[i + 1].realval - table[i].realval) * ((agc_gain - table[i].regval) << 10) /
				(table[i + 1].regval  - table[i].regval ) + (table[i].realval << 10);
	else {
		/* agc at or near saturation, use I&Q level */
		stv0900_read_regs(demod, POWERI, iq_reg, 2);
		iq_power = (u32) iq_reg[0] + (u32) iq_reg[1];
		
		imin = 0;
		imax = iqsize - 1;
		
		for (;;) {
			i = (imax + imin) >> 1;
			
			if (iq_power > iq[i].regval)
				imax = i - 1;
			else if (iq_power < iq[i + 1].regval)
				imin = i + 1;
			else
				break;
		}
			
		if (i)
			level = (iq[i + 1].realval - iq[i].realval) * ((iq_power - iq[i].regval) << 10) /
					(iq[i + 1].regval  - iq[i].regval ) + (iq[i].realval << 10);
		else
			level = iq[1].realval << 10;
	}
	
	return level - demod->bbgain;
}

static int stv0900_update_signal_strength(struct dvb_frontend *fe)
{
	struct stv0900_demod *demod = fe->demodulator_priv;
	
	stv0900_mutex_lock(demod);	
	fe->dtv_property_cache.signal_level = stv0900_get_signal_level(demod);
	stv0900_mutex_unlock(demod);
	return 0;
}

/* estimates cnr in dB * 1024 */
s32 stv0900_get_snr(struct stv0900_demod *demod)
{
	const struct stv0900_point *lookup;
	s32	size, regval, imin, imax, i, noise_field, snr = 0;

	stv0900_mutex_lock(demod);
	
	if (stv0900_get_standard(demod) == STV0900_DVBS2_STANDARD) {
		noise_field = NNOSPLHT1;
		lookup = stv0900_s2_cn;
		size = sizeof(stv0900_s2_cn) / sizeof(struct stv0900_point);
	} else {
		noise_field = NNOSDATAT1;
		lookup = stv0900_s1_cn;
		size = sizeof(stv0900_s1_cn) / sizeof(struct stv0900_point);
	}

	if (stv0900_get_bits(demod, LOCK_DEFINITIF)) {
		if ((lookup != NULL) && size) {
			regval = 0;
			for (i = 0; i < 16; i++) {
				regval += stv0900_read_be(demod, noise_field, 2);
				usleep(1000);
			}

			imin = 0;
			imax = size - 1;
			
			for (;;) {
				i = (imax + imin) >> 1;
				
				if (regval > lookup[i].regval)
					imax = i - 1;
				else if (regval < lookup[i + 1].regval)
					imin = i + 1;
				else
					break;
			}
			
			if (i != 0)
				snr = (lookup[i + 1].realval - lookup[i].realval) * (regval - lookup[i].regval) /
					  (lookup[i + 1].regval  - lookup[i].regval ) + lookup[i].realval;
		}
	}

	stv0900_mutex_unlock(demod);
	return snr;
}

static int stv0900_update_snr(struct dvb_frontend *fe)
{
	struct stv0900_demod *demod = fe->demodulator_priv;
	
	stv0900_mutex_lock(demod);	
	fe->dtv_property_cache.snr = stv0900_get_snr(demod);
	stv0900_mutex_unlock(demod);
	return 0;
}

static int stv0900_update_ucblocks(struct dvb_frontend *fe)
{
	struct stv0900_demod *demod = fe->demodulator_priv;
	struct dtv_frontend_properties *c = &fe->dtv_property_cache;

	c->ucblocks = 0x0;
	stv0900_mutex_lock(demod);

	if (stv0900_get_standard(demod) == STV0900_DVBS2_STANDARD)
		c->ucblocks = stv0900_read_be(demod, UPCRCKO1, 2) + stv0900_read_be(demod, BBFCRCKO1, 2);
		
	stv0900_mutex_unlock(demod);
	return 0;
}

static int stv0900_update_ber(struct dvb_frontend *fe) 
{
	struct stv0900_demod *demod = fe->demodulator_priv;
	struct dtv_frontend_properties *c = &fe->dtv_property_cache;
	u32 demod_state;
	u64 pkt_count, bad_count;
	c->ber = 0;
	
	stv0900_mutex_lock(demod);
	demod_state = stv0900_get_bits(demod, HEADER_MODE);

	if (demod_state == STV0900_DVBS_FOUND || demod_state == STV0900_DVBS2_FOUND) {
		bad_count = stv0900_get_err_count(demod, 1);
		pkt_count = stv0900_read_be64(demod, FBERCPT4, 5) + bad_count;

		if (pkt_count)
			c->ber = 1000000000ULL * bad_count / pkt_count;
		
		stv0900_write_reg(demod, FBERCPT4, 0);
		stv0900_write_reg(demod, ERRCTRL2, 0xc1);
	}

	stv0900_mutex_unlock(demod);
	return 0;
}

int stv0900_get_demod_lock(struct stv0900_demod *demod)
{
	struct tuner_state *tuner = &demod->frontend.tuner;
	struct stv0900_signal *result = &demod->result;
	s32 timer = 0, lock = 0;

	while (timer < demod->demod_timeout) {
		switch (stv0900_get_bits(demod, HEADER_MODE)) {
			case STV0900_SEARCH:
			case STV0900_PLH_DETECTED:
			default:
				lock = 0;
				break;
				
			case STV0900_DVBS2_FOUND:
			case STV0900_DVBS_FOUND:
				lock = stv0900_get_bits(demod, LOCK_DEFINITIF);
				break;
		}

		if (lock)
			break;
		
		usleep_release(10000);
		timer += 10;
	}

	if (lock)
		dprintk("DEMOD LOCK OK time %d\n", timer);
	else {
		result->bad_lock_freq = stv0900_get_carr_freq(demod) + tuner->frequency;
		result->bad_lock_sr   = stv0900_get_symbol_rate_priv(demod);
		dprintk("DEMOD LOCK FAIL time %d freq %lld sr %d\n", timer, result->bad_lock_freq, result->bad_lock_sr);
	}
	
	result->lock_time = timer;
	
	return lock;
}

void stv0900_activate_s2_modcod(struct stv0900_demod *demod)
{
	u32 matype, mod_code, fmod, reg_index, field_index;
	u8 reg[16];

	if (demod->chip_id <= 0x11) {
		usleep(5000);

		mod_code = stv0900_read_reg(demod, PLHMODCOD);
		matype   = mod_code & 0x3;
		mod_code = (mod_code & 0x7f) >> 2;

		reg_index   = MODCODLSTF - (mod_code >> 1);
		field_index = mod_code & 0x1;

		switch (matype) {
			case 0:
			default:
				fmod = 14;
				break;
			case 1:
				fmod = 13;
				break;
			case 2:
				fmod = 11;
				break;
			case 3:
				fmod = 7;
				break;
		}

		if (INRANGE(STV0900_QPSK_12, mod_code, STV0900_8PSK_910) && matype <= 1) {
			if (field_index == 0)
				stv0900_write_reg(demod, reg_index, 0xf0 | fmod);
			else
				stv0900_write_reg(demod, reg_index, (fmod << 4) | 0xf);
		}

	} else {
		reg[0] = 0xff;
		reg[1] = 0xfc;
		
		for (reg_index = 2; reg_index < 15; reg_index++)
			reg[reg_index] = 0xcc;
		
		reg[15] = 0xcf;
		
		stv0900_write_regs(demod, MODCODLST0, reg, 16);
	}
}

void stv0900_stop_all_s2_modcod(struct stv0900_demod *demod, u32 index)
{
	int i;
	u8 reg[16];
	
	for (i = 0; i < 16; i++)
		reg[i] = 0xff;
	
	stv0900_write_regs(demod, REGi(R0900_P1_MODCODLST0, index), reg, 16);
}

void stv0900_activate_s2_modcod_single(struct stv0900_demod *demod, u32 index)
{
	u32 reg_index;
	u8 reg[16];

	reg[0] = 0xff;
	reg[1] = 0xf0;
	
	for (reg_index = 2; reg_index < 15; reg_index++)
		reg[reg_index] = 0x0;
	
	reg[15] = 0x0f;
	
	stv0900_write_regs(demod, REGi(R0900_P1_MODCODLST0, index), reg, 16);
}

static enum stv0900_error stv0900_st_dvbs2_single(struct stv0900_demod *demod, enum stv0900_demod_mode LDPC_Mode, u32 index)
{
	s32 reg_ind;
	u8 reg[16], algoswrst;
	
	switch (LDPC_Mode) {
		case STV0900_DUAL:
		default:
			if (demod->chip->demod_mode != STV0900_DUAL || stv0900_get_bits(demod, F0900_DDEMOD) != 1 || !demod->init_complete) {
				demod->chip->demod_mode = STV0900_DUAL;
				
				stv0900_write_reg(demod, R0900_GENCFG, 0x1d);
				
				stv0900_write_bits(demod, F0900_FRESFEC, 1);
				stv0900_write_bits(demod, F0900_FRESFEC, 0);
				
				for (reg_ind = 0; reg_ind < 7; reg_ind++)
					reg[reg_ind] = 0xff;
				
				for (reg_ind = 8; reg_ind < 14; reg_ind++)
					reg[reg_ind] = 0xcc;
				
				reg[14] = 0xff;
				reg[15] = 0xcf;
				
				stv0900_write_regs(demod, R0900_P1_MODCODLST0, reg, 16);
				stv0900_write_regs(demod, R0900_P2_MODCODLST0, reg, 16);
			}
			
			break;
		case STV0900_SINGLE:
			demod->chip->demod_mode = STV0900_SINGLE;
			
			stv0900_stop_all_s2_modcod(demod, 1 - index);
			stv0900_activate_s2_modcod_single(demod, index);
			
			stv0900_write_reg(demod, R0900_GENCFG, index ? 0x06 :0x04);
			
			stv0900_write_bits(demod, F0900_FRESFEC, 1);
			stv0900_write_bits(demod, F0900_FRESFEC, 0);
			
			reg_ind   = REGi(R0900_P1_PDELCTRL1, index);
			algoswrst = stv0900_read_reg(demod, reg_ind);
			
			stv0900_write_reg(demod, reg_ind, algoswrst | 0x01);
			stv0900_write_reg(demod, reg_ind, algoswrst & 0xfe);
			stv0900_write_reg(demod, reg_ind, algoswrst | 0x01);
			stv0900_write_reg(demod, reg_ind, algoswrst & 0xfe);
			break;
	}
	
	return STV0900_NO_ERROR;
}

static enum dvbfe_algo stv0900_frontend_algo(struct dvb_frontend *fe)
{
	return DVBFE_ALGO_CUSTOM;
}

void stv0900_start_search(struct stv0900_demod *demod)
{
	s32 freq;
	u8  reg[2];

	stv0900_write_bits(demod, DEMOD_MODE, 0x1f);
	
	if (demod->chip_id == 0x10)
		stv0900_write_reg(demod, CORRELEXP, 0xaa);

	if (demod->chip_id < 0x20)
		stv0900_write_reg(demod, CARHDR, 0x55);

	if (demod->chip_id <= 0x20) {
		if (demod->symbol_rate <= 5000000) {
			stv0900_write_reg(demod, CARCFG, 0x44);
			stv0900_write_be(demod, CFRUP1,  0x0fff, 2);
			stv0900_write_be(demod, CFRLOW1, 0xf000, 2);
			stv0900_write_reg(demod, RTCS2, 0x68);
		} else {
			stv0900_write_reg(demod, CARCFG, 0xc4);
			stv0900_write_reg(demod, RTCS2, 0x44);
		}
	} else { /*cut 3.0 above*/
		if (demod->symbol_rate <= 5000000)
			stv0900_write_reg(demod, RTCS2, 0x68);
		else
			stv0900_write_reg(demod, RTCS2, 0x44);

		stv0900_write_reg(demod, CARCFG, 0x46);
		
		if (demod->algo == STV0900_WARM_START)
			freq = (1000000LL << 16) / demod->mclk;
		else {
			freq = demod->srch_range >> 1;
			if (demod->symbol_rate <= 5000000)
				freq += 80000;
			else
				freq += 600000;

			freq = ((s64) freq << 16) / demod->mclk;
		}

		stv0900_write_be(demod, CFRUP1,   freq, 2);
		stv0900_write_be(demod, CFRLOW1, -freq, 2);
	}

	stv0900_write_reg(demod, CFRINIT1, 0);
	stv0900_write_reg(demod, CFRINIT0, 0);

	if (demod->chip_id >= 0x20) {
		stv0900_write_reg(demod, EQUALCFG, 0x41);
		stv0900_write_reg(demod, FFECFG, 0x41);

		if (demod->standard == STV0900_DVBS1_STANDARD || demod->standard == STV0900_DSS_STANDARD || demod->standard == STV0900_AUTO_STANDARD) {
			stv0900_write_reg(demod, VITSCALE, 0x82);
			stv0900_write_reg(demod, VAVSRVIT, 0x0);
		}
	}

	stv0900_write_reg(demod, SFRSTEP, 0x0);
	
	reg[0] = 0xe0;
	reg[1] = 0xc0;
	
	stv0900_write_regs(demod, TMGTHRISE, reg, 2);
	stv0900_write_bits(demod, SCAN_ENABLE, 0);
	stv0900_write_bits(demod, CFR_AUTOSCAN, 0);
	stv0900_write_bits(demod, S1S2_SEQUENTIAL, 0);
	stv0900_write_reg(demod, RTC, 0x88);
	
	if (demod->chip_id >= 0x20) {
		if (demod->symbol_rate < 2000000) {
			if (demod->chip_id <= 0x20)
				stv0900_write_reg(demod, CARFREQ, 0x39);
			else  /*cut 3.0*/
				stv0900_write_reg(demod, CARFREQ, 0x89);

			stv0900_write_reg(demod, CARHDR, 0x40);
		} else if (demod->symbol_rate < 10000000) {
			stv0900_write_reg(demod, CARFREQ, 0x4c);
			stv0900_write_reg(demod, CARHDR, 0x20);
		} else {
			stv0900_write_reg(demod, CARFREQ, 0x4b);
			stv0900_write_reg(demod, CARHDR, 0x20);
		}
	} else {
		if (demod->symbol_rate < 10000000)
			stv0900_write_reg(demod, CARFREQ, 0xef);
		else
			stv0900_write_reg(demod, CARFREQ, 0xed);
	}

	switch (demod->algo) {
		case STV0900_WARM_START:
			stv0900_write_reg(demod, DMDISTATE, 0x1f);
			stv0900_write_reg(demod, DMDISTATE, 0x18);
			break;
		case STV0900_COLD_START:
			stv0900_write_reg(demod, DMDISTATE, 0x1f);
			stv0900_write_reg(demod, DMDISTATE, 0x15);
			break;
		default:
			break;
	}
}

u8 stv0900_get_optim_carr_loop(struct stv0900_demod *demod)
{
	struct stv0900_signal *result = &demod->result;
	enum stv0900_modcode modcode = result->modcode;
	u32 srate = result->symbol_rate;
	u32 pilot = result->pilot;
	
	u8 aclc_value = 0x29;
	s32 i;
	const struct stv0900_car_loop_optim *cls2, *cllqs2, *cllas2;

	if (demod->chip_id <= 0x12) {
		cls2   = FE_STV0900_S2CarLoop;
		cllqs2 = FE_STV0900_S2LowQPCarLoopCut30;
		cllas2 = FE_STV0900_S2APSKCarLoopCut30;
	} else if (demod->chip_id == 0x20) {
		cls2   = FE_STV0900_S2CarLoopCut20;
		cllqs2 = FE_STV0900_S2LowQPCarLoopCut20;
		cllas2 = FE_STV0900_S2APSKCarLoopCut20;
	} else {
		cls2   = FE_STV0900_S2CarLoopCut30;
		cllqs2 = FE_STV0900_S2LowQPCarLoopCut30;
		cllas2 = FE_STV0900_S2APSKCarLoopCut30;
	}

	if (modcode < STV0900_QPSK_12) {
		for (i = 0; i < 3 && modcode != cllqs2[i].modcode; i++);

		if (i >= 3)
			i  = 2;
	} else {
		for (i = 0; i < 14 && modcode != cls2[i].modcode; i++);

		if (i >= 14) {
			for (i = 0; i < 11 && modcode != cllas2[i].modcode; i++);

			if (i >= 11)
				i  = 10;
		}
	}

	if (modcode <= STV0900_QPSK_25) {
		if (pilot) {
			if (srate <= 3000000)
				aclc_value = cllqs2[i].car_loop_pilots_on_2;
			else if (srate <= 7000000)
				aclc_value = cllqs2[i].car_loop_pilots_on_5;
			else if (srate <= 15000000)
				aclc_value = cllqs2[i].car_loop_pilots_on_10;
			else if (srate <= 25000000)
				aclc_value = cllqs2[i].car_loop_pilots_on_20;
			else
				aclc_value = cllqs2[i].car_loop_pilots_on_30;
		} else {
			if (srate <= 3000000)
				aclc_value = cllqs2[i].car_loop_pilots_off_2;
			else if (srate <= 7000000)
				aclc_value = cllqs2[i].car_loop_pilots_off_5;
			else if (srate <= 15000000)
				aclc_value = cllqs2[i].car_loop_pilots_off_10;
			else if (srate <= 25000000)
				aclc_value = cllqs2[i].car_loop_pilots_off_20;
			else
				aclc_value = cllqs2[i].car_loop_pilots_off_30;
		}
	} else if (modcode <= STV0900_8PSK_910) {
		if (pilot) {
			if (srate <= 3000000)
				aclc_value = cls2[i].car_loop_pilots_on_2;
			else if (srate <= 7000000)
				aclc_value = cls2[i].car_loop_pilots_on_5;
			else if (srate <= 15000000)
				aclc_value = cls2[i].car_loop_pilots_on_10;
			else if (srate <= 25000000)
				aclc_value = cls2[i].car_loop_pilots_on_20;
			else
				aclc_value = cls2[i].car_loop_pilots_on_30;
		} else {
			if (srate <= 3000000)
				aclc_value = cls2[i].car_loop_pilots_off_2;
			else if (srate <= 7000000)
				aclc_value = cls2[i].car_loop_pilots_off_5;
			else if (srate <= 15000000)
				aclc_value = cls2[i].car_loop_pilots_off_10;
			else if (srate <= 25000000)
				aclc_value = cls2[i].car_loop_pilots_off_20;
			else
				aclc_value = cls2[i].car_loop_pilots_off_30;
		}

	} else {
		if (srate <= 3000000)
			aclc_value = cllas2[i].car_loop_pilots_on_2;
		else if (srate <= 7000000)
			aclc_value = cllas2[i].car_loop_pilots_on_5;
		else if (srate <= 15000000)
			aclc_value = cllas2[i].car_loop_pilots_on_10;
		else if (srate <= 25000000)
			aclc_value = cllas2[i].car_loop_pilots_on_20;
		else
			aclc_value = cllas2[i].car_loop_pilots_on_30;
	}

	return aclc_value;
}

u8 stv0900_get_optim_short_carr_loop(struct stv0900_demod *demod)
{
	struct stv0900_signal *result = &demod->result;
	enum stv0900_modulation modulation = result->modulation;
	u32 srate = result->symbol_rate;
	
	const struct stv0900_short_frames_car_loop_optim *s2scl;
	const struct stv0900_short_frames_car_loop_optim_vs_mod *s2sclc30;
	s32 mod_index = 0;
	u8 aclc_value = 0x0b;

	s2scl    = FE_STV0900_S2ShortCarLoop;
	s2sclc30 = FE_STV0900_S2ShortCarLoopCut30;

	switch (modulation) {
		case STV0900_QPSK:
		default:
			mod_index = 0;
			break;
		case STV0900_8PSK:
			mod_index = 1;
			break;
		case STV0900_16APSK:
			mod_index = 2;
			break;
		case STV0900_32APSK:
			mod_index = 3;
			break;
	}

	if (demod->chip_id >= 0x30) {
		if (srate <= 3000000)
			aclc_value = s2sclc30[mod_index].car_loop_2;
		else if (srate <= 7000000)
			aclc_value = s2sclc30[mod_index].car_loop_5;
		else if (srate <= 15000000)
			aclc_value = s2sclc30[mod_index].car_loop_10;
		else if (srate <= 25000000)
			aclc_value = s2sclc30[mod_index].car_loop_20;
		else
			aclc_value = s2sclc30[mod_index].car_loop_30;

	} else if (demod->chip_id >= 0x20) {
		if (srate <= 3000000)
			aclc_value = s2scl[mod_index].car_loop_cut20_2;
		else if (srate <= 7000000)
			aclc_value = s2scl[mod_index].car_loop_cut20_5;
		else if (srate <= 15000000)
			aclc_value = s2scl[mod_index].car_loop_cut20_10;
		else if (srate <= 25000000)
			aclc_value = s2scl[mod_index].car_loop_cut20_20;
		else
			aclc_value = s2scl[mod_index].car_loop_cut20_30;

	} else {
		if (srate <= 3000000)
			aclc_value = s2scl[mod_index].car_loop_cut12_2;
		else if (srate <= 7000000)
			aclc_value = s2scl[mod_index].car_loop_cut12_5;
		else if (srate <= 15000000)
			aclc_value = s2scl[mod_index].car_loop_cut12_10;
		else if (srate <= 25000000)
			aclc_value = s2scl[mod_index].car_loop_cut12_20;
		else
			aclc_value = s2scl[mod_index].car_loop_cut12_30;

	}

	return aclc_value;
}

static int stv0900_hard_reset(struct dvb_frontend *fe)
{
	struct stv0900_demod *demod = fe->demodulator_priv;
	struct stv0900_chip *chip = demod->chip;
	const struct stv0900_config *config = demod->config;
	
	enum stv0900_error error      = STV0900_NO_ERROR;
	enum stv0900_error demodError = STV0900_NO_ERROR;

	int selosci, i;
	
	dprintk("%s\n", __func__);
		
	stv0900_mutex_lock(demod);
	demodError = stv0900_initialize(demod);
	
	if (demodError == STV0900_NO_ERROR) {
		error = STV0900_NO_ERROR;
	} else {
		if (demodError == STV0900_INVALID_HANDLE)
			error = STV0900_INVALID_HANDLE;
		else
			error = STV0900_I2C_ERROR;
		
		stv0900_mutex_unlock(demod);
		return error;
	}
	
//	chip->quartz     = config->xtal;
	chip->quartz = config->tun1_clkdiv < 2 ? config->xtal : config->xtal / config->tun1_clkdiv;
	chip->demod_mode = config->demod_mode;
	chip->ts_config  = config->ts_config_regs;
	
	stv0900_st_dvbs2_single(demod, chip->demod_mode, demod->index);
	stv0900_set_mclk(demod, 135000000);
	
	usleep(3000);
	
	switch (chip->clkmode) {
		case 0:
		case 2:
			stv0900_write_reg(demod, R0900_SYNTCTRL, 0x20 | chip->clkmode);
			break;
		default:
			selosci = 0x02 & stv0900_read_reg(demod, R0900_SYNTCTRL);
			stv0900_write_reg(demod, R0900_SYNTCTRL, 0x20 | selosci);
			break;
	}
	
	usleep(3000);
	
	chip->mclk = stv0900_get_mclk_freq(demod, chip->quartz);
	
	if (!chip->ts_config)
		stv0900_set_ts_parallel_serial(demod);
	else {
		for (i = 0; chip->ts_config[i].addr != 0xffff; i++)
			stv0900_write_reg(demod, chip->ts_config[i].addr, chip->ts_config[i].val);
		
		stv0900_write_bits(demod, F0900_P2_RST_HWARE, 1);
		stv0900_write_bits(demod, F0900_P2_RST_HWARE, 0);
		stv0900_write_bits(demod, F0900_P1_RST_HWARE, 1);
		stv0900_write_bits(demod, F0900_P1_RST_HWARE, 0);
	}
	
	/* tuner init */
	if (config->tun1_auto) {
		switch (config->tun1_type) {
			case STV0900_TUNER_STB6100:
				stv0900_write_reg(demod, R0900_P1_TNRCFG, 0x3c);
				stv0900_write_reg(demod, R0900_P1_TNRCFG2, 0x86);
				stv0900_write_reg(demod, R0900_P1_TNRCFG3, 0x18);
				stv0900_write_reg(demod, R0900_P1_TNRXTAL, config->xtal / 1000000);
				stv0900_write_reg(demod, R0900_P1_TNRSTEPS, 0x05);
				stv0900_write_reg(demod, R0900_P1_TNRGAIN, 0x17);
				stv0900_write_reg(demod, R0900_P1_TNRADJ, 0x1f);
				stv0900_write_reg(demod, R0900_P1_TNRCTL2, 0x0);
				stv0900_write_bits(demod, F0900_P1_TUN_TYPE, 3);
				break;
			case STV0900_TUNER_STV6110:
				stv0900_write_reg(demod, R0900_P1_TNRCFG, 0x4c);
				stv0900_write_reg(demod, R0900_P1_TNRCFG2, 0x06);
				stv0900_write_reg(demod, R0900_P1_TNRCFG3, 0x18);
				stv0900_write_reg(demod, R0900_P1_TNRXTAL, config->xtal / 1000000);
				stv0900_write_reg(demod, R0900_P1_TNRSTEPS, 0x05);
				stv0900_write_reg(demod, R0900_P1_TNRGAIN, 0x41);
				stv0900_write_reg(demod, R0900_P1_TNRADJ, 0x0);
				stv0900_write_reg(demod, R0900_P1_TNRCTL2, 0x97);
				stv0900_write_bits(demod, F0900_P1_TUN_TYPE, 4);
				
				switch (config->tun1_clkdiv) {
					case 0:
					case 1:
						stv0900_write_bits(demod, F0900_P1_TUN_KDIVEN, 0);
						break;
					case 2:
						stv0900_write_bits(demod, F0900_P1_TUN_KDIVEN, 1);
						break;
					case 4:
						stv0900_write_bits(demod, F0900_P1_TUN_KDIVEN, 2);
						break;
					case 8:
						stv0900_write_bits(demod, F0900_P1_TUN_KDIVEN, 3);
						break;
				}
				break;
		}
	}
	else
		stv0900_write_bits(demod, F0900_P1_TUN_TYPE, 6);
	
	stv0900_write_bits(demod, F0900_P1_TUN_MADDRESS, config->tun1_maddress);
	
	switch (config->tun1_type) {
		case STV0900_TUNER_STB6100:
			stv0900_write_reg(demod, R0900_TSTTNR1, 0x27);
			break;
		case STV0900_TUNER_STV6110:
			stv0900_write_reg(demod, R0900_TSTTNR1, 0x26);
			break;
		default:
			break;
	}
	
	stv0900_write_reg(demod, R0900_P1_TNRLD, 1); /* hw tuner */
	
	/* tuner init */
	if (config->tun2_auto) {
		switch (config->tun2_type) {
			case STV0900_TUNER_STB6100:
				stv0900_write_reg(demod, R0900_P2_TNRCFG, 0x3c);
				stv0900_write_reg(demod, R0900_P2_TNRCFG2, 0x86);
				stv0900_write_reg(demod, R0900_P2_TNRCFG3, 0x18);
				stv0900_write_reg(demod, R0900_P2_TNRXTAL, config->xtal / 1000000);
				stv0900_write_reg(demod, R0900_P2_TNRSTEPS, 0x05);
				stv0900_write_reg(demod, R0900_P2_TNRGAIN, 0x17);
				stv0900_write_reg(demod, R0900_P2_TNRADJ, 0x1f);
				stv0900_write_reg(demod, R0900_P2_TNRCTL2, 0x0);
				stv0900_write_bits(demod, F0900_P2_TUN_TYPE, 3);
				break;			
			case STV0900_TUNER_STV6110:
				stv0900_write_reg(demod, R0900_P2_TNRCFG, 0x4c);
				stv0900_write_reg(demod, R0900_P2_TNRCFG2, 0x06);
				stv0900_write_reg(demod, R0900_P2_TNRCFG3, 0x18);
				stv0900_write_reg(demod, R0900_P2_TNRXTAL, config->xtal / 1000000);
				stv0900_write_reg(demod, R0900_P2_TNRSTEPS, 0x05);
				stv0900_write_reg(demod, R0900_P2_TNRGAIN, 0x41);
				stv0900_write_reg(demod, R0900_P2_TNRADJ, 0x0);
				stv0900_write_reg(demod, R0900_P2_TNRCTL2, 0x97);
				stv0900_write_bits(demod, F0900_P2_TUN_TYPE, 4);
				
				switch (config->tun2_clkdiv) {
					case 0:
					case 1:
						stv0900_write_bits(demod, F0900_P2_TUN_KDIVEN, 0);
						break;
					case 2:
						stv0900_write_bits(demod, F0900_P2_TUN_KDIVEN, 1);
						break;
					case 4:
						stv0900_write_bits(demod, F0900_P2_TUN_KDIVEN, 2);
						break;
					case 8:
						stv0900_write_bits(demod, F0900_P2_TUN_KDIVEN, 3);
						break;
				}
				break;
		}
	}
	else
		stv0900_write_bits(demod, F0900_P2_TUN_TYPE, 6);
	
	stv0900_write_bits(demod, F0900_P2_TUN_MADDRESS, config->tun2_maddress);
	
	switch (config->tun2_type) {
		case STV0900_TUNER_STB6100:
			stv0900_write_reg(demod, R0900_TSTTNR3, 0x27);
			break;
		case STV0900_TUNER_STV6110:
			stv0900_write_reg(demod, R0900_TSTTNR3, 0x26);
			break;
		default:
			break;
	}
	
	stv0900_write_reg(demod, R0900_P2_TNRLD, 1); /* hw tuner */
	
	stv0900_write_bits(demod, F0900_P1_TUN_IQSWAP, demod->init_params.tun1_iq_inv);
	stv0900_write_bits(demod, F0900_P2_TUN_IQSWAP, demod->init_params.tun2_iq_inv);

	if (chip->errs)
		error = STV0900_I2C_ERROR;
	
	stv0900_mutex_unlock(demod);
	return error;
}

static enum stv0900_error stv0900_init_chip(struct stv0900_demod *demod, struct i2c_adapter *i2c)
{
	struct stv0900_chip  *chip  = 0;
	struct stv0900_inode *inode = find_inode(i2c, demod->config->demod_address);
	
	if (!inode) {
		chip = kzalloc(sizeof(struct stv0900_chip), GFP_KERNEL);
		
		if (!chip)
			return STV0900_INVALID_HANDLE;

		inode = append_chip(chip);
		
		if (!inode) {
			kfree(chip);
			return STV0900_INVALID_HANDLE;
		}
		
		chip->dmds_used = 1;
		chip->i2c       = i2c;
		chip->clkmode   = demod->config->clkmode;
		chip->errs      = STV0900_NO_ERROR;

		dprintk("%s: Create new chip structure!\n", __func__);
	}
	else if (inode && demod->config->demod_mode == STV0900_DUAL) {
		chip = inode->chip;
		chip->dmds_used++;
		
		if (!chip->mutex_present) {
			mutex_init(&chip->demod_mutex);
			chip->mutex_present = 1;
		}
		
		dprintk("%s: Found chip structure!\n", __func__);
	}
	
	if (!chip)
		return STV0900_INVALID_HANDLE;
	
	chip->demod[demod->index] = demod;

	demod->chip      = chip;
	demod->bbgain    = 0;
	demod->prev_gain = -1;
	
	if (demod->index == 0) {
		demod->tuner_auto = demod->config->tun1_auto;
		demod->tuner_type = demod->config->tun1_type;
	}
	else {
		demod->tuner_auto = demod->config->tun2_auto;
		demod->tuner_type = demod->config->tun2_type;
	}

	return STV0900_NO_ERROR;
}

static int stv0900_set_pls(struct stv0900_demod *demod, u8 pls_mode, u32 pls_code)
{
	u8 reg[3];
	
	enum stv0900_error error = STV0900_NO_ERROR;
	
	if (pls_mode == 0 && pls_code == 0)
		pls_code = 1;

	reg[0] = (pls_mode << 2) | (pls_code >> 16);
	reg[1] = (pls_code >> 8) & 0xff;
	reg[2] = pls_code & 0xff;
	
	stv0900_write_regs(demod, PLROOT2, reg, 3);
	
	return error;
}

static int stv0900_set_mis(struct stv0900_demod *demod, int mis)
{
	if (mis < 0 || mis > 255)
		stv0900_write_bits(demod, FILTER_EN, 0);
	else {
		stv0900_write_bits(demod, FILTER_EN, 1);
		stv0900_write_reg(demod, ISIENTRY, mis);
		stv0900_write_reg(demod, ISIBITENA, 0xff);
	}

	return STV0900_NO_ERROR;
}

static enum dvbfe_search stv0900_search(struct dvb_frontend *fe)
{
	struct stv0900_demod *demod = fe->demodulator_priv;
	struct dtv_frontend_properties *c = &fe->dtv_property_cache;
	struct dvb_frontend_ops *ops = &fe->ops;
	struct stv0900_signal *result = &demod->result;
	struct stv0900_blindscan *blind = &demod->blind;

	enum stv0900_algo search_algo;
	enum stv0900_standard standard;
	enum stv0900_error error = STV0900_NO_ERROR;
	int scan_mode;

	stv0900_mutex_lock(demod);
	
	if (stv0900_check_dead_demod(demod)) {
		stv0900_mutex_unlock(demod);
		return -EIO;
	}
	
	if (c->symbol_rate < ops->info.symbol_rate_min || c->symbol_rate > ops->info.symbol_rate_max) {
		stv0900_mutex_unlock(demod);
		return DVBFE_ALGO_SEARCH_FAILED;
	}

	switch (c->algo) {
		case ALGO_COLDSTART:
			scan_mode   = 0;
			search_algo = STV0900_COLD_START;
			break;
			
		case ALGO_WARMSTART:
			scan_mode   = 0;
			search_algo = STV0900_WARM_START;
			break;
			
		case ALGO_BLINDSCAN:
			scan_mode      = 1;
			search_algo    = STV0900_BLIND_SEARCH;
			c->symbol_rate = c->scan_sr_min;
			break;
			
		case ALGO_EXPERIMENTAL:
			scan_mode   = 1;
			search_algo = STV0900_BLIND_SCAN_AEP;
			break;
			
		case ALGO_BLIND:
		case ALGO_FAST:
		case ALGO_DEFAULT:
		default:
			scan_mode   = 0;
			search_algo = STV0900_BLIND_SEARCH;
			break;
	}
	
	switch (c->delivery_system) {
		case SYS_DVBS:
			standard = STV0900_DVBS1_STANDARD;
			break;
		case SYS_DSS:
			standard = STV0900_DSS_STANDARD;
			break;
		case SYS_DVBS2:
			standard = STV0900_DVBS2_STANDARD;
			break;
		case SYS_S_AUTO:
		default:
			standard = STV0900_AUTO_STANDARD;
			break;
	}

	if (scan_mode) {
		if (!c->scan_sr_min || c->scan_sr_min > c->scan_sr_max)
			c->scan_sr_min = ops->info.symbol_rate_min;
		
		if (!c->scan_sr_max || c->scan_sr_min > c->scan_sr_max)
			c->scan_sr_max = ops->info.symbol_rate_max;
	}
	
	demod->chip->errs  = STV0900_NO_ERROR;
	demod->standard    = standard;
	demod->symbol_rate = c->symbol_rate;
	demod->srch_range  = scan_mode ? 0 : 20000000;
	demod->freq        = c->frequency;	
	demod->bw          = c->bandwidth_hz ? c->bandwidth_hz : 72000000;
	demod->algo        = search_algo;
	demod->scan_mode   = scan_mode;
	demod->iq_inv      = STV0900_IQ_AUTO;
	demod->fec         = STV0900_FEC_UNKNOWN;
	demod->rolloff     = STV0900_35;
	
	memset(result, 0, sizeof(struct stv0900_signal));

	result->frequency   = c->frequency;
	result->symbol_rate = c->symbol_rate;
	
	if (demod->config->set_ts_params)
		demod->config->set_ts_params(fe, 0);
	
	if (c->stream_id == NO_STREAM_ID_FILTER) {
		stv0900_set_pls(demod, 0, 0);
		stv0900_set_mis(demod, -1);
	}
	else {
		stv0900_set_pls(demod, (c->stream_id >> 26) & 0x3, (c->stream_id >> 8) & 0x3ffff);
		stv0900_set_mis(demod, c->stream_id);
	}
	
	blind->skip_tune = 0;
	
	if (stv0900_algo(demod) == STV0900_RANGEOK && demod->chip->errs == STV0900_NO_ERROR) {
		switch (result->standard) {
			case STV0900_DVBS1_STANDARD:
				c->delivery_system = SYS_DVBS;
				break;
			case STV0900_DVBS2_STANDARD:
				c->delivery_system = SYS_DVBS2;
				break;
			case STV0900_DSS_STANDARD:
				c->delivery_system = SYS_DSS;
				break;
			default:
				c->delivery_system = SYS_UNDEFINED;
				break;
		}
		
		c->status        = result->status;
		c->frequency     = result->frequency;
		c->symbol_rate   = result->symbol_rate;
		c->snr           = result->cnr;
		c->signal_level  = result->signal_level;
		c->lock_timeout  = result->lock_timeout;
		c->lock_time     = result->fine_lock_time;
		
		c->custom_param2 = result->lock_attempts;
		c->custom_param3 = result->timing_ref;
		
		c->custom_param6 = result->coarse_cf_mean;
		c->custom_param7 = result->coarse_cf_sdev;
		c->custom_param8 = result->coarse_sr_mean;
		c->custom_param9 = result->coarse_sr_sdev;
		
		switch (c->delivery_system) {
			case SYS_DVBS:
			case SYS_DSS:
				switch (result->fec) {
					case STV0900_FEC_1_2:
						c->fec_inner = FEC_1_2;
						break;
					case STV0900_FEC_2_3:
						c->fec_inner = FEC_2_3;
						break;
					case STV0900_FEC_3_4:
						c->fec_inner = FEC_3_4;
						break;
					case STV0900_FEC_5_6:
						c->fec_inner = FEC_5_6;
						break;
					case STV0900_FEC_6_7:
						c->fec_inner = FEC_6_7;
						break;
					case STV0900_FEC_7_8:
						c->fec_inner = FEC_7_8;
						break;
					default:
						c->fec_inner = FEC_NONE;
						break;
				}
				
				c->pilot   = PILOT_OFF;
				c->rolloff = ROLLOFF_35;
				break;
			case SYS_DVBS2:
				switch (result->modcode) {
					case STV0900_QPSK_14:
						c->fec_inner = FEC_1_4;
						break;
					case STV0900_QPSK_13:
						c->fec_inner = FEC_1_3;
						break;
					case STV0900_QPSK_12:
						c->fec_inner = FEC_1_2;
						break;
					case STV0900_QPSK_35:
					case STV0900_8PSK_35:
						c->fec_inner = FEC_3_5;
						break;
					case STV0900_QPSK_23:
					case STV0900_8PSK_23:
					case STV0900_16APSK_23:
						c->fec_inner = FEC_2_3;
						break;
					case STV0900_QPSK_34:
					case STV0900_8PSK_34:
					case STV0900_16APSK_34:
					case STV0900_32APSK_34:
						c->fec_inner = FEC_3_4;
						break;
					case STV0900_QPSK_45:
					case STV0900_16APSK_45:
					case STV0900_32APSK_45:
						c->fec_inner = FEC_4_5;
						break;
					case STV0900_QPSK_56:
					case STV0900_8PSK_56:
					case STV0900_16APSK_56:
					case STV0900_32APSK_56:
						c->fec_inner = FEC_5_6;
						break;
					case STV0900_QPSK_89:
					case STV0900_8PSK_89:
					case STV0900_16APSK_89:
					case STV0900_32APSK_89:
						c->fec_inner = FEC_8_9;
						break;
					case STV0900_QPSK_910:
					case STV0900_8PSK_910:
					case STV0900_16APSK_910:
					case STV0900_32APSK_910:
						c->fec_inner = FEC_9_10;
						break;
					default:
						c->fec_inner = FEC_NONE;
						break;
				}
				
				switch (result->pilot) {
					case STV0900_PILOTS_OFF:
						c->pilot = PILOT_OFF;
						break;
					case STV0900_PILOTS_ON:
						c->pilot = PILOT_ON;
						break;
				}
				
				switch (result->rolloff) {
					case STV0900_35:
						c->rolloff = ROLLOFF_35;
						break;
					case STV0900_25:
						c->rolloff = ROLLOFF_25;
						break;
					case STV0900_20:
						c->rolloff = ROLLOFF_20;
						break;
					default:
						c->rolloff = ROLLOFF_AUTO;
						break;
				}
				break;
			default:
				c->pilot     = PILOT_OFF;
				c->rolloff   = ROLLOFF_35;
				c->fec_inner = FEC_NONE;
				break;
		}
		
		switch (result->spectrum) {
			case STV0900_IQ_NORMAL:
				c->inversion = INVERSION_OFF;
				break;
			case STV0900_IQ_SWAPPED:
				c->inversion = INVERSION_ON;
				break;
			default:
				c->inversion = INVERSION_AUTO;
				break;
		}
		
		switch (result->modulation) {
			case STV0900_QPSK:
			default:
				c->modulation = QPSK;
				break;
			case STV0900_8PSK:
				c->modulation = PSK_8;
				break;
			case STV0900_16APSK:
				c->modulation = APSK_16;
				break;
			case STV0900_32APSK:
				c->modulation = APSK_32;
				break;
		}
	} else {
		result->locked = 0;
		
		switch (demod->err) {
			case STV0900_I2C_ERROR:
				error = STV0900_I2C_ERROR;
				break;
			case STV0900_NO_ERROR:
			default:
				error = STV0900_SEARCH_FAILED;
				break;
		}
	}

	stv0900_mutex_unlock(demod);

	if (result->locked && error == STV0900_NO_ERROR) {
		dprintk("Search Success\n");
		return DVBFE_ALGO_SEARCH_SUCCESS;
	} else {
		dprintk("Search Fail\n");
		return DVBFE_ALGO_SEARCH_FAILED;
	}
}

static int stv0900_capture_constellation(struct dvb_frontend *fe)
{
	struct stv0900_demod *demod = fe->demodulator_priv;
	struct dtv_frontend_properties *c = &fe->dtv_property_cache;
	
	int i, n, err = 0, capture_ok = 1;
	u8 sel = 0, *iq = 0;
		
	n = c->sample_cnt << 1;
	
	if (n > SpectrumSize * 2)
		return -EINVAL;
	
	switch (c->const_src) {
		default:
		case CONST_SYMB:
			sel = 0x04;
			break;
			
		case CONST_INTER_SYMB:
			sel = 0x05;
			break;
			
		case CONST_DEMOD_OUT:
			sel = 0x00;
			break;
			
		case CONST_DEMOD_IN:
			sel = 0x08;
			break;
	}
	
	stv0900_mutex_lock(demod);	
	stv0900_write_reg(demod, IQCONST, sel);
	
	iq = (u8*) demod->spectrum.data;

	for (i = 0; i < n; i += 2) {
		stv0900_read_regs(demod, ISYMB, iq + i, 2);
		
		if ((i & 0x3ff) == 0 && stv0900_check_early_exit(demod)) {
			capture_ok = 0;
			err = -EINTR;
			break;
		}
	}
	
	stv0900_mutex_unlock(demod);

	if (capture_ok)
		err = copy_to_user(c->constellation_buffer, iq, n * sizeof(u8)) ? -EFAULT : 0;
	
	return err;
}

/*
 * Integer square root and log base 2 for for 64-bit unsigned integers. Square root
 * adapted from 32-bit version at:
 *
 * http://www.codecodex.com/wiki/Calculate_an_integer_square_root
 *
 * which was adapted from a technique suggested by Jack Crenshaw, originally at:
 *
 * http://www.embedded.com/98/9802fe2.htm
 *
 * Rather than derive 'place' (called 'x' here) from a series of shifts, it is calculated
 * directly by evaluating the base-2 log of the argument and shifting a single bit into 
 * the appropriate position. The calculation of log base 2 is adapted from a 32-bit 
 * algorithm suggested at:
 *
 * http://graphics.stanford.edu/~seander/bithacks.html
 *
 */

#define LT(n) n, n, n, n, n, n, n, n, n, n, n, n, n, n, n, n

static const char LogTable256[256] = {
	-1, 0, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3,
	LT(4), LT(5), LT(5), LT(6), LT(6), LT(6), LT(6),
	LT(7), LT(7), LT(7), LT(7), LT(7), LT(7), LT(7), LT(7)
};

s32 stv0900_ln2(u64 n)
{
	u64 r, x, y;
	u32 l;
	
	if ((x = n >> 32)) {
		if ((y = n >> 48))
			l = (r = n >> 56) ? 56 + LogTable256[r] : 48 + LogTable256[y];
		else 
			l = (r = n >> 40) ? 40 + LogTable256[r] : 32 + LogTable256[x];
	}
	else if ((y = n >> 16))
		l = (r = n >> 24) ? 24 + LogTable256[r] : 16 + LogTable256[y];
	else 
		l = (r = n >>  8) ?  8 + LogTable256[r] :      LogTable256[n];
	
	return l;
}

s64 stv0900_sqrt64(u64 n) 
{
	u64 r, x, y;
	
	/* find largest even power of 2 <= n */
	x = 1LL << (stv0900_ln2(n) & ~1);
	
	/* evaluate sqrt two bits at a time */
    r = 0;  
    y = n;  
	
    while (x) {  
        if (y >= r + x) {  
            y -= r + x;  
            r += x << 1;  
        }
		
        r >>= 1;  
        x >>= 2;  
    }
	
	return r; 
}

void stv0900_shellsort(s32 *a, u32 n) {
	u32 i, j, gap;
	s32 tmp;
	
	for (gap = 1; gap < n; gap = (gap << 1) + 1);
	
	for (gap >>= 1; gap > 0; gap >>= 1) {
		for (i = gap; i < n; i++) {
			tmp = a[i];
			
			for (j = i; j >= gap && a[j - gap] > tmp; j -= gap)
				a[j] = a[j - gap];
			
			a[j] = tmp;
		}
	}
}

#define SpectrumTunerBW (72 * 1000000)
#define SpectrumEdge    (30 * 1000000)

static int stv0900_capture_spectrum_priv(struct stv0900_demod *demod, struct stv0900_spectrum *spectrum, int normal)
{
	struct dvb_frontend_ops *ops = &demod->frontend.ops;
	struct tuner_state *tuner = &demod->frontend.tuner;
	
	s64 freq_min = ops->info.frequency_min;
	s64 freq_max = ops->info.frequency_max;
	
	s64 cf, prev_cf, start;
	s32 *spec, *agc1, *agc2, step, offs, i, prev_i, j, freq, prev, sr, mclk, x;
	s32 agc1_level = 0, agc2_level, splice_level= 0, corr, nsteps;
	u8  err = 0, newcf = 0, zcd = 0;
	
	start  = spectrum->start;
	step   = spectrum->step;
	nsteps = spectrum->nsteps;
	spec   = spectrum->data;
	
	if (normal) {
		agc1 = spec;
		agc2 = spec;
	}
	else {
		agc1 = spec;
		agc2 = spec + nsteps;
	}
	
	stv0900_write_reg(demod, DMDISTATE, 0x5c);
	stv0900_write_reg(demod, AGC2REF, 0x38);
	stv0900_write_be(demod, CFRINC1, 0x8000, 2);
	stv0900_write_bits(demod, SCAN_ENABLE, 0);
	stv0900_write_bits(demod, CFR_AUTOSCAN, 0);
	stv0900_write_reg(demod, DMDT0M, 0x0);
	stv0900_write_reg(demod, CARCFG, 0xe4);	
	
	/* calculate filter bandwidth correction */
	x  = spectrum->bw * 1024LL / 1000000LL;
	x *= x;
	x *= x;
	
	corr = ((stv0900_ln2(x) - 36) * 3083) / 8;
	sr   = (spectrum->bw * 3) >> 2;
	cf   = step > 0 ? 0 : 0x7fffffffffffffffLL;
	offs = SpectrumEdge - spectrum->bw / 2;
	mclk = demod->mclk;
	
	prev    = 0;
	prev_i  = 0;
	prev_cf = 0;
	
	stv0900_set_symbol_rate_bounds(demod, sr, sr, sr);		
	
	for (i = 0; i < nsteps; i++, start += step) {
		if (step > 0) {
			if (start > freq_max)
				start = freq_max;
			
			if (start > cf + offs) {
				cf    = start + offs;
				newcf = 1;
				
				if (cf > freq_max)
					cf = freq_max;
			}
		}
		else {
			if (start < freq_min)
				start = freq_min;
			
			if (start < cf - offs) {
				cf    = start - offs;
				newcf = 1;
				
				if (cf < freq_min)
					cf = freq_min;
			}
		}
		
		if (newcf) {
			if (stv0900_check_early_exit(demod)) {
				err = -EINTR;
				break;
			}

			if (i != 0) {
				if (normal)
					for (j = prev_i; j < i; j++)
						spec[j] += agc1_level;
				else
					for (j = prev_i; j < i; j++)
						agc1[j] = agc1_level;
				
				freq = ((start - prev_cf) << 16) / mclk;
				stv0900_write_be(demod, CFRINIT1, freq, 2);
				stv0900_write_reg(demod, DMDISTATE, 0x58);
				usleep(2000);
				
				splice_level = agc1_level + stv0900_get_agc2_offset(demod);
				
				stv0900_write_reg(demod, DMDISTATE, 0x5c);
			}
			
			prev_cf = cf;
			stv0900_set_tuner(demod, cf, SpectrumTunerBW);
			cf = tuner->frequency;
			usleep_release(0);
			
			prev_cf -= cf;
			
			/* stop i2c flood if demod is no longer responding */
			if (ABS(prev_cf) > 1000000 && stv0900_check_dead_demod(demod))
				return -EIO;
			
			prev    = -step;
			prev_i  = i;
			prev_cf = cf;
			newcf   = 0;
			zcd     = 0;
		}
		
		freq = ((start - cf) << 16) / mclk;
		stv0900_write_be(demod, CFRINIT1, freq, 2);
		stv0900_write_reg(demod, DMDISTATE, 0x58);
		usleep(2000);
		
		agc2_level = stv0900_get_agc2_offset(demod);
		agc2[i]    = agc2_level;
		
		if (freq * prev < 0) {
			zcd  = 1;
			prev = step;
			
			agc1_level = stv0900_get_signal_level(demod) + corr;
			
			if (prev_i != 0)
				agc1_level += splice_level - (agc1_level + agc2[prev_i]);
		}
		
		stv0900_write_reg(demod, DMDISTATE, 0x5c);
	}
	
	if (!err) {
		if (zcd == 0) {
			agc1_level = stv0900_get_signal_level(demod) + corr;
			
			if (prev_i != 0)
				agc1_level += splice_level - (agc1_level + agc2[prev_i]);
		}

		if (normal)
			for (j = prev_i; j < i; j++)
				spec[j] += agc1_level;
		else
			for (j = prev_i; j < i; j++)
				agc1[j] = agc1_level;
	}
		
	stv0900_write_be(demod, CFRINC1, 0x0000, 2);
	
	return err;
}

static int stv0900_capture_spectrum(struct dvb_frontend *fe)
{
	struct stv0900_demod *demod = fe->demodulator_priv;
	struct stv0900_spectrum *spectrum = &demod->spectrum;
	struct dtv_frontend_properties *c = &fe->dtv_property_cache;
	
	int normal, prev, err = 0;
	s32 nsteps, nxfr;
	
	/* already validated in dvb_frontend.c */
	nsteps = c->scan_freq_count;
	normal = !(c->scan_flags & SPECTRUM_CAL_DATA);
	nxfr   = normal ? nsteps : nsteps << 1;
	prev   = c->scan_flags & GET_PREV_SPECTRUM;
	
	if (!access_ok(VERIFY_WRITE, c->spectrum_buffer, nxfr * sizeof(s32)) || nxfr > SpectrumSize)
		return -EINVAL;
	
	if (!prev) {
		if (c->bandwidth_hz < 1000)
			c->bandwidth_hz = 1000;
		else  if (c->bandwidth_hz > SpectrumEdge)
			c->bandwidth_hz = SpectrumEdge;
		
		spectrum->start  = c->scan_freq_start;
		spectrum->step   = c->scan_freq_step;
		spectrum->nsteps = nsteps;
		spectrum->bw     = c->bandwidth_hz;
		
		stv0900_mutex_lock(demod);
		err = stv0900_capture_spectrum_priv(demod, spectrum, normal);
		stv0900_mutex_unlock(demod);
	}
	else if (nxfr != spectrum->nsteps)
		err = -EINVAL;
	
	if (!err)
		err = copy_to_user(c->spectrum_buffer, spectrum->data, nxfr * sizeof(s32)) ? -EFAULT : 0;
	
	return err;
}

s32 stv0900_get_carr_freq(struct stv0900_demod *demod)
{
	return (s64) demod->mclk * (s64) ge2comp(stv0900_read_be(demod, CFR2, 3), 24) / (1LL << 24);
}

static int stv0900_update_frequency(struct dvb_frontend *fe)
{
	struct stv0900_demod *demod = fe->demodulator_priv;
	struct tuner_state *tuner = &fe->tuner;
	struct dtv_frontend_properties *c = &fe->dtv_property_cache;
	
	stv0900_mutex_lock(demod);
	c->frequency = tuner->frequency + stv0900_get_carr_freq(demod);
	stv0900_mutex_unlock(demod);
	
	return 0;
}

u32 stv0900_get_symbol_rate_priv(struct stv0900_demod *demod)
{
	u32 sr = demod->mclk * (u64) stv0900_read_be(demod, SFR3, 4) >> 32;
	return sr + (s64) sr * ge2comp(stv0900_read_be(demod, TMGREG2, 3), 24) / (1LL << 29);
}

static int stv0900_update_symbol_rate(struct dvb_frontend *fe)
{
	struct stv0900_demod *demod = fe->demodulator_priv;
	struct dtv_frontend_properties *c = &fe->dtv_property_cache;
	
	stv0900_mutex_lock(demod);
	c->symbol_rate = stv0900_get_symbol_rate_priv(demod);
	stv0900_mutex_unlock(demod);
	
	return 0;
}

void stv0900_set_symbol_rate(struct stv0900_demod *demod, u32 sr)
{
	dprintk("%s: sr %d\n", __func__, sr);
	stv0900_write_be(demod, SFRINIT1, ((u64) sr << 16) / demod->mclk, 2);
}

void stv0900_reset_symbol_rates(struct stv0900_demod *demod)
{
	struct stv0900_blindscan *blind = &demod->blind;
	
	u8 reg[6];
	
	reg[0] = blind->init_sr_bits >> 8;
	reg[1] = blind->init_sr_bits & 0xff;
	reg[2] = (blind->max_sr_bits >> 8) & 0x7f;
	reg[3] = blind->max_sr_bits & 0xff;
	reg[4] = (blind->min_sr_bits >> 8) & 0x7f;
	reg[5] = blind->min_sr_bits & 0xff;
	
	stv0900_write_regs(demod, SFRINIT1, reg, 6);
}

void stv0900_set_symbol_rate_bounds(struct stv0900_demod *demod, u32 min_sr, u32 init_sr, u32 max_sr)
{
	struct stv0900_blindscan *blind = &demod->blind;

	blind->init_sr_bits = ((u64) init_sr << 16) / demod->mclk;
	blind->max_sr_bits  = ((u64) max_sr  << 16) / demod->mclk;
	blind->min_sr_bits  = ((u64) min_sr  << 16) / demod->mclk;
	
	stv0900_reset_symbol_rates(demod);
}

void stv0900_set_symbol_rate_range(struct stv0900_demod *demod, u32 sr, u32 percent)
{
	struct stv0900_blindscan *blind = &demod->blind;
	
	u64 sr16 = (u64) sr << 16;
	
	blind->init_sr_bits = sr16 / demod->mclk;
	blind->max_sr_bits  = (sr16 * (100ULL + percent)) / (demod->mclk * 100ULL);
	blind->min_sr_bits  = (sr16 * (100ULL - percent)) / (demod->mclk * 100ULL);
	
	stv0900_reset_symbol_rates(demod);
}

enum fe_status stv0900_get_status(struct stv0900_demod *demod)
{
	struct dvb_frontend *fe = &demod->frontend;
	struct dtv_frontend_properties *c = &fe->dtv_property_cache;
	u32 stat = FE_HAS_SIGNAL;
	u8  reg;
	
	stv0900_mutex_lock(demod);
	reg = stv0900_read_reg(demod, DSTATUS);
	
	if (reg & 0x80)
		stat |= FE_HAS_CARRIER;
	
	if (reg & 0x40)
		stat |= FE_HAS_TIMING;
	
	switch (stv0900_get_bits(demod, HEADER_MODE)) {
		case STV0900_DVBS_FOUND:
			reg = stv0900_read_reg(demod, VSTATUSVIT);
			
			if (reg & 0x10)
				stat |= FE_HAS_VITERBI;
				
			if (reg & 0x08)
				stat |= FE_HAS_SYNC;
			break;
			
		case STV0900_DVBS2_FOUND:
			if (stv0900_get_bits(demod, PKTDELIN_LOCK))
				stat |= FE_HAS_VITERBI | FE_HAS_SYNC;
			break;
			
		case STV0900_SEARCH:
		case STV0900_PLH_DETECTED:
		default:
			break;
	}
	
	if (stv0900_get_bits(demod, TSFIFO_LINEOK))
		stat |= FE_HAS_LOCK;
	
	if ((stat ^ c->status) & FE_HAS_LOCK) {
		if (stat & FE_HAS_LOCK) {
			dprintk("FULL LOCK %x\n", stat);
			
			if (demod->config->set_lock_led)
				demod->config->set_lock_led(fe, 1);
		}
		else {
			dprintk("UNLOCKED %x\n",  stat);
			
			if (demod->config->set_lock_led)
				demod->config->set_lock_led(fe, 0);
		}
	}
	
	stv0900_mutex_unlock(demod);
	return stat;
}

static int stv0900_update_status(struct dvb_frontend *fe)
{
	struct stv0900_demod *demod = fe->demodulator_priv;

	stv0900_mutex_lock(demod);
	fe->dtv_property_cache.status = stv0900_get_status(fe->demodulator_priv);
	stv0900_mutex_unlock(demod);
	return 0;
}

static int stv0900_update_bitrate(struct dvb_frontend *fe)
{
	struct stv0900_demod *demod = fe->demodulator_priv;
	struct dtv_frontend_properties *c = &fe->dtv_property_cache;

	stv0900_mutex_lock(demod);
	/* Formula Bit rate = Mclk * px_tsfifo_bitrate / 16384 */
	c->bitrate = (stv0900_get_mclk_freq(demod, demod->chip->quartz) >> 14) * stv0900_read_be(demod, TSBITRATE1, 2);
	stv0900_mutex_unlock(demod);
	dprintk("TS bitrate = %d Mbit/sec \n", c->bitrate);
	
	return 0;
}

static int stv0900_sleep(struct dvb_frontend *fe)
{
	struct stv0900_demod *demod = fe->demodulator_priv;
	struct stv0900_chip * chip = demod->chip;
	struct dvb_tuner_ops *ops;
	
	dprintk("%s\n", __func__);
	stv0900_mutex_lock(demod);
	
	if (demod->config->set_lock_led && demod->init_complete)
		demod->config->set_lock_led(fe, 0);
	
	if (!demod->tuner_auto) {
		ops = &fe->ops.tuner_ops;
		
		stv0900_i2c_gate_ctrl(fe, 1);
		
		if (ops->sleep) {
			if (ops->sleep(fe) < 0)
				dprintk("%s: Invalid parameter\n", __func__);
		}
		
		stv0900_i2c_gate_ctrl(fe, 0);
	}
	
	if (chip->demod_mode == STV0900_SINGLE || demod->index == 0) {
		chip->sleeping[0] = 1;
		
		stv0900_write_bits(demod, F0900_ADC1_PON, 0);
		stv0900_write_bits(demod, F0900_DISEQC1_PON, 0);
		stv0900_write_bits(demod, F0900_STOP_CLKPKDT1, 1);
		stv0900_write_bits(demod, F0900_STOP_CLKADCI1, 1);
		stv0900_write_bits(demod, F0900_STOP_CLKSAMP1, 1);
		stv0900_write_bits(demod, F0900_STOP_CLKVIT1, 1);
	}
	
	if (chip->demod_mode == STV0900_SINGLE || demod->index == 1) {
		chip->sleeping[1] = 1;
		
		stv0900_write_bits(demod, F0900_ADC2_PON, 0);
		stv0900_write_bits(demod, F0900_DISEQC2_PON, 0);
		stv0900_write_bits(demod, F0900_STOP_CLKPKDT2, 1);
		stv0900_write_bits(demod, F0900_STOP_CLKADCI2, 1);
		stv0900_write_bits(demod, F0900_STOP_CLKSAMP2, 1);
		stv0900_write_bits(demod, F0900_STOP_CLKVIT2, 1);
	}
	
	if (chip->sleeping[0] && chip->sleeping[1]) {
		stv0900_write_bits(demod, F0900_STOP_CLKFEC, 1);
		stv0900_write_bits(demod, F0900_STOP_CLKTS, 1);
		stv0900_write_bits(demod, F0900_STANDBY, 1);
	}
	
	stv0900_mutex_unlock(demod);
	return 0;
}

static int stv0900_wakeup(struct dvb_frontend *fe)
{
	struct stv0900_demod *demod = fe->demodulator_priv;
	struct stv0900_chip * chip = demod->chip;
	struct dvb_tuner_ops *ops;
	
	dprintk("%s\n", __func__);
	
	if (demod->config->set_lock_led)
		demod->config->set_lock_led(fe, 0);
	
	if (!demod->tuner_auto) {
		ops = &fe->ops.tuner_ops;
		
		stv0900_i2c_gate_ctrl(fe, 1);
		
		if (ops->init) {
			if (ops->init(fe) < 0)
				dprintk("%s: Invalid parameter\n", __func__);
		}
		
		stv0900_i2c_gate_ctrl(fe, 0);
	}
	
	if (chip->sleeping[0] && chip->sleeping[1]) {
		stv0900_write_bits(demod, F0900_STANDBY, 0);
		stv0900_write_bits(demod, F0900_STOP_CLKFEC, 0);
		stv0900_write_bits(demod, F0900_STOP_CLKTS, 0);
	}
	
	if (chip->demod_mode == STV0900_SINGLE || demod->index == 0) {
		chip->sleeping[0] = 0;
		
		stv0900_write_bits(demod, F0900_ADC1_PON, 1);
		stv0900_write_bits(demod, F0900_DISEQC1_PON, 1);
		stv0900_write_bits(demod, F0900_STOP_CLKPKDT1, 0);
		stv0900_write_bits(demod, F0900_STOP_CLKADCI1, 0);
		stv0900_write_bits(demod, F0900_STOP_CLKSAMP1, 0);
		stv0900_write_bits(demod, F0900_STOP_CLKVIT1, 0);
	}
	
	if (chip->demod_mode == STV0900_SINGLE || demod->index == 1) {
		chip->sleeping[1] = 0;
		
		stv0900_write_bits(demod, F0900_ADC2_PON, 1);
		stv0900_write_bits(demod, F0900_DISEQC2_PON, 1);
		stv0900_write_bits(demod, F0900_STOP_CLKPKDT2, 0);
		stv0900_write_bits(demod, F0900_STOP_CLKADCI2, 0);
		stv0900_write_bits(demod, F0900_STOP_CLKSAMP2, 0);
		stv0900_write_bits(demod, F0900_STOP_CLKVIT2, 0);
	}
	
	return 0;
}

static int stv0900_stop_ts(struct dvb_frontend *fe, int stop_ts)
{
	struct stv0900_demod *demod = fe->demodulator_priv;

	if (stop_ts)
		stv0900_write_bits(demod, RST_HWARE, 1);
	else
		stv0900_write_bits(demod, RST_HWARE, 0);

	return 0;
}

static int stv0900_diseqc_init(struct dvb_frontend *fe)
{
	struct stv0900_demod *demod = fe->demodulator_priv;

	stv0900_write_bits(demod, DISTX_MODE, demod->config->diseqc_mode);
	stv0900_write_bits(demod, DISEQC_RESET, 1);
	stv0900_write_bits(demod, DISEQC_RESET, 0);

	return 0;
}

static int stv0900_init(struct dvb_frontend *fe)
{
	struct stv0900_demod *demod = fe->demodulator_priv;

	dprintk("%s\n", __func__);
	stv0900_mutex_lock(demod);

	stv0900_wakeup(fe);
	stv0900_hard_reset(fe);
	stv0900_stop_ts(fe, 1);
	stv0900_diseqc_init(fe);

	stv0900_mutex_unlock(demod);
	return 0;
}

static int stv0900_diseqc_send(struct stv0900_demod *demod, u8 *data, u32 NbData)
{
	s32 i;

	stv0900_write_bits(demod, DIS_PRECHARGE, 1);
	
	for (i = 0; i < NbData; i++) {
		while (stv0900_get_bits(demod, FIFO_FULL));
		stv0900_write_reg(demod, DISTXDATA, data[i]);
	}

	stv0900_write_bits(demod, DIS_PRECHARGE, 0);

	for (i = 0; !stv0900_get_bits(demod, TX_IDLE) && i < 10; i++)
		usleep(10000);

	return 0;
}

static int stv0900_send_master_cmd(struct dvb_frontend *fe, struct dvb_diseqc_master_cmd *cmd)
{
	struct stv0900_demod *demod = fe->demodulator_priv;
	int status;
	
	stv0900_mutex_lock(demod);
	status = stv0900_diseqc_send(demod, cmd->msg, cmd->msg_len);
	stv0900_mutex_unlock(demod);
	return status;
}

static int stv0900_send_burst(struct dvb_frontend *fe, enum fe_sec_mini_cmd burst)
{
	struct stv0900_demod *demod = fe->demodulator_priv;
	u8 data;
	
	stv0900_mutex_lock(demod);

	switch (burst) {
		case SEC_MINI_A:
			stv0900_write_bits(demod, DISTX_MODE, 3);/* Unmodulated */
			data = 0x00;
			stv0900_diseqc_send(demod, &data, 1);
			break;
		case SEC_MINI_B:
			stv0900_write_bits(demod, DISTX_MODE, 2);/* Modulated */
			data = 0xff;
			stv0900_diseqc_send(demod, &data, 1);
			break;
	}

	stv0900_mutex_unlock(demod);
	return 0;
}

static int stv0900_recv_slave_reply(struct dvb_frontend *fe, struct dvb_diseqc_slave_reply *reply)
{
	struct stv0900_demod *demod = fe->demodulator_priv;
	s32 i;

	stv0900_mutex_lock(demod);	
	reply->msg_len = 0;

	for (i = 0; !stv0900_get_bits(demod, RX_END) && i < 10; i++)
		usleep(10000);

	if (stv0900_get_bits(demod, RX_END)) {
		reply->msg_len = stv0900_get_bits(demod, FIFO_BYTENBR);

		for (i = 0; i < reply->msg_len; i++)
			reply->msg[i] = stv0900_read_reg(demod, DISRXDATA);
	}

	stv0900_mutex_unlock(demod);
	return 0;
}

static int stv0900_set_tone(struct dvb_frontend *fe, enum fe_sec_tone_mode toneoff)
{
	struct stv0900_demod *demod = fe->demodulator_priv;

	stv0900_mutex_lock(demod);	

	switch (toneoff) {
		case SEC_TONE_ON:
			/*Set the DiseqC mode to 22Khz _continues_ tone*/
			stv0900_write_bits(demod, DISTX_MODE, 0);
			stv0900_write_bits(demod, DISEQC_RESET, 1);
			/*release DiseqC reset to enable the 22KHz tone*/
			stv0900_write_bits(demod, DISEQC_RESET, 0);
			break;
		case SEC_TONE_OFF:
			/*return diseqc mode to config->diseqc_mode.
			Usually it's without _continues_ tone */
			stv0900_write_bits(demod, DISTX_MODE, demod->config->diseqc_mode);
			/*maintain the DiseqC reset to disable the 22KHz tone*/
			stv0900_write_bits(demod, DISEQC_RESET, 1);
			stv0900_write_bits(demod, DISEQC_RESET, 0);
			break;
		default:
			stv0900_mutex_unlock(demod);
			return -EINVAL;
	}

	stv0900_mutex_unlock(demod);
	return 0;
}

static void stv0900_release(struct dvb_frontend *fe)
{
	struct stv0900_demod *demod = fe->demodulator_priv;

	dprintk("%s: Removing demod %d\n", __func__, demod->index);

	if (demod->config->set_lock_led && demod->init_complete)
		demod->config->set_lock_led(fe, 0);
	
	kfree(demod->spectrum.data);
	
	demod->chip->demod[demod->index] = 0;

	if (--demod->chip->dmds_used <= 0) {
		remove_inode(demod->chip);
		kfree(demod->chip);
	}

	kfree(demod);
}

static int stv0900_get_frontend(struct dvb_frontend *fe)
{
	struct stv0900_demod *demod = fe->demodulator_priv;
	struct stv0900_signal *result = &demod->result;
	struct dtv_frontend_properties *c = &fe->dtv_property_cache;

	c->frequency   = result->frequency;
	c->symbol_rate = result->symbol_rate;
	return 0;
}

static struct dvb_frontend_ops stv0900_ops = {
	.delsys = { SYS_S_AUTO, SYS_DVBS, SYS_DSS, SYS_DVBS2 },
	.info = {
		.name			       = "STV0900 frontend",
		.frequency_min		   =   940000000,
		.frequency_max		   =  2160000000,
		.frequency_stepsize	   =      125000,
		.frequency_tolerance   =           0,
		.symbol_rate_min	   =      100000,
		.symbol_rate_max	   =    60000000,
		.symbol_rate_tolerance =         500,
		.caps		= FE_CAN_FEC_1_2 | FE_CAN_FEC_2_3 |
					  FE_CAN_FEC_3_4 | FE_CAN_FEC_5_6 |
					  FE_CAN_FEC_7_8 | FE_CAN_QPSK    |
					  FE_CAN_2G_MODULATION |
					  FE_CAN_FEC_AUTO
	},
	.release					= stv0900_release,
	.init						= stv0900_init,
	.get_frontend				= stv0900_get_frontend,
	.sleep						= stv0900_sleep,
	.get_frontend_algo			= stv0900_frontend_algo,
	.i2c_gate_ctrl				= stv0900_i2c_gate_ctrl,
	.diseqc_send_master_cmd		= stv0900_send_master_cmd,
	.diseqc_send_burst			= stv0900_send_burst,
	.diseqc_recv_slave_reply	= stv0900_recv_slave_reply,
	.set_tone					= stv0900_set_tone,
	.reset						= stv0900_hard_reset,
	.search						= stv0900_search,
	.update_ber					= stv0900_update_ber,
	.update_bitrate				= stv0900_update_bitrate,
	.update_frequency			= stv0900_update_frequency,
	.update_signal_strength		= stv0900_update_signal_strength,
	.update_snr					= stv0900_update_snr,
	.update_status				= stv0900_update_status,
	.update_symbol_rate			= stv0900_update_symbol_rate,
	.update_ucblocks            = stv0900_update_ucblocks,
	.capture_constellation		= stv0900_capture_constellation,
	.capture_spectrum			= stv0900_capture_spectrum,
};

struct dvb_frontend *stv0900_attach(const struct stv0900_config *config, struct i2c_adapter *i2c, int index)
{
	struct stv0900_demod *demod;
	struct stv0900_init *init_params;
	struct stv0900_chip *chip;
	
	enum stv0900_error err;
	s32 *spec;

	demod = kzalloc(sizeof(struct stv0900_demod), GFP_KERNEL);
	spec  = kmalloc(sizeof(s32) * SpectrumSize, GFP_KERNEL);

	if (!demod || !spec)
		goto error;

	dprintk("%s: demod_mode %x addr %x\n", __func__, config->demod_mode, config->demod_address);
	
	demod->index    = index;
	demod->config   = config;
	demod->i2c      = i2c;
	demod->i2c_addr = config->demod_address;

	demod->frontend.demodulator_priv = demod;
	init_params = &demod->init_params;
	
	memcpy(&demod->frontend.ops, &stv0900_ops, sizeof(struct dvb_frontend_ops));
	
	switch (index) {
		case 0:
		case 1:
			init_params->tun1_iq_inv = STV0900_IQ_NORMAL;
			init_params->tun2_iq_inv = STV0900_IQ_SWAPPED;
				
			err = stv0900_init_chip(demod, i2c);

			if (err)
				goto error;

			chip = demod->chip;
			demod->blind.demod = demod;
			demod->spectrum.data = spec;

			if (chip->dmds_used == 1) {
				if (stv0900_hard_reset(&demod->frontend)) {
					stv0900_release(&demod->frontend);
					return 0;
				}
			}
			else {
				demod->chip_id = demod->chip->chip_id;
				demod->mclk    = demod->chip->mclk;				
			}
			
			if (demod->chip->chip_id >= 0x30)
				demod->frontend.ops.info.caps |= FE_CAN_MULTISTREAM;
			
			stv0900_sleep(&demod->frontend);

			demod->init_complete = 1;
			break;
		default:
			goto error;
			break;
	}

	printk(KERN_DEBUG "%s: Attaching STV0900 demodulator %d, chip id %x\n", __func__, index, demod->chip->chip_id);
	return &demod->frontend;

error:
	printk(KERN_DEBUG "%s: Failed to attach STV0900 demodulator %d\n", __func__, index);

	kfree(demod);
	kfree(spec);
	
	return 0;
}

EXPORT_SYMBOL(stv0900_attach);
MODULE_PARM_DESC(debug, "Set debug");
MODULE_AUTHOR("Igor M. Liplianin");
MODULE_DESCRIPTION("ST STV0900 frontend");
MODULE_LICENSE("GPL");

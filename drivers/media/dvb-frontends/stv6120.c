/*
 * STV6120 Silicon tuner driver
 *
 * Copyright (C)      Chris Lee <updatelee@gmail.com>
 * Copyright (C) 2016 Pendragon Sound
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include <linux/module.h>
#include <linux/string.h>
#include <linux/slab.h>
#include <linux/i2c.h>

#include "stv6120_reg.h"
#include "stv6120.h"
#include "stv6120_priv.h"

/* Max transfer size done by I2C transfer functions */
#define MAX_XFER_SIZE  64

int stv6120debug = 1;
module_param_named(debug, stv6120debug, int, 0644);

LIST_HEAD(stv6120list);

static int stv6120_read_regs(struct stv6120_state *state, u8 reg, u8 *data, u8 len) {
	struct i2c_msg msgs[] = {
		{ .addr = state->config->adr, .flags = 0,        .buf = &reg, .len = 1   },
		{ .addr = state->config->adr, .flags = I2C_M_RD, .buf = data, .len = len }
	};

	return i2c_transfer(state->i2c, msgs, 2) == 2 ? 0 : -EREMOTEIO;
}

static int stv6120_read_reg(struct stv6120_state *state, u8 reg) {
	u8 data;
	return stv6120_read_regs(state, reg, &data, 1) == 0 ? data : -EREMOTEIO;
}

static int stv6120_write_regs(struct stv6120_state *state, u8 reg, u8 *data, u8 len) {
	u8 buf[MAX_XFER_SIZE];

	struct i2c_msg msg = {
		.addr = state->config->adr, .flags = 0, .buf = buf, .len = len + 1
	};

	buf[0] = reg;
	memcpy(&buf[1], data, len);

	return i2c_transfer(state->i2c, &msg, 1) == 1 ? 0 : -EREMOTEIO;
}

static int stv6120_write_reg(struct stv6120_state *state, u8 reg, u8 data) {
	return stv6120_write_regs(state, reg, &data, 1);
}

static void stv6120_set_mutex(struct stv6120_state *state, int lock) {
	if (lock)
		mutex_lock(&state->base->tunerLock);
	else
		mutex_unlock(&state->base->tunerLock);
}

static void stv6120_path(struct stv6120_state *state, enum stv6120_rfsel rfs) {
	const struct stv6120_config *config = state->config;
	enum stv6120_rfsel *rfsel = state->base->rfsel;
	enum stv6120_rfsel other = rfsel[1 - config->tuner];
	u8 *cache = state->base->cache;

	if (rfs == STV6120_RF_NONE) {
		AndCache(PATHON, ~(1 << config->tuner));
		InsertCache(LNAON, 1 << other);
	}
	else {
		if (config->tuner == STV6120_TUNER1)
			InsertCache(RFSEL1, config->rfsel);
		else
			InsertCache(RFSEL2, config->rfsel);
		
		OrCache(PATHON,     1 << config->tuner);
		InsertCache(LNAON, (1 << rfs) | (1 << other));
	}
	
	rfsel[config->tuner] = rfs;
	WriteCacheN(RFSEL1, 2);
}

static int stv6120_init(struct dvb_frontend *fe) {
	struct stv6120_state *state = fe->tuner_priv;
	struct stv6120_base *base = state->base;
	struct tuner_state *tuner = state->tuner;
	const struct stv6120_config *config = state->config;
	u8 *cache = base->cache;
	
	dprintk("%s: STV6120\n", __func__); 
	
	stv6120_set_mutex(state, 1);

	if (!base->initDone) {
		if (stv6120_write_regs(state, STV6120_CTRL1, stv6120_init_table, sizeof(stv6120_init_table)))
			return -EREMOTEIO;
		
		ReadCacheN(MCLKDIV, STV6120_CTRL23 + 1);		
		InsertCache(MCLKDIV, 1);
		InsertCache(ODIV, config->odiv == 2);
		InsertCache(RDIV, config->refclk >= 27000000);
		InsertCache(K, (config->refclk / 1000000) - 16);		
		WriteCache(MCLKDIV);
		
		base->initDone = 1;
	}
	
	InsertCache(SYN,   1);
	InsertCache(SDOFF, 0);
	WriteCache(SYN);

	stv6120_path(state, config->rfsel);	
	base->sleeping[config->tuner] = 0;
	stv6120_set_mutex(state, 0);
	
	tuner->gain_cmd = ((s32) ExtractCache(BBGAIN) << 11) - 15872;
	
	return 0;
}

static int stv6120_sleep(struct dvb_frontend *fe) {
	struct stv6120_state *state = fe->tuner_priv;
	struct stv6120_base *base = state->base;
	const struct stv6120_config *config = state->config;
	u8 tuner  = config->tuner;
	u8 *cache = state->base->cache;
	
	dprintk("%s: STV6120\n", __func__); 
	
	if (!base->sleeping[tuner]) {
		stv6120_set_mutex(state, 1);
		stv6120_path(state, STV6120_RF_NONE);

		InsertCache(SYN,   0);
		InsertCache(SDOFF, 1);
		WriteCache(SYN);
		
		if (base->sleeping[1 - tuner])
			base->initDone = 0;
		
		base->sleeping[tuner] = 1;
		stv6120_set_mutex(state, 0);
	}
	
	return 0;
}

#if 0
static int stv6120_set_cutoff(struct dvb_frontend *fe, u64 frequency) {
	struct stv6120_state *state = fe->tuner_priv;
	const struct stv6120_config *config = state->config;
	u8 *cache = state->base->cache;
	int i, imin, imax;
	
	imin = 0;
	imax = sizeof(stv0120_cutoff_table) / sizeof(struct LookupEntry);
	
	for (;;) {
		i = (imax + imin) >> 1;
		
		if (frequency >= stv0120_cutoff_table[i].real)
			imax = i - 1;
		else if (frequency < stv0120_cutoff_table[i + 1].real)
			imin = i + 1;
		else
			break;
	}
	
	InsertCache(CFHF, stv0120_cutoff_table[i].raw);
	return WriteCache(CFHF);
}
#endif

static void stv6120_update_frequency(struct stv6120_state *state) {
	const struct stv6120_config *config = state->config;
	u8 *cache = state->base->cache;
	u32 s, n, f;
	
	s = ExtractCache(RDIV) + ExtractCache(PDIV) + 19;
	n = (ExtractCache(N8)  <<  8) | ExtractCache(N0);
	f = (ExtractCache(F15) << 15) | (ExtractCache(F7) << 7) | ExtractCache(F0);
	
	state->tuner->frequency = (config->refclk * (u64) ((n << 18) | f) + (1 << (s - 1))) >> s;	
}

static void stv6120_set_frequency_priv(struct stv6120_state *state) {
	const struct stv6120_config *config = state->config;
	u64 frequency = state->tuner->frequency_cmd;
	u8 *cache = state->base->cache;
	u64 fvco;
	u32 n18, n, f;
	u8  pdiv = 0, icp = 7;

	if (frequency < 596000000)
		pdiv = frequency < 299000000 ? 3 : 2;
	else if (frequency < 1191000000)
		pdiv = 1;
	
	fvco = frequency << (pdiv + 1);
	
	if (fvco < 3388000000) {
		if (fvco < 2701000000)
			icp = fvco < 2473000000 ? 0 : 1;
		else
			icp = fvco < 3022000000 ? 2 : 3;
	}
	else if (fvco < 4395000000)
		icp = fvco < 3846000000 ? 5 : 6;
	
	n18 = ((fvco << (ExtractCache(RDIV) + 18)) + (config->refclk >> 1)) / config->refclk;
	n   = n18 >> 18;
	f   = n18 - (n << 18);
	
	InsertCache(N0,   n);
	InsertCache(N8,   n >> 8);
	InsertCache(F0,   f);
	InsertCache(F7,   f >> 7);
	InsertCache(F15,  f >> 15);
	InsertCache(ICP,  icp);
	InsertCache(PDIV, pdiv);
	stv6120_update_frequency(state);
	
	dprintk("%s: cmd %llu set %llu\n", __func__, frequency, state->tuner->frequency);
}

static int stv6120_set_frequency(struct dvb_frontend *fe, u32 frequency) {
	struct stv6120_state *state = fe->tuner_priv;
	const struct stv6120_config *config = state->config;
	u8 *cache = state->base->cache;
	
	fe->tuner.frequency_cmd = (u64) frequency * 1000;
	
	stv6120_set_frequency_priv(state);
	WriteCacheN(N0, STV6120_CTRL8 - STV6120_CTRL3);		
	WriteField(CALVCOSTRT, 1);
	
	return 0;
}

static void stv6120_update_bandwidth(struct stv6120_state *state) {
	const struct stv6120_config *config = state->config;
	u8 *cache = state->base->cache;
	state->tuner->bandwidth = (ExtractCache(CF) + 5) * 2000000;
}

static void stv6120_set_bandwidth_priv(struct stv6120_state *state) {
	const struct stv6120_config *config = state->config;
	u8 *cache = state->base->cache;
	u32 bandwidth = state->tuner->bandwidth_cmd;
	u8 cf;

	if (bandwidth > 72000000 || bandwidth == 0)
		cf = 31;
	else if (bandwidth < 10000000)
		cf = 0;
	else
		cf = (bandwidth + 1999999) / 2000000 - 5;
	
	InsertCache(CF, cf);
	stv6120_update_bandwidth(state);
	
	dprintk("%s: cmd %u set %u\n", __func__, bandwidth, state->tuner->bandwidth);
}

static int stv6120_set_bandwidth(struct dvb_frontend *fe, u32 bandwidth) {
	struct stv6120_state *state = fe->tuner_priv;
	const struct stv6120_config *config = state->config;
	u8 *cache = state->base->cache;
	
	fe->tuner.bandwidth_cmd = bandwidth;

	stv6120_set_bandwidth_priv(state);
	WriteCache(CF); 	
	WriteField(CALRCSTRT, 1);
	
	return 0;
}

/*
 *	The following is a best estimate of relative tuner gain based on several stv0910 demod 
 *	drivers that interface to stv6120 tuners. Of course the actual tuner gain depends on 
 *	the AGC feedback loop, but this is modeled separately in the demod drivers. The implicit
 *	relative gain dependence on the tuner BBGAIN setting is:
 *
 *	gain in dB = 2.0 * bbgain - 15.5
 *
 *	Because this routine reports in units of 1/1024 dB, this becomes:
 *
 *	gain = 2048 * bbgain - 15872
 *
 *	However there is also a frequency-dependent negative gain tilt of 6 dB from 950 to 2150 
 *	MHz, with 0 dB taken at 1550 MHz. With frequency in MHz this can be calculated as:
 *
 *	tilt in dB = -frequency / 200.0 + 7.75
 *
 *	Adjusting this for units of 1/1024 dB and frequency in Hz, this becomes:
 *
 *	tilt = -(4 * frequency + 390625) / 781250 + 7936
 *
 *	The tilt factor is assumed to be constant below 950 MHz
 */

static int stv6120_get_tilt(struct dvb_frontend *fe, u64 frequency, s32 *tilt) {
	if (frequency < 950000000)
		frequency = 950000000;
	
	*tilt = 7936 - ((frequency << 2) + 390625) / 781250;
	return 0;
}

static void stv6120_update_gain(struct stv6120_state *state) {
	struct tuner_state *tuner = state->tuner;
	const struct stv6120_config *config = state->config;
	u8 *cache = state->base->cache;
	
	tuner->gain = ((s32) ExtractCache(BBGAIN) << 11) - 15872;
	stv6120_get_tilt(state->fe, tuner->frequency, &tuner->tilt);
}

static void stv6120_set_gain_priv(struct stv6120_state *state) {
	struct tuner_state *tuner = state->tuner;
	const struct stv6120_config *config = state->config;
	u8 *cache = state->base->cache;
	u64 freq  = tuner->frequency;
	s32 bbgain;
	
	if (freq < 950000000)
		freq = 950000000;
	
	bbgain = (tuner->gain_cmd + ((freq << 2) + 390625) / 781250 + 8960) / 2048;
	
	if (bbgain > 0x8)
		bbgain = 0x8;
	else if (bbgain < 0)
		bbgain = 0;
	
	InsertCache(BBGAIN, bbgain);
	stv6120_update_gain(state);
}

static int stv6120_set_params(struct dvb_frontend *fe) {
	struct stv6120_state *state = fe->tuner_priv;
	const struct stv6120_config *config = state->config;
	u8 *cache = state->base->cache;
	
	stv6120_set_frequency_priv(state);
	stv6120_set_bandwidth_priv(state);
	stv6120_set_gain_priv(state);
	
	WriteCacheN(BBGAIN, STV6120_CTRL8 - STV6120_CTRL2);		
	STV6120_WRITE(LOCK, 0x08 | InsertField(CALVCOSTRT, 1) | InsertField(CALRCSTRT, 1));
	
	return 0;
}

static int stv6120_get_status(struct dvb_frontend *fe, u32 *status) {
	struct stv6120_state *state = fe->tuner_priv;	
	const struct stv6120_config *config = state->config;
	*status = ReadField(LOCK);	
	return 0;
}

static int stv6120_release(struct dvb_frontend *fe) {
	struct stv6120_state *state = fe->tuner_priv;

	if (--state->base->count == 0) {
		list_del(&state->base->stv6120list);
		kfree(state->base);
	}
	
	kfree(state);
	return 0;
}

static struct dvb_tuner_ops stv6120_ops = {
	.info = {
		.name			= "STV6120 DVB Tuner",
		.frequency_min	=  240000000,
		.frequency_max	= 2160000000,
		.frequency_step	=		  57,
	},
	.release			= stv6120_release,
	.init				= stv6120_init,
	.sleep				= stv6120_sleep,
	.set_params			= stv6120_set_params,
	.get_status			= stv6120_get_status,
	.set_frequency		= stv6120_set_frequency,
	.set_bandwidth		= stv6120_set_bandwidth,
};

static struct stv6120_base *matchBase(struct i2c_adapter *i2c, u8 adr) {
	struct stv6120_base *p;
	
	list_for_each_entry(p, &stv6120list, stv6120list)
		if (p->i2c == i2c && p->adr == adr)
			return p;
	
	return 0;
}

struct dvb_frontend *stv6120_attach(struct dvb_frontend *fe, struct i2c_adapter *i2c, const struct stv6120_config *config) {
	struct stv6120_state *state;
	struct stv6120_base *base;
	
	if (!(state = kzalloc(sizeof(struct stv6120_state), GFP_KERNEL)))
		return 0;
	
	if (!(base = matchBase(i2c, config->adr))) {
		if (!(base = kzalloc(sizeof(struct stv6120_base), GFP_KERNEL)))
			goto fail;
		
		base->i2c      = i2c;
		base->adr      = config->adr;
		base->rfsel[0] = STV6120_RF_NONE;
		base->rfsel[1] = STV6120_RF_NONE;

		mutex_init(&base->tunerLock);
		list_add(&base->stv6120list, &stv6120list);
	}
	
	base->count++;
	
	state->i2c     = i2c;
	state->config  = config;
	state->fe      = fe;
	state->tuner   = &fe->tuner;
	state->base    = base;
	fe->tuner_priv = state;
	
	memcpy(&fe->ops.tuner_ops, &stv6120_ops, sizeof(struct dvb_tuner_ops));

	printk(KERN_DEBUG "%s: Attaching stv6120, tuner: %d\n", __func__, config->tuner);

	stv6120_init(fe);
	stv6120_sleep(fe);
	return fe;
	
fail:
	kfree(state);
	return 0;
}

EXPORT_SYMBOL(stv6120_attach);

MODULE_AUTHOR("Chris Lee");
MODULE_DESCRIPTION("STV6120 Silicon tuner");
MODULE_LICENSE("GPL");

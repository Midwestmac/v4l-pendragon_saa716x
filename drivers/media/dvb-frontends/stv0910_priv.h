/*
 * Driver for the ST STV0910 DVB-S/S2 demodulator.
 *
 * Copyright (C) 2014-2015 Ralph Metzler <rjkm@metzlerbros.de>
 *                         Marcus Metzler <mocm@metzlerbros.de>
 *                         developed for Digital Devices GmbH
 * Copyright (C) 2015-2016 Chris Lee <updatelee@gmail.com>
 * Copyright (C) 2016      Pendragon Sound
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 only, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA
 * Or, point your browser to http://www.gnu.org/copyleft/gpl.html
 */

#ifndef __STV0910_PRIV_H__
#define __STV0910_PRIV_H__

#define dprintk(args...) do { if (stv0910debug) printk(KERN_DEBUG args); } while (0)
#define usleep_release(a)	dvb_frontend_nsleep_release(&state->fe, (a) * 1000LL)
#define usleep(a)			nsleep((a) * 1000LL)

enum stv0910_frame { LongFrame, ShortFrame };

enum stv0910_modcode {
	DVBS2_UNKNOWN,    DVBS2_QPSK_1_4,   DVBS2_QPSK_1_3,   DVBS2_QPSK_2_5,
	DVBS2_QPSK_1_2,   DVBS2_QPSK_3_5,   DVBS2_QPSK_2_3,	  DVBS2_QPSK_3_4,
	DVBS2_QPSK_4_5,	  DVBS2_QPSK_5_6,   DVBS2_QPSK_8_9,	  DVBS2_QPSK_9_10,
	DVBS2_8PSK_3_5,	  DVBS2_8PSK_2_3,   DVBS2_8PSK_3_4,   DVBS2_8PSK_5_6,
	DVBS2_8PSK_8_9,	  DVBS2_8PSK_9_10,  DVBS2_16APSK_2_3, DVBS2_16APSK_3_4,
	DVBS2_16APSK_4_5, DVBS2_16APSK_5_6, DVBS2_16APSK_8_9, DVBS2_16APSK_9_10,
	DVBS2_32APSK_3_4, DVBS2_32APSK_4_5, DVBS2_32APSK_5_6, DVBS2_32APSK_8_9,
	DVBS2_32APSK_9_10
};

enum stv0910_rolloff { Roll35, Roll25, Roll20, Roll15 };

enum stv0900_search_state {
	Searching = 0,
	PLHdetected,
	DVBS2found,
	DVBSfound
};

enum stv0910_algo {
	BlindSearch,	/* offset freq and SR are Unknown */
	ColdStart,		/* only SR is known */
	WarmStart,		/* offset freq and SR are known */
	BlindScanAEP
};

enum stv0910_search_status {
	SearchOK,
	NoAGC1,
	NoCarrier,
	NoTiming,
	NoData,
	OutOfRange,
	Failure,
};

struct LookupTable {
	s32  real;
	s32  raw;
};

struct stv0910_base {
	struct list_head    stv0910list;
	
	struct i2c_adapter  *i2c;
	struct mutex        tunerLock;
	struct mutex        demodLock;
	
	u32                 mclk;
	u32					count;
	u8					sleeping[2];
	u8					adr;
	u8					initDone;
};

struct stv0910_state {
	const struct stv0910_config	*config;
	struct stv0910_base			*base;
	struct dvb_frontend			fe;
	enum stv0910_modcode		modCode;
	enum stv0910_frame			frame;
	
	u32		demodTimeout;
	u32		fecTimeout;
	u8		berSource;
	u8		berScale;
	u8		i2crpt;
	u8		tscfgh;
	u8		disTXcfg;
	u8		disRXcfg;
};

#endif
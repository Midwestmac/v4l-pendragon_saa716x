/*
 * STV6120 Silicon tuner driver
 *
 * Copyright (C)      Chris Lee <updatelee@gmail.com>
 * Copyright (C) 2016 Pendragon Sound
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#ifndef __STV6120_PRIV_H
#define __STV6120_PRIV_H

#define dprintk(args...) do { if (stv6120debug) printk(KERN_DEBUG args); } while (0)
#define usleep(a)	nsleep((a) * 1000LL)

struct stv6120_base {
	struct list_head    stv6120list;	
	struct i2c_adapter	*i2c;
	struct mutex        tunerLock;
	enum stv6120_rfsel	rfsel[2];
	int					count;
	u8					cache[STV6120_CTRL23 + 1];
	u8					sleeping[2];
	u8					adr;
	u8					initDone;
};

struct stv6120_state {
	struct i2c_adapter	        *i2c;
	struct dvb_frontend			*fe;
	struct tuner_state			*tuner;
	const struct stv6120_config	*config;
	struct stv6120_base			*base;
};

static u8 stv6120_init_table[] = {
	0x77,	//	0x00	STV6120_CTRL1
	0x33,	//	0x01	STV6120_CTRL2
	0xce,	//	0x02	STV6120_CTRL3
	0x54,	//	0x03	STV6120_CTRL4
	0x55,	//	0x04	STV6120_CTRL5
	0x0d,	//	0x05	STV6120_CTRL6
	0x32,	//	0x06	STV6120_CTRL7
	0x44,	//	0x07	STV6120_CTRL8
	0x0e,	//	0x08	STV6120_STAT1
	0xf9,	//	0x09	STV6120_CTRL9
	0x03,	//	0x0a	STV6120_CTRL10
	0x33,	//	0x0b	STV6120_CTRL11
	0xce,	//	0x0c	STV6120_CTRL12
	0x54,	//	0x0d	STV6120_CTRL13
	0x55,	//	0x0e	STV6120_CTRL14
	0x0d,	//	0x0f	STV6120_CTRL15
	0x32,	//	0x10	STV6120_CTRL16
	0x44,	//	0x11	STV6120_CTRL17
	0x0e,	//	0x12	STV6120_STAT2
	0x00,	//	0x13	STV6120_CTRL18
	0x00,	//	0x14	STV6120_CTRL19
	0x4c,	//	0x15	STV6120_CTRL20
	0x00,	//	0x16	STV6120_CTRL21
	0x00,	//	0x17	STV6120_CTRL22
	0x4c	//	0x18	STV6120_CTRL23
};

struct LookupEntry {
	u64	real;
	u32 raw;
};

struct LookupEntry stv0120_cutoff_table[] = {
	{	       ~0x0LL, 0x00	},
	{	   5828000000, 0x01	},
	{	   4778000000, 0x02	},
	{	   4118000000, 0x03	},
	{	   3513000000, 0x04	},
	{	   3136000000, 0x05	},
	{	   2794000000, 0x06	},
	{	   2562000000, 0x07	},
	{	   2331000000, 0x08	},
	{	   2169000000, 0x09	},
	{	   2006000000, 0x0a	},
	{	   1890000000, 0x0b	},
	{	   1771000000, 0x0c	},
	{	   1680000000, 0x0d	},
	{	   1586000000, 0x0e	},
	{	   1514000000, 0x0f	},
	{	   1433000000, 0x10	},
	{	   1374000000, 0x11	},
	{	   1310000000, 0x12	},
	{	   1262000000, 0x13	},
	{	   1208000000, 0x14	},
	{	   1167000000, 0x15	},
	{	   1122000000, 0x16	},
	{	   1087000000, 0x17	},
	{	   1049000000, 0x18	},
	{	   1018000000, 0x19	},
	{	    983000000, 0x1a	},
	{	    956000000, 0x1b	},
	{	    926000000, 0x1c	},
	{	    902000000, 0x1d	},
	{	    875000000, 0x1e	},
	{	    854000000, 0x1f	},
	{	            0, 0x1f	}
};

#endif /* __STV6120_PRIV_H__ */

/*
 * STV6120 Silicon tuner driver
 *
 * Copyright (C)      Chris Lee <updatelee@gmail.com>
 * Copyright (C) 2016 Pendragon Sound
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PAReadCacheRTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#ifndef __STV6120_REG_H__
#define __STV6120_REG_H__

#define SelectReg(reg)				(config->tuner ? STV6120_P2_##reg : STV6120_P1_##reg)

#define STV6120_READ(reg)			stv6120_read_reg(state, SelectReg(reg))
#define STV6120_READN(reg, buf, n)	stv6120_read_regs(state, SelectReg(reg), buf, n)
#define STV6120_WRITE(reg, val)		stv6120_write_reg(state, SelectReg(reg), val)
#define STV6120_WRITEN(reg, buf, n)	stv6120_write_regs(state, SelectReg(reg), buf, n)

#define MaskField(field)			(STV6120_M_##field << STV6120_S_##field)
#define ExtractField(field, val)	(((val) >> STV6120_S_##field) & STV6120_M_##field)
#define ClearField(field, val)		((val) & ~(MaskField(field)))
#define InsertField(field, val)		(((val) & STV6120_M_##field) << STV6120_S_##field)

#define ReadField(field)			ExtractField(field, STV6120_READ(field))
#define WriteField(field, val)		STV6120_WRITE(field, ClearField(field, STV6120_READ(field)) | InsertField(field, val))

#define ExtractCache(field)			ExtractField(field, cache[SelectReg(field)])
#define ClearCache(field)			cache[SelectReg(field)]  = ClearField(field, cache[SelectReg(field)])
#define InsertCache(field, val)		cache[SelectReg(field)]  = ClearField(field, cache[SelectReg(field)]) | InsertField(field, val)
#define OrCache(field, val)			cache[SelectReg(field)] |= InsertField(field, val)
#define AndCache(field, val)		cache[SelectReg(field)] &= InsertField(field, val) | ~MaskField(field)

#define ReadCache(reg)				cache[SelectReg(reg)] = STV6120_READ(reg)
#define ReadCacheN(reg, n)			STV6120_READN(reg, &cache[SelectReg(reg)], n)
#define WriteCache(reg)				STV6120_WRITE(reg, cache[SelectReg(reg)])
#define WriteCacheN(reg, n)			STV6120_WRITEN(reg, &cache[SelectReg(reg)], n)

#define STV6120_CTRL1				0x00
#define STV6120_CTRL2				0x01
#define STV6120_CTRL3				0x02
#define STV6120_CTRL4				0x03
#define STV6120_CTRL5				0x04
#define STV6120_CTRL6				0x05
#define STV6120_CTRL7				0x06
#define STV6120_CTRL8				0x07
#define STV6120_STAT1				0x08
#define STV6120_CTRL9				0x09
#define STV6120_CTRL10				0x0a
#define STV6120_CTRL11				0x0b
#define STV6120_CTRL12				0x0c
#define STV6120_CTRL13				0x0d
#define STV6120_CTRL14				0x0e
#define STV6120_CTRL15				0x0f
#define STV6120_CTRL16				0x10
#define STV6120_CTRL17				0x11
#define STV6120_STAT2				0x12
#define STV6120_CTRL18				0x13
#define STV6120_CTRL19				0x14
#define STV6120_CTRL20				0x15
#define STV6120_CTRL21				0x16
#define STV6120_CTRL22				0x17
#define STV6120_CTRL23				0x18

#define STV6120_MCLKDIV				STV6120_CTRL1
#define	STV6120_P1_MCLKDIV			STV6120_MCLKDIV
#define	STV6120_P2_MCLKDIV			STV6120_MCLKDIV
#define STV6120_S_MCLKDIV			0
#define STV6120_M_MCLKDIV			0x1

#define STV6120_ODIV				STV6120_CTRL1
#define	STV6120_P1_ODIV				STV6120_ODIV
#define	STV6120_P2_ODIV				STV6120_ODIV
#define STV6120_S_ODIV				1
#define STV6120_M_ODIV				0x1

#define STV6120_RDIV				STV6120_CTRL1
#define	STV6120_P1_RDIV				STV6120_RDIV
#define	STV6120_P2_RDIV				STV6120_RDIV
#define STV6120_S_RDIV				2
#define STV6120_M_RDIV				0x1

#define STV6120_K					STV6120_CTRL1
#define	STV6120_P1_K				STV6120_K
#define	STV6120_P2_K				STV6120_K
#define STV6120_S_K					3
#define STV6120_M_K					0x1f

#define STV6120_P1_BBGAIN			STV6120_CTRL2
#define STV6120_P2_BBGAIN			STV6120_CTRL11
#define STV6120_S_BBGAIN			0
#define STV6120_M_BBGAIN			0xf

#define STV6120_P1_REFOUTSEL		STV6120_CTRL2
#define STV6120_P2_REFOUTSEL		STV6120_CTRL11
#define STV6120_S_REFOUTSEL			4
#define STV6120_M_REFOUTSEL			0x1

#define STV6120_P1_SYN				STV6120_CTRL2
#define STV6120_P2_SYN				STV6120_CTRL11
#define STV6120_S_SYN				5
#define STV6120_M_SYN				0x1

#define STV6120_P1_SDOFF			STV6120_CTRL2
#define STV6120_P2_SDOFF			STV6120_CTRL11
#define STV6120_S_SDOFF				6
#define STV6120_M_SDOFF				0x1

#define STV6120_P1_DCLOOPOFF		STV6120_CTRL2
#define STV6120_P2_DCLOOPOFF		STV6120_CTRL11
#define STV6120_S_DCLOOPOFF			7
#define STV6120_M_DCLOOPOFF			0x1

#define STV6120_P1_N0				STV6120_CTRL3
#define STV6120_P2_N0				STV6120_CTRL12
#define STV6120_S_N0				0
#define STV6120_M_N0				0xff

#define STV6120_P1_N8				STV6120_CTRL4
#define STV6120_P2_N8				STV6120_CTRL13
#define STV6120_S_N8				0
#define STV6120_M_N8				0x1

#define STV6120_P1_F0				STV6120_CTRL4
#define STV6120_P2_F0				STV6120_CTRL13
#define STV6120_S_F0				1
#define STV6120_M_F0				0x7f

#define STV6120_P1_F7				STV6120_CTRL5
#define STV6120_P2_F7				STV6120_CTRL14
#define STV6120_S_F7				0
#define STV6120_M_F7				0xff

#define STV6120_P1_F15				STV6120_CTRL6
#define STV6120_P2_F15				STV6120_CTRL15
#define STV6120_S_F15				0
#define STV6120_M_F15				0x7

#define STV6120_P1_VCOILOW			STV6120_CTRL6
#define STV6120_P2_VCOILOW			STV6120_CTRL15
#define STV6120_S_VCOILOW			3
#define STV6120_M_VCOILOW			0x1

#define STV6120_P1_ICP				STV6120_CTRL6
#define STV6120_P2_ICP				STV6120_CTRL15
#define STV6120_S_ICP				4
#define STV6120_M_ICP				0x7

#define STV6120_P1_CF				STV6120_CTRL7
#define STV6120_P2_CF				STV6120_CTRL16
#define STV6120_S_CF				0
#define STV6120_M_CF				0x1f

#define STV6120_P1_PDIV				STV6120_CTRL7
#define STV6120_P2_PDIV				STV6120_CTRL16
#define STV6120_S_PDIV				5
#define STV6120_M_PDIV				0x3

#define STV6120_P1_RCCLFOFF			STV6120_CTRL7
#define STV6120_P2_RCCLFOFF			STV6120_CTRL16
#define STV6120_S_RCCLFOFF			7
#define STV6120_M_RCCLFOFF			0x1

#define STV6120_P1_CFHF				STV6120_CTRL8
#define STV6120_P2_CFHF				STV6120_CTRL17
#define STV6120_S_CFHF				0
#define STV6120_M_CFHF				0x1f

#define STV6120_P1_CALTIME			STV6120_CTRL8
#define STV6120_P2_CALTIME			STV6120_CTRL17
#define STV6120_S_CALTIME			5
#define STV6120_M_CALTIME			0x1

#define STV6120_TCAL				STV6120_CTRL17
#define	STV6120_P1_TCAL				STV6120_TCAL
#define	STV6120_P2_TCAL				STV6120_TCAL
#define STV6120_S_TCAL				6
#define STV6120_M_TCAL				0x3

#define STV6120_P1_LOCK				STV6120_STAT1
#define STV6120_P2_LOCK				STV6120_STAT2
#define STV6120_S_LOCK				0
#define STV6120_M_LOCK				0x1

#define STV6120_P1_CALRCSTRT		STV6120_STAT1
#define STV6120_P2_CALRCSTRT		STV6120_STAT2
#define STV6120_S_CALRCSTRT			1
#define STV6120_M_CALRCSTRT			0x1

#define STV6120_P1_CALVCOSTRT		STV6120_STAT1
#define STV6120_P2_CALVCOSTRT		STV6120_STAT2
#define STV6120_S_CALVCOSTRT		2
#define STV6120_M_CALVCOSTRT		0x1

#define STV6120_RFSEL1				STV6120_CTRL9
#define	STV6120_P1_RFSEL1			STV6120_RFSEL1
#define	STV6120_P2_RFSEL1			STV6120_RFSEL1
#define STV6120_S_RFSEL1			0
#define STV6120_M_RFSEL1			0x3

#define STV6120_RFSEL2				STV6120_CTRL9
#define	STV6120_P1_RFSEL2			STV6120_RFSEL2
#define	STV6120_P2_RFSEL2			STV6120_RFSEL2
#define STV6120_S_RFSEL2			2
#define STV6120_M_RFSEL2			0x3

#define STV6120_PATHON				STV6120_CTRL10
#define	STV6120_P1_PATHON			STV6120_PATHON
#define	STV6120_P2_PATHON			STV6120_PATHON
#define STV6120_S_PATHON			0
#define STV6120_M_PATHON			0x3

#define STV6120_LNAON				STV6120_CTRL10
#define	STV6120_P1_LNAON			STV6120_LNAON
#define	STV6120_P2_LNAON			STV6120_LNAON
#define STV6120_S_LNAON				2
#define STV6120_M_LNAON				0xf

#endif /* __STV6120_REG_H__ */

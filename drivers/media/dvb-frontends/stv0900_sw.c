/*
 * stv0900_sw.c
 *
 * Driver for ST STV0900 satellite demodulator IC.
 *
 * Copyright (C) ST Microelectronics.
 * Copyright (C) 2009 NetUP Inc.
 * Copyright (C) 2009 Igor M. Liplianin <liplianin@netup.ru>
 * Copyright (C) 2009-2016 Pendragon Sound
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "stv0900.h"
#include "stv0900_reg.h"
#include "stv0900_priv.h"

int stv0900_check_signal_presence(struct stv0900_demod *demod)
{
	s32	carr_offset, agc2_integr, max_carrier;

	int no_signal = 0;
	
	carr_offset = ge2comp(stv0900_read_be(demod, CFR2, 2), 16);
	agc2_integr = stv0900_read_be(demod, AGC2I1, 2);
	max_carrier = (s64) demod->srch_range * (11 * 65536) / (demod->mclk * 20LL);
	
	if (max_carrier > 0x4000)
		max_carrier = 0x4000;

	if (agc2_integr > 0x2000 || carr_offset < -2 * max_carrier || carr_offset > 2 * max_carrier)
		no_signal = 1;

	return no_signal;
}

static void stv0900_get_sw_loop_params(struct stv0900_demod *demod, s32 *frequency_inc, s32 *steps)
{
	s32 freq_inc, max_steps, srate, max_carrier;
	enum stv0900_standard standard;

	srate        = demod->symbol_rate;
	max_carrier  = demod->srch_range / 1000;
	max_carrier += max_carrier / 10;
	standard     = demod->standard;

	max_carrier  = 65536 * (max_carrier >> 1);
	max_carrier /= demod->mclk / 1000;

	if (max_carrier > 0x4000)
		max_carrier = 0x4000;

	freq_inc  = srate;
	freq_inc /= demod->mclk >> 10;
	freq_inc  = freq_inc << 6;

	switch (standard) {
		case STV0900_DVBS1_STANDARD:
		case STV0900_DSS_STANDARD:
			freq_inc *= 3;
			demod->demod_timeout = 20;
			break;
		case STV0900_DVBS2_STANDARD:
			freq_inc *= 4;
			demod->demod_timeout = 25;
			break;
		case STV0900_AUTO_STANDARD:
		default:
			freq_inc *= 3;
			demod->demod_timeout = 25;
			break;
	}

	freq_inc /= 100;

	if (freq_inc > max_carrier || freq_inc < 0)
		freq_inc = max_carrier >> 1;

	demod->demod_timeout *= 27500;

	if (srate > 0)
		demod->demod_timeout /= srate / 1000;

	if (demod->demod_timeout > 100 || demod->demod_timeout < 0)
		demod->demod_timeout = 100;

	max_steps = (max_carrier / freq_inc) + 1;

	if (max_steps > 100 || max_steps < 0) {
		max_steps = 100;
		freq_inc  = max_carrier / max_steps;
	}

	*frequency_inc = freq_inc;
	*steps         = max_steps;
}

static int stv0900_search_carr_sw_loop(struct stv0900_demod *demod, s32 FreqIncr, int zigzag, s32 MaxStep)
{
	struct stv0900_signal *result = &demod->result;

	int	no_signal, lock = 0;
	s32	stepCpt, freqOffset, max_carrier;

	max_carrier = demod->srch_range / 1000;
	max_carrier += (max_carrier / 10);

	max_carrier = 65536 * (max_carrier / 2);
	max_carrier /= demod->mclk / 1000;

	if (max_carrier > 0x4000)
		max_carrier = 0x4000;

	if (zigzag)
		freqOffset = 0;
	else
		freqOffset = -max_carrier + FreqIncr;

	stepCpt = 0;

	do {
		stv0900_write_reg(demod, DMDISTATE, 0x1c);
		stv0900_write_be(demod, CFRINIT1, freqOffset, 2);
		stv0900_write_reg(demod, DMDISTATE, 0x18);
		stv0900_write_bits(demod, ALGOSWRST, 1);

		if (demod->chip_id == 0x12) {
			stv0900_write_bits(demod, RST_HWARE, 1);
			stv0900_write_bits(demod, RST_HWARE, 0);
		}

		if (zigzag) {
			if (freqOffset >= 0)
				freqOffset = -freqOffset - 2 * FreqIncr;
			else
				freqOffset = -freqOffset;
		} else
			freqOffset += + 2 * FreqIncr;

		stepCpt++;
		
		lock      = stv0900_get_demod_lock(demod);
		no_signal = stv0900_check_signal_presence(demod);
		
		result->lock_timeout   = demod->demod_timeout;
		result->fine_lock_time = result->lock_time;

	} while (!lock && !no_signal && freqOffset - FreqIncr <  max_carrier && freqOffset + FreqIncr > -max_carrier && stepCpt < MaxStep);

	stv0900_write_bits(demod, ALGOSWRST, 0);

	return lock;
}

static int stv0900_sw_algo(struct stv0900_demod *demod)
{
	int	lock = 0, no_signal, zigzag;
	s32	s2fw, fqc_inc, trial_cntr, max_steps;

	stv0900_get_sw_loop_params(demod, &fqc_inc, &max_steps);
	
	switch (demod->standard) {
		case STV0900_DVBS1_STANDARD:
		case STV0900_DSS_STANDARD:
			if (demod->chip_id >= 0x20)
				stv0900_write_reg(demod, CARFREQ, 0x3b);
			else
				stv0900_write_reg(demod, CARFREQ, 0xef);

			stv0900_write_reg(demod, DMDCFGMD, 0x49);
			zigzag = 0;
			break;
		case STV0900_DVBS2_STANDARD:
			if (demod->chip_id >= 0x20)
				stv0900_write_reg(demod, CORRELABS, 0x79);
			else
				stv0900_write_reg(demod, CORRELABS, 0x68);

			stv0900_write_reg(demod, DMDCFGMD, 0x89);

			zigzag = 1;
			break;
		case STV0900_AUTO_STANDARD:
		default:
			if (demod->chip_id >= 0x20) {
				stv0900_write_reg(demod, CARFREQ, 0x3b);
				stv0900_write_reg(demod, CORRELABS, 0x79);
			} else {
				stv0900_write_reg(demod, CARFREQ, 0xef);
				stv0900_write_reg(demod, CORRELABS, 0x68);
			}

			stv0900_write_reg(demod, DMDCFGMD, 0xc9);
			zigzag = 0;
			break;
	}

	trial_cntr = 0;
	do {
		lock = stv0900_search_carr_sw_loop(demod, fqc_inc, zigzag, max_steps);
		no_signal = stv0900_check_signal_presence(demod);
		trial_cntr++;
		
		if (lock || no_signal || trial_cntr == 2) {
			if (demod->chip_id >= 0x20) {
				stv0900_write_reg(demod, CARFREQ, 0x49);
				stv0900_write_reg(demod, CORRELABS, 0x9e);
			} else {
				stv0900_write_reg(demod, CARFREQ, 0xed);
				stv0900_write_reg(demod, CORRELABS, 0x88);
			}

			if (stv0900_get_bits(demod, HEADER_MODE) == STV0900_DVBS2_FOUND && lock) {
				usleep_release(demod->demod_timeout * 1000);
				s2fw = stv0900_get_bits(demod, FLYWHEEL_CPT);

				if (s2fw < 0xd) {
					usleep_release(demod->demod_timeout * 1000);
					s2fw = stv0900_get_bits(demod, FLYWHEEL_CPT);
				}

				if (s2fw < 0xd) {
					lock = 0;

					if (trial_cntr < 2) {
						if (demod->chip_id >= 0x20)
							stv0900_write_reg(demod, CORRELABS, 0x79);
						else
							stv0900_write_reg(demod, CORRELABS, 0x68);

						stv0900_write_reg(demod, DMDCFGMD, 0x89);
					}
				}
			}
		}

	} while (!lock && trial_cntr < 2 && !no_signal);

	return lock;
}

static void stv0900_set_dvbs2_rolloff(struct stv0900_demod *demod)
{
	s32 rolloff;

	if (demod->chip_id == 0x10) {
		stv0900_write_bits(demod, MANUALSX_ROLLOFF, 1);
		rolloff = stv0900_read_reg(demod, MATSTR1) & 0x03;
		stv0900_write_bits(demod, ROLLOFF_CONTROL, rolloff);
	}
}

static u32 stv0900_carrier_width(u32 srate, enum stv0900_rolloff ro)
{
	u32 rolloff;

	switch (ro) {
		case STV0900_20:
			rolloff = 120;
			break;
		case STV0900_25:
			rolloff = 125;
			break;
		case STV0900_35:
		default:
			rolloff = 135;
			break;
	}

	return (u64) srate * rolloff / 100;
}

static int stv0900_check_timing_lock(struct stv0900_demod *demod)
{
	int timingLock = 0;
	s32	i, timingcpt = 0;
	u8  csave, tsave[2], tmp[2];

	csave = stv0900_read_reg(demod, CARFREQ);
	stv0900_read_regs(demod, TMGTHRISE, tsave, 2);
	
	tmp[0] = 0x20;
	tmp[1] = 0x0;
	
	stv0900_write_regs(demod, TMGTHRISE, tmp, 2);
	stv0900_write_bits(demod, CFR_AUTOSCAN, 0);
	
	tmp[0] = 0x80;
	tmp[1] = 0x40;
	
	stv0900_write_regs(demod, RTC, tmp, 2);
	stv0900_write_reg(demod, CARFREQ, 0x0);
	stv0900_write_be(demod, CFRINIT1, 0x0, 2);
	stv0900_write_reg(demod, AGC2REF, 0x65);
	stv0900_write_reg(demod, DMDISTATE, 0x18);
	usleep(7000);

	for (i = 0; i < 10; i++) {
		if (stv0900_get_bits(demod, TMGLOCK_QUALITY) >= 2)
			if (++timingcpt >= 3) {
				timingLock = 1;
				break;
			}

		usleep(1000);
	}

	stv0900_write_reg(demod, AGC2REF, 0x38);
	
	tmp[0] = 0x88;
	tmp[1] = 0x68;
	
	stv0900_write_regs(demod, RTC, tmp, 2);
	stv0900_write_reg(demod, CARFREQ, csave);
	stv0900_write_regs(demod, TMGTHRISE, tsave, 2);

	return timingLock;
}

static int stv0900_get_demod_cold_lock(struct stv0900_demod *demod)
{
	struct dvb_frontend *fe = &demod->frontend;
	struct tuner_state *tuner = &fe->tuner;
	struct stv0900_signal *result = &demod->result;

	int	lock = 0;
	s32	srate, search_range, carrier_step, nb_steps, current_step, direction;
	s32 tuner_freq, freq;

	srate        = demod->symbol_rate;
	search_range = demod->srch_range;

	if (srate >= 10000000) {
		lock = stv0900_get_demod_lock(demod);
		
		if (!lock) {
			if (stv0900_check_timing_lock(demod)) {
				stv0900_write_reg(demod, DMDISTATE, 0x1f);
				stv0900_write_reg(demod, DMDISTATE, 0x15);
				lock = stv0900_get_demod_lock(demod);
			}
		}
	} else {
		lock = stv0900_get_demod_lock(demod);
		if (!lock) {
			carrier_step = srate / 4;
			
			if (demod->chip_id <= 0x20) {
				if (srate >= 2000000) {
					demod->demod_timeout /= 3;
					
					if (demod->demod_timeout > 1000)
						demod->demod_timeout = 1000;
				} else
					demod->demod_timeout /= 2;
			}
			else
				demod->demod_timeout = (demod->demod_timeout * 3) / 4;
			
			nb_steps = search_range / carrier_step;

			if (nb_steps & 1)
				nb_steps++;

			if (nb_steps <= 0)
				nb_steps = 2;
			else if (nb_steps > 24)
				nb_steps = 24;

			current_step = 1;
			direction    = 1;

			if (demod->chip_id <= 0x20) {
				tuner_freq = tuner->frequency;
				demod->bw  = stv0900_carrier_width(demod->symbol_rate, demod->rolloff) << 1;
			} else
				tuner_freq = 0;

			while (!lock && current_step <= nb_steps) {
				tuner_freq += direction * current_step * carrier_step;

				if (demod->chip_id <= 0x20) {
					stv0900_set_tuner(demod, tuner_freq, demod->bw);
					
					stv0900_write_reg(demod, DMDISTATE, 0x1c);
					stv0900_write_be(demod, CFRINIT1, 0x0, 2);
					stv0900_write_reg(demod, DMDISTATE, 0x1f);
					stv0900_write_reg(demod, DMDISTATE, 0x15);
				} else {
					freq = ((s64) tuner_freq << 16) / demod->mclk;
					
					stv0900_write_reg(demod, DMDISTATE, 0x1c);
					stv0900_write_be(demod, CFRINIT1, freq, 2);
					stv0900_write_reg(demod, DMDISTATE, 0x1f);
					stv0900_write_reg(demod, DMDISTATE, 0x05);
				}

				lock = stv0900_get_demod_lock(demod);
				direction *= -1;
				current_step++;
				
				if (stv0900_check_early_exit(demod)) {
					lock = 0;
					break;
				}
			}
		}
	}
	
	result->lock_timeout   = demod->demod_timeout;
	result->fine_lock_time = result->lock_time;

	return lock;
}

static void stv0900_get_lock_timeout(struct stv0900_demod *demod, s32 srate)
{
	struct dtv_frontend_properties *c = &demod->frontend.dtv_property_cache;
	u8 level = c->lock_timeout_level;
	
	demod->demod_timeout = 500 + 2500000000LL / srate;
	demod->fec_timeout   = demod->demod_timeout >> 2;
	
	if (level == 0)
		level  = 5;

	if (level > 5) {
		if (level > 7)
			level = 7;
		
		demod->demod_timeout <<= level - 5;
	}
	else if (level < 5) {
		if (level < 3)
			level = 3;

		demod->demod_timeout >>= 5 - level;
	}
	
	c->lock_timeout_level = level;
}

static void stv0900_set_viterbi_acq(struct stv0900_demod *demod)
{
	u8 vth[6];
	
	vth[0] = 0x96;
	vth[1] = 0x64;
	vth[2] = 0x36;
	vth[3] = 0x23;
	vth[4] = 0x1e;
	vth[5] = 0x19;
	
	stv0900_write_regs(demod, VTH12, vth, 6);
}

static void stv0900_set_viterbi_tracq(struct stv0900_demod *demod)
{
	u8 vth[6];
	
	vth[0] = 0xd0;
	vth[1] = 0x7d;
	vth[2] = 0x53;
	vth[3] = 0x2f;
	vth[4] = 0x24;
	vth[5] = 0x1f;
	
	stv0900_write_regs(demod, VTH12, vth, 6);
}

static void stv0900_set_viterbi_standard(struct stv0900_demod *demod, enum stv0900_standard standard, enum stv0900_fec fec)
{
	switch (standard) {
		case STV0900_AUTO_STANDARD:
			dprintk("%s: ViterbiStandard Auto\n", __func__);
			stv0900_write_reg(demod, FECM, 0x10);
			stv0900_write_reg(demod, PRVIT, 0x3f);
			break;
		case STV0900_DVBS1_STANDARD:
			dprintk("%s: ViterbiStandard DVB-S\n", __func__);
			stv0900_write_reg(demod, FECM, 0x00);
			switch (fec) {
				case STV0900_FEC_UNKNOWN:
				default:
					stv0900_write_reg(demod, PRVIT, 0x2f);
					break;
				case STV0900_FEC_1_2:
					stv0900_write_reg(demod, PRVIT, 0x01);
					break;
				case STV0900_FEC_2_3:
					stv0900_write_reg(demod, PRVIT, 0x02);
					break;
				case STV0900_FEC_3_4:
					stv0900_write_reg(demod, PRVIT, 0x04);
					break;
				case STV0900_FEC_5_6:
					stv0900_write_reg(demod, PRVIT, 0x08);
					break;
				case STV0900_FEC_7_8:
					stv0900_write_reg(demod, PRVIT, 0x20);
					break;
			}
			break;
		case STV0900_DSS_STANDARD:
			dprintk("%s: ViterbiStandard DSS\n", __func__);
			stv0900_write_reg(demod, FECM, 0x80);
			switch (fec) {
				case STV0900_FEC_UNKNOWN:
				default:
					stv0900_write_reg(demod, PRVIT, 0x13);
					break;
				case STV0900_FEC_1_2:
					stv0900_write_reg(demod, PRVIT, 0x01);
					break;
				case STV0900_FEC_2_3:
					stv0900_write_reg(demod, PRVIT, 0x02);
					break;
				case STV0900_FEC_6_7:
					stv0900_write_reg(demod, PRVIT, 0x10);
					break;
			}
			break;
		default:
			dprintk("%s: ViterbiStandard None\n", __func__);
			break;
	}
}

static enum stv0900_fec stv0900_get_vit_fec(struct stv0900_demod *demod)
{
	enum stv0900_fec prate;
	s32 rate_fld = stv0900_get_bits(demod, VIT_CURPUN);

	switch (rate_fld) {
		case 13:
			prate = STV0900_FEC_1_2;
			break;
		case 18:
			prate = STV0900_FEC_2_3;
			break;
		case 21:
			prate = STV0900_FEC_3_4;
			break;
		case 24:
			prate = STV0900_FEC_5_6;
			break;
		case 25:
			prate = STV0900_FEC_6_7;
			break;
		case 26:
			prate = STV0900_FEC_7_8;
			break;
		default:
			prate = STV0900_FEC_UNKNOWN;
			break;
	}

	return prate;
}

static void stv0900_set_dvbs1_track_car_loop(struct stv0900_demod *demod, u32 srate)
{
	u8 abclc[2];
	
	if (demod->chip_id >= 0x30) {
		if (srate >= 15000000) {
			abclc[0] = 0x2b;
			abclc[1] = 0x1a;
		} 
		else if (srate >= 7000000) {
			abclc[0] = 0x0c;
			abclc[1] = 0x1b;
		} 
		else {
			abclc[0] = 0x2c;
			abclc[1] = 0x1c;
		}
	} else {
		abclc[0] = 0x1a;
		abclc[1] = 0x09;
	}

	stv0900_write_regs(demod, ACLC, abclc, 2);
}

static void stv0900_8psk_carrier_adaptation(struct stv0900_demod *demod)
{
	struct stv0900_signal *result = &demod->result;	
	enum stv0900_modcode modcode = result->modcode;
	
	u32 do_adapt = 0;
	s32 c_n;
	u8 aclc, nco2Th, abclc[2];
	
	if(demod->chip_id <= 0x20) { 
		if(result->standard == STV0900_DVBS2_STANDARD && result->pilot && modcode >= STV0900_8PSK_35 && modcode <= STV0900_8PSK_910) {
			c_n = stv0900_get_snr(demod);
			
			if (c_n >= 12288) /* 12 dB */
				do_adapt = 1;
			else if (c_n >= 11264) { /* 11 dB */
				if (modcode <= STV0900_8PSK_56)
					do_adapt = 1;
			}
			else if (c_n >= 10240) { /* 10 dB */
				if (modcode <= STV0900_8PSK_34)
					do_adapt = 1;
			}
			else if (c_n >= 8704) { /* 8.5 dB */
				if (modcode <= STV0900_8PSK_23)
					do_adapt = 1;
			}
			else if (c_n >= 7680) { /* 7.5 dB */
				if (modcode == STV0900_8PSK_35)
					do_adapt = 1;
			}
			
			if (do_adapt) {
				abclc[0] = 0x1a;
				abclc[1] = 0x09;
			}
			else {
				abclc[0] = 0x0;
				abclc[1] = 0x0;
			}
			
			stv0900_write_regs(demod, ACLC, abclc, 2);
		}
	}
	else {
		if (result->standard == STV0900_DVBS2_STANDARD) {
			nco2Th = modcode <= STV0900_QPSK_910 ? 0x30 : 0x20;
			
			stv0900_write_reg(demod, NCO2MAX1, 0);
			
			usleep(100000);
			
			if(stv0900_get_bits(demod, NCO2MAX1) >= nco2Th) {
				if (modcode >= STV0900_8PSK_35 && modcode <= STV0900_8PSK_910) {
					if (result->pilot) {
						c_n = stv0900_get_snr(demod);
						
						if (c_n > 10547) { /* 10.3 dB */
							stv0900_write_reg(demod, ACLC2S28, 0x3a);
							stv0900_write_reg(demod, BCLC2S28, 0x0a);
							stv0900_write_reg(demod, CAR2CFG, 0x76);
						}
						else {
							aclc = stv0900_get_optim_carr_loop(demod);
							
							stv0900_write_reg(demod, CAR2CFG, 0x66);
							stv0900_write_reg(demod, ACLC2S28, aclc);
							stv0900_write_reg(demod, BCLC2S28, 0x86);
						}
					}
					else {
						stv0900_write_reg(demod, ACLC2S28, 0x1b);
						stv0900_write_reg(demod, BCLC2S28, 0x8a);
					}
				}
			}
		}
	}
}

static void stv0900_track_optimization(struct stv0900_demod *demod)
{
	struct stv0900_signal *result = &demod->result;

	s32	aclc;
	u8 reg[2];

	switch (result->standard) {
		case STV0900_DVBS1_STANDARD:
		case STV0900_DSS_STANDARD:
			dprintk("%s: found DVB-S or DSS\n", __func__);
			if (demod->standard == STV0900_AUTO_STANDARD) {
				stv0900_write_bits(demod, DVBS1_ENABLE, 1);
				stv0900_write_bits(demod, DVBS2_ENABLE, 0);
			}

			if (demod->chip_id >= 0x30) {
				if (stv0900_get_vit_fec(demod) == STV0900_FEC_1_2) {
					reg[0] = 0x98;
					reg[1] = 0x18;
				} else {
					reg[0] = 0x18;
					reg[1] = 0x18;
				}
				
				stv0900_write_regs(demod, GAUSSR0, reg, 2);
			}
				
			stv0900_write_reg(demod, ERRCTRL1, 0x75);
			stv0900_set_dvbs1_track_car_loop(demod, result->symbol_rate);
			break;
		case STV0900_DVBS2_STANDARD:
			dprintk("%s: found DVB-S2\n", __func__);
			
			reg[0] = 0;
			reg[1] = 0;
			
			stv0900_write_regs(demod, ACLC, reg, 2);
			stv0900_write_bits(demod, DVBS1_ENABLE, 0);
			stv0900_write_bits(demod, DVBS2_ENABLE, 1);
			
			aclc = result->frame_len == STV0900_LONG_FRAME ? stv0900_get_optim_carr_loop(demod) : 
															 stv0900_get_optim_short_carr_loop(demod);

			dprintk("%s: DVB-S2 modcode %d pilot %d frame_len %x aclc %x\n", 
					__func__, result->modcode, result->pilot, result->frame_len, aclc);
				
			switch (result->modulation) {
				case STV0900_QPSK:
				default:
					stv0900_write_reg(demod, ACLC2S2Q, aclc);
					break;
				case STV0900_8PSK:
					stv0900_write_reg(demod, ACLC2S2Q, 0x2a);
					stv0900_write_reg(demod, ACLC2S28, aclc);
					break;
				case STV0900_16APSK:
					stv0900_write_reg(demod, ACLC2S2Q, 0x2a);
					stv0900_write_reg(demod, ACLC2S216A, aclc);
					break;
				case STV0900_32APSK:
					stv0900_write_reg(demod, ACLC2S2Q, 0x2a);
					stv0900_write_reg(demod, ACLC2S232A, aclc);
					break;
			}

			stv0900_write_reg(demod, ERRCTRL1, 0x67);
			
			if (demod->chip_id >= 0x30) {
				reg[0] = 0xac;
				reg[1] = 0x2c;
				
				stv0900_write_regs(demod, GAUSSR0, reg, 2);
			}
			else if (demod->chip_id <= 0x11) {
				if (demod->chip->demod_mode != STV0900_SINGLE)
					stv0900_activate_s2_modcod(demod);
			}
			break;
		case STV0900_UNKNOWN_STANDARD:
		default:
			dprintk("%s: found unknown standard\n", __func__);
			stv0900_write_bits(demod, DVBS1_ENABLE, 1);
			stv0900_write_bits(demod, DVBS2_ENABLE, 1);
			break;
	}

	if (demod->chip_id < 0x20) {
		stv0900_write_reg(demod, CARHDR, 0x08);

		if (demod->chip_id == 0x10)
			stv0900_write_reg(demod, CORRELEXP, 0x0a);
	}

	stv0900_write_reg(demod, AGC2REF, 0x38);
	stv0900_write_reg(demod, SFRUP1, 0x80);
	stv0900_write_reg(demod, SFRLOW1, 0x80);

	if (demod->chip_id >= 0x20)
		stv0900_write_reg(demod, CARFREQ, 0x49);

	if (result->standard == STV0900_DVBS1_STANDARD || result->standard == STV0900_DSS_STANDARD) {
		stv0900_set_viterbi_tracq(demod);
		
		if (demod->chip_id >= 0x20) {
			stv0900_write_reg(demod, VAVSRVIT, 0x0a);
			stv0900_write_reg(demod, VITSCALE, 0x0);
		}		
	}
	
	if (demod->chip_id >= 0x30)
		stv0900_write_reg(demod, CARHDR, 0x10);
	
	if (result->standard == STV0900_DVBS2_STANDARD)
		stv0900_8psk_carrier_adaptation(demod);
}

static int stv0900_get_fec_lock(struct stv0900_demod *demod)
{
	s32 timer = 0, lock = 0, state = 0;

	while (timer < demod->fec_timeout && !lock) {
		switch (state = stv0900_get_bits(demod, HEADER_MODE)) {
			case STV0900_DVBS_FOUND:
				lock = stv0900_get_bits(demod, LOCKEDVIT);
				break;
			case STV0900_DVBS2_FOUND:
				lock = stv0900_get_bits(demod, PKTDELIN_LOCK);
				break;
			case STV0900_SEARCH:
			case STV0900_PLH_DETECTED:
			default:
				lock = 0;
				break;
		}

		if (!lock) {
			usleep_release(10000);
			timer += 10;
		}
	}

	dprintk("FEC   LOCK %s state %d time %d timeout %d\n", lock ? "OK" : "FAIL", state, timer, demod->fec_timeout);

	return lock;
}

static int stv0900_wait_for_lock(struct stv0900_demod *demod)
{
	s32 timer = 0, lock;

	lock = stv0900_get_demod_lock(demod);

	if (lock)
		lock = stv0900_get_fec_lock(demod);

	if (lock && !stv0900_get_bits(demod, TSFIFO_LINEOK)) {
		for (lock = 0; timer < demod->fec_timeout && !lock; timer++) {
			usleep_release(1000);
			lock = stv0900_get_bits(demod, TSFIFO_LINEOK);
		}
	}

	dprintk("%s: %s timer %d timeout %d\n", __func__, lock ? "OK" : "FAIL", timer, demod->fec_timeout);

	return lock;
}

enum stv0900_standard stv0900_get_standard(struct stv0900_demod *demod)
{
	enum stv0900_standard fnd_standard;

	switch (stv0900_get_bits(demod, HEADER_MODE)) {
		case 1:
		case 2:
			fnd_standard = STV0900_DVBS2_STANDARD;
			break;
		case 3:
			if (stv0900_get_bits(demod, DSS_DVB))
				fnd_standard = STV0900_DSS_STANDARD;
			else
				fnd_standard = STV0900_DVBS1_STANDARD;

			break;
		default:
			fnd_standard = STV0900_UNKNOWN_STANDARD;
	}

	return fnd_standard;
}

static enum stv0900_signal_type stv0900_get_signal_params(struct stv0900_demod *demod)
{
	struct dvb_frontend *fe = &demod->frontend;
	struct tuner_state *tuner = &fe->tuner;
	struct stv0900_signal *result = &demod->result;
	enum stv0900_signal_type range = STV0900_OUTOFRANGE;
	s64 offsetFreq = 0;
	u8 timing, demod_type;
	int	i = 0;

	usleep(5000);
	
	if (demod->algo == STV0900_BLIND_SEARCH) {
		timing = stv0900_read_reg(demod, TMGREG2);
		stv0900_write_reg(demod, SFRSTEP, 0x5c);

		for (i = 0; i <= 50 && timing && timing != 0xff; i += 5) {
			timing = stv0900_read_reg(demod, TMGREG2);
			usleep_release(5000);
		}
	}

	result->standard = stv0900_get_standard(demod);
	result->frequency = tuner->frequency + stv0900_get_carr_freq(demod);
	
	result->symbol_rate = stv0900_get_symbol_rate_priv(demod);
	
	result->fec = stv0900_get_vit_fec(demod);
	result->modcode = stv0900_get_bits(demod, DEMOD_MODCOD);
	demod_type = stv0900_get_bits(demod, DEMOD_TYPE);
	result->pilot = demod_type & 0x01;
	result->frame_len = demod_type >> 1;
	result->rolloff = stv0900_get_bits(demod, ROLLOFF_STATUS);
	
	switch (result->standard) {
		case STV0900_DVBS2_STANDARD:
			result->spectrum = stv0900_get_bits(demod, SPECINV_DEMOD);
			if (result->modcode <= STV0900_QPSK_910)
				result->modulation = STV0900_QPSK;
			else if (result->modcode <= STV0900_8PSK_910)
				result->modulation = STV0900_8PSK;
			else if (result->modcode <= STV0900_16APSK_910)
				result->modulation = STV0900_16APSK;
			else if (result->modcode <= STV0900_32APSK_910)
				result->modulation = STV0900_32APSK;
			else
				result->modulation = STV0900_UNKNOWN;
			break;
		case STV0900_DVBS1_STANDARD:
		case STV0900_DSS_STANDARD:
			result->spectrum = stv0900_get_bits(demod, IQINV);
			result->modulation = STV0900_QPSK;
			break;
		default:
			result->modulation = STV0900_UNKNOWN;
			break;
	}

	offsetFreq = (s64) result->frequency - (s64) tuner->frequency;

	if (demod->algo == STV0900_BLIND_SEARCH)
		range = STV0900_RANGEOK;
	else {
		demod->freq = result->frequency;
	
		if (ABS(offsetFreq) * 2 < demod->srch_range + 500000)
			range = STV0900_RANGEOK;
	}

	printk(KERN_DEBUG "%s: range %d freq %llu offset %lld sr %d modcode %d rolloff %d pilot %d frame %d\n", 
			__func__, range, result->frequency, offsetFreq, result->symbol_rate, result->modcode, 
		    result->rolloff, result->pilot, result->frame_len);
	
	return range;
}

static enum stv0900_signal_type stv0900_dvbs1_acq_workaround(struct stv0900_demod *demod)
{
	enum stv0900_signal_type signal_type = STV0900_NODATA;

	s32	srate, freq, path = 0;

	demod->result.locked = 0;

	if (stv0900_get_bits(demod, HEADER_MODE) == STV0900_DVBS_FOUND) {
		srate = stv0900_get_symbol_rate_priv(demod);
		
		if (demod->algo == STV0900_BLIND_SEARCH)
			stv0900_set_symbol_rate(demod, srate);

		stv0900_get_lock_timeout(demod, srate);
		freq = stv0900_read_be(demod, CFR2, 2);
		stv0900_write_bits(demod, CFR_AUTOSCAN, 0);
		stv0900_write_bits(demod, SPECINV_CONTROL, STV0900_IQ_FORCE_SWAPPED);
		stv0900_write_reg(demod, DMDISTATE, 0x1c);
		stv0900_write_be(demod, CFRINIT1, freq, 2);
		stv0900_write_reg(demod, DMDISTATE, 0x18);
		
		if (stv0900_wait_for_lock(demod)) {
			demod->result.locked = 1;
			signal_type = stv0900_get_signal_params(demod);
			stv0900_track_optimization(demod);
			path = 1;
		} else {
			stv0900_write_bits(demod, SPECINV_CONTROL, STV0900_IQ_FORCE_NORMAL);
			stv0900_write_reg(demod, DMDISTATE, 0x1c);
			stv0900_write_be(demod, CFRINIT1, freq, 2);
			stv0900_write_reg(demod, DMDISTATE, 0x18);
			path = 2;
			
			if (stv0900_wait_for_lock(demod)) {
				demod->result.locked = 1;
				signal_type = stv0900_get_signal_params(demod);
				stv0900_track_optimization(demod);
				path = 3;
			}
		}
	} else
		demod->result.locked = 0;

	return signal_type;
}

static int stv0900_blind_check_agc2_min_level(struct stv0900_demod *demod)
{
	u32 agc2level, init_freq, freq_step, min = 0xffffffff;
	s32 i, j, nb_steps, direction, agc2_th;
	int over_thresh = 1;

	if (demod->chip_id <= 0x20)
		agc2_th = STV0900_BLIND_SEARCH_AGC2_TH << 2;
	else
		agc2_th = STV0900_BLIND_SEARCH_AGC2_TH_CUT30 << 2;
	
	stv0900_write_reg(demod, AGC2REF, 0x38);
	stv0900_write_bits(demod, SCAN_ENABLE, 0);
	stv0900_write_bits(demod, CFR_AUTOSCAN, 0);

	stv0900_write_bits(demod, AUTO_GUP, 1);
	stv0900_write_bits(demod, AUTO_GLOW, 1);

	stv0900_write_reg(demod, DMDT0M, 0x0);

	freq_step = demod->symbol_rate >> 1;
	
	if (freq_step > 1000000)
		freq_step = 1000000;
	
	stv0900_set_symbol_rate(demod, freq_step);
	nb_steps = demod->srch_range / freq_step;
	
	if (nb_steps <= 0)
		nb_steps = 1;

	direction = 1;

	freq_step = (freq_step << 8) / (demod->mclk >> 8);

	init_freq = 0;

	for (i = 0; i < nb_steps; i++) {
		if (direction > 0)
			init_freq = init_freq + (freq_step * i);
		else
			init_freq = init_freq - (freq_step * i);

		direction *= -1;
		stv0900_write_reg(demod, DMDISTATE, 0x5C);
		stv0900_write_be(demod, CFRINIT1, init_freq, 2);
		stv0900_write_reg(demod, DMDISTATE, 0x58);
		usleep(2000);
		agc2level = 0;

		for (j = 0; j < 4; j++)
			agc2level += stv0900_read_be(demod, AGC2I1, 2);
		
		if (min > agc2level)
			min = agc2level;
		
		if (agc2level < agc2_th) {
			over_thresh = 0;
			break;
		}
	}

	dprintk("%s: status %d step %d min %x th %x\n", __func__, over_thresh, i, min, agc2_th);
	stv0900_set_symbol_rate(demod, demod->symbol_rate);
	return over_thresh;
}

static void stv0900_setup_coarse_search(struct stv0900_demod *demod)
{
	u8 reg[2];

	stv0900_write_reg(demod, DMDISTATE, 0x5f);
	
	stv0900_write_reg(demod, TMGCFG, 0x12);
	stv0900_write_reg(demod, TMGCFG2, 0xc0);
	
	reg[0] = 0xf0;
	reg[1] = 0xe0;
	
	stv0900_write_regs(demod, TMGTHRISE, reg, 2);
	
	stv0900_write_bits(demod, CFR_AUTOSCAN, 1);
	stv0900_write_bits(demod, SCAN_ENABLE, 1);
	
	stv0900_write_bits(demod, AUTO_GUP, 1);
	stv0900_write_bits(demod, AUTO_GLOW, 1);
	
	stv0900_write_reg(demod, DMDT0M, 0x0);
	stv0900_write_reg(demod, AGC2REF, 0x50);
	
	if (demod->chip_id >= 0x30) {
		stv0900_write_reg(demod, CARFREQ, 0x99);
		stv0900_write_reg(demod, SFRSTEP, 0x98);
	} 
	else if (demod->chip_id >= 0x20) {
		stv0900_write_reg(demod, CARFREQ, 0x6a);
		stv0900_write_reg(demod, SFRSTEP, 0x95);
	}
	else {
		stv0900_write_reg(demod, CARFREQ, 0xed);
		stv0900_write_reg(demod, SFRSTEP, 0x73);
	}
	
	stv0900_write_bits(demod, S1S2_SEQUENTIAL, 0);
	
	reg[0] = 0x88;
	reg[1] = 0x44;
	
	stv0900_write_regs(demod, RTC, reg, 2);
}

static int stv0900_search_coarse(struct stv0900_demod *demod, s32 init_cf, u32 init_sr)
{
	struct dvb_frontend *fe = &demod->frontend;
	struct tuner_state *tuner = &fe->tuner;
	struct dvb_frontend_ops *ops = &demod->frontend.ops;
	struct stv0900_signal *result = &demod->result;
	
	s64 sum_cf2, sum_sr2;
	s32 cf, sr, cfl[8], srl[8], sum_cf, sum_sr, ave_cf, ave_sr, prev_sr, del_sr;
	s32 i, j, timingcpt, cfr, cfj, srj;
	s32 agc2_th, agc2_integr, sfr, coarse_sr = 0;
	
	if (demod->chip_id >= 0x30)
		agc2_th = 0x2e00;
	else
		agc2_th = 0x1f00;
	
	cfr = ((s64) init_cf << 16) / demod->mclk;
	sfr = ((s64) init_sr << 16) / demod->mclk;
	
	stv0900_write_reg(demod, DMDISTATE, 0x5f);
	stv0900_write_be(demod, CFRINIT1, cfr, 2);
	stv0900_write_be(demod, SFRINIT1, sfr, 2);
	stv0900_write_reg(demod, DMDISTATE, (cfr || sfr) ? 0x41 : 0x40);
	
	memset(cfl, 0, 8 * sizeof(s32));
	memset(srl, 0, 8 * sizeof(s32));
	
	sum_cf  = 0;
	sum_sr  = 0;
	sum_cf2 = 0;
	sum_sr2 = 0;
	prev_sr = 0;
	
	for (i = 0; i < 50; i++) {
		usleep(1000);
		
		cf = stv0900_get_carr_freq(demod);
		sr = stv0900_get_symbol_rate_priv(demod);
		
		j = i & 0x7;
		
		cfj = cfl[j];
		srj = srl[j];
		
		sum_cf += cf - cfj;
		sum_sr += sr - srj;
		
		sum_cf2 += (s64) cf * (s64) cf - (s64) cfj * (s64) cfj;
		sum_sr2 += (s64) sr * (s64) sr - (s64) srj * (s64) srj;
		
		cfl[j] = cf;
		srl[j] = sr;
		
		ave_cf = sum_cf >> 3;
		ave_sr = sum_sr >> 3;
		
		del_sr = ave_sr - prev_sr;
		
		if (i > 12 && ABS(del_sr) < ave_sr >> 4)
			break;
		
		prev_sr = ave_sr;
	}
	
	result->coarse_cf_mean = ave_cf;
	result->coarse_cf_sdev = stv0900_sqrt64((sum_cf2 << 3) - (s64) sum_cf * (s64) sum_cf) >> 3;
	result->coarse_sr_mean = ave_sr;
	result->coarse_sr_sdev = stv0900_sqrt64((sum_sr2 << 3) - (s64) sum_sr * (s64) sum_sr) >> 3;
	
	timingcpt   = 0;
	agc2_integr = 0;
	
	for (j = 0; j < 10; j++) {
		if (stv0900_get_bits(demod, TMGLOCK_QUALITY) >= 2)
			timingcpt++;
		
		agc2_integr += stv0900_read_be(demod, AGC2I1, 2);
	}
	
	agc2_integr /= 10;
	
	dprintk("coarse: time %d start %d cf %d %d sr %d %d timing %d agc2 %d\n", i, init_cf, ave_cf, result->coarse_cf_sdev, 
			ave_sr, result->coarse_sr_sdev, timingcpt, agc2_integr);
	
	if (stv0900_check_early_exit(demod)) {
		dprintk("%s: early exit\n", __func__);
		coarse_sr = -1;
	}
	else if (timingcpt >= 5 && agc2_integr < agc2_th && ave_sr >= ops->info.symbol_rate_min && ave_sr <= ops->info.symbol_rate_max)
		coarse_sr = ave_sr;
	
	result->frequency = tuner->frequency + result->coarse_cf_mean;
	
	return coarse_sr;
}

static int stv0900_search_srate_coarse(struct stv0900_demod *demod, s32 demod_if, s32 srch_range)
{
	s32 i, n, step, direction, coarse_sr;
	
	step = demod->symbol_rate;
	n    = srch_range / step + 1;

	direction = 1;
	coarse_sr = 0;
	
	for (i = 0; !coarse_sr && i < n; i++) {
		demod_if  += direction * i * step;
		direction *= -1;
		coarse_sr  = stv0900_search_coarse(demod, demod_if, step);
	}

	return coarse_sr;
}

static void stv0900_search_srate_fine(struct stv0900_demod *demod)
{
	struct stv0900_signal *result = &demod->result;

	s32	coarse_freq;
	u8 reg[2];
	
	coarse_freq = ((s64) result->coarse_cf_mean << 16) / demod->mclk;
	
	stv0900_write_reg(demod, DMDISTATE, 0x1f);
	stv0900_write_reg(demod, TMGCFG2, 0xc1);

	reg[0] = 0x20;
	reg[1] = 0x0;
	
	stv0900_write_regs(demod, TMGTHRISE, reg, 2);
	stv0900_write_reg(demod, TMGCFG, 0xd2);
	stv0900_write_bits(demod, CFR_AUTOSCAN, 0);
	stv0900_write_reg(demod, AGC2REF, 0x38);

	if (demod->chip_id >= 0x30)
		stv0900_write_reg(demod, CARFREQ, 0x79);
	else if (demod->chip_id >= 0x20)
		stv0900_write_reg(demod, CARFREQ, 0x49);
	else
		stv0900_write_reg(demod, CARFREQ, 0xed);
	
	stv0900_set_symbol_rate_range(demod, result->coarse_sr_mean, 30);
	
	stv0900_write_reg(demod, DMDT0M, 0x20);
	stv0900_write_be(demod, CFRINIT1, coarse_freq, 2);
	stv0900_write_reg(demod, DMDISTATE, 0x18);
	
	dprintk("fine: freq %llu sr %u min %u max %u\n", result->frequency, 
			result->coarse_sr_mean, result->coarse_sr_mean * 7 / 10, result->coarse_sr_mean * 13 / 10);
}

static void stv0900_set_search_standard(struct stv0900_demod *demod)
{
	const char* str;
	u8 reg[2];
	
	if (stvdebug) {
		switch (demod->standard) {
			case STV0900_DVBS1_STANDARD:
				str = "DVB-S";
				break;
			case STV0900_DSS_STANDARD:
				str = "DSS";
				break;
			case STV0900_DVBS2_STANDARD:
				str = "DVB-S2";
				break;
			case STV0900_AUTO_STANDARD:
			default:
				str = "AUTO";
				break;
		}

		printk(KERN_DEBUG "%s: %s\n", __func__, str);
	}
	
	switch (demod->standard) {
		case STV0900_DVBS1_STANDARD:
		case STV0900_DSS_STANDARD:
			stv0900_write_bits(demod, DVBS1_ENABLE, 1);
			stv0900_write_bits(demod, DVBS2_ENABLE, 0);
			stv0900_write_bits(demod, STOP_CLKVIT, 0);
			stv0900_set_dvbs1_track_car_loop(demod, demod->symbol_rate);
			stv0900_write_reg(demod, CAR2CFG, 0x22);

			stv0900_set_viterbi_acq(demod);
			stv0900_set_viterbi_standard(demod, demod->standard, demod->fec);
			break;
		case STV0900_DVBS2_STANDARD:
			stv0900_write_bits(demod, DVBS1_ENABLE, 0);
			stv0900_write_bits(demod, DVBS2_ENABLE, 1);
			stv0900_write_bits(demod, STOP_CLKVIT, 1);
				
			reg[0] = 0;
			reg[1] = 0;
				
			stv0900_write_regs(demod, ACLC, reg, 2);

			if (demod->chip_id <= 0x20) /*cut 1.x and 2.0*/
				stv0900_write_reg(demod, CAR2CFG, 0x26);
			else
				stv0900_write_reg(demod, CAR2CFG, 0x66);
			
			if (demod->chip->demod_mode == STV0900_DUAL) {
				if (demod->chip_id <= 0x11)
					stv0900_stop_all_s2_modcod(demod, demod->index);
				else
					stv0900_activate_s2_modcod(demod);
				
			} else
				stv0900_activate_s2_modcod_single(demod, demod->index);
			
			stv0900_set_viterbi_tracq(demod);			
			break;
		case STV0900_AUTO_STANDARD:
		default:
			stv0900_write_bits(demod, DVBS1_ENABLE, 1);
			stv0900_write_bits(demod, DVBS2_ENABLE, 1);
			stv0900_write_bits(demod, STOP_CLKVIT, 0);
			stv0900_set_dvbs1_track_car_loop(demod, demod->symbol_rate);
			
			if (demod->chip_id <= 0x20)
				stv0900_write_reg(demod, CAR2CFG, 0x26);
			else
				stv0900_write_reg(demod, CAR2CFG, 0x66);
				
			if (demod->chip->demod_mode == STV0900_DUAL) {
				if (demod->chip_id <= 0x11)
					stv0900_stop_all_s2_modcod(demod, demod->index);
				else
					stv0900_activate_s2_modcod(demod);
					
			} else
				stv0900_activate_s2_modcod_single(demod, demod->index);
				
			if (demod->symbol_rate >= 2000000)
				stv0900_set_viterbi_acq(demod);
			else
				stv0900_set_viterbi_tracq(demod);
			
			stv0900_set_viterbi_standard(demod, demod->standard, demod->fec);
			break;
	}
}

static enum stv0900_signal_type stv0900_algo_setup(struct stv0900_demod *demod)
{
	struct dtv_frontend_properties *c = &demod->frontend.dtv_property_cache;
	struct stv0900_blindscan *blind = &demod->blind;
	
	s32 i, aq_power, agc1_power, save_bw;
	u8 iq[2];
	
	enum stv0900_algo algo;
	enum stv0900_signal_type signal_type = STV0900_NOCARRIER;
	
	algo    = demod->algo;
	save_bw = demod->bw;
	blind->coarse_init_done = 0;

	stv0900_write_bits(demod, RST_HWARE, 1);
	stv0900_write_reg(demod, DMDISTATE, 0x5c);
	
	if (demod->chip_id >= 0x20) {
		if (demod->symbol_rate > 5000000)
			stv0900_write_reg(demod, CORRELABS, 0x9e);
		else
			stv0900_write_reg(demod, CORRELABS, 0x82);
	} else
		stv0900_write_reg(demod, CORRELABS, 0x88);

	if (demod->algo == STV0900_BLIND_SEARCH) {
		stv0900_write_reg(demod, TMGCFG2, 0xc0);
		stv0900_write_reg(demod, CORRELMANT, 0x70);

		stv0900_set_symbol_rate_bounds(demod, c->scan_sr_min, demod->symbol_rate,  c->scan_sr_max);
	}
	else {
		stv0900_write_reg(demod, DMDT0M, 0x20);
		stv0900_write_reg(demod, TMGCFG, 0xd2);

		if (demod->symbol_rate < 2000000)
			stv0900_write_reg(demod, CORRELMANT, 0x63);
		else
			stv0900_write_reg(demod, CORRELMANT, 0x70);

		stv0900_write_reg(demod, AGC2REF, 0x38);

		demod->bw = stv0900_carrier_width(demod->symbol_rate, demod->rolloff);
		
		if (demod->chip_id >= 0x20) {
			stv0900_write_reg(demod, KREFTMG, 0x5a);

			if (demod->algo == STV0900_COLD_START)
				demod->bw = (demod->bw + demod->srch_range) * 15 / 10;
			else if (demod->algo == STV0900_WARM_START)
				demod->bw += demod->srch_range;
		}
		else {
			stv0900_write_reg(demod, KREFTMG, 0xc1);
			demod->bw = (demod->bw + demod->srch_range) * 15 / 10;
		}
		
		stv0900_write_reg(demod, TMGCFG2, 0x01);
		
		stv0900_set_symbol_rate_range(demod, demod->symbol_rate, 5);
	}

	if (blind->skip_tune)
		demod->bw = save_bw;
	else {
		stv0900_set_tuner(demod, demod->freq, demod->bw);
		
		agc1_power = stv0900_read_be(demod, AGCIQIN1, 2);
		aq_power   = 0;

		if (agc1_power == 0) {
			for (i = 0; i < 5; i++) {
				stv0900_read_regs(demod, POWERI, iq, 2);
				aq_power += (u32) iq[0] + (u32) iq[1];
			}
		}
		
		if (agc1_power == 0 && aq_power < IQPOWER_THRESHOLD * 10) {
			demod->result.locked = 0;
			dprintk("%s: NO AGC1, POWERI, POWERQ\n", __func__);
			return STV0900_NOAGC1;
		}
	}

	stv0900_write_reg(demod, DEMOD, 0x8);
	stv0900_set_search_standard(demod);

	if (demod->algo != STV0900_BLIND_SEARCH)
		stv0900_start_search(demod);

	return signal_type;
}

static enum stv0900_signal_type stv0900_get_signal_type(struct stv0900_demod *demod, enum stv0900_signal_type signal_type)
{
	struct stv0900_signal *result = &demod->result;
	
	int no_signal = 0;
	u8 tscfgh, pdelctrl1, tstres0;
	
	if (signal_type == STV0900_RANGEOK) {
		/* stv0900_get_signal_params must be called before stv0900_get_signal_type */
		stv0900_track_optimization(demod);
		
		tscfgh = stv0900_read_reg(demod, TSCFGH) & ~0x01;
		stv0900_write_reg(demod, TSCFGH, tscfgh | 0x01);
		
		if (result->standard == STV0900_DVBS2_STANDARD) {
			tstres0   = stv0900_read_reg(demod, R0900_TSTRES0) & ~0x80;
			pdelctrl1 = stv0900_read_reg(demod, PDELCTRL1) & ~0x01;

			stv0900_write_reg(demod, R0900_TSTRES0, tstres0 | 0x80);
			stv0900_write_reg(demod, PDELCTRL1, pdelctrl1 | 0x01);
			stv0900_write_reg(demod, R0900_TSTRES0, tstres0);
			stv0900_write_reg(demod, PDELCTRL1, pdelctrl1);
		}

		stv0900_write_reg(demod, TSCFGH, tscfgh);
		
		result->locked       = stv0900_wait_for_lock(demod);
		result->status       = stv0900_get_status(demod);
		result->cnr          = stv0900_get_snr(demod);
		result->signal_level = stv0900_get_signal_level(demod);
		
		if (result->locked) {
			if (result->standard == STV0900_DVBS2_STANDARD) {
				stv0900_set_dvbs2_rolloff(demod);
				stv0900_write_bits(demod, RESET_UPKO_COUNT, 1);
				stv0900_write_bits(demod, RESET_UPKO_COUNT, 0);
				stv0900_write_reg(demod, ERRCTRL1, 0x67);
			}
			else
				stv0900_write_reg(demod, ERRCTRL1, 0x75);

			stv0900_write_reg(demod, FBERCPT4, 0);
			stv0900_write_reg(demod, ERRCTRL2, 0xc1);
		} else {
			signal_type = STV0900_NODATA;
			no_signal = stv0900_check_signal_presence(demod);
			result->locked = 0;
		}
	}
	
	if (signal_type == STV0900_NODATA && !no_signal) {
		if (demod->chip_id <= 0x11) {
			if (stv0900_get_bits(demod, HEADER_MODE) == STV0900_DVBS_FOUND && demod->iq_inv <= STV0900_IQ_AUTO_NORMAL_FIRST)
				signal_type = stv0900_dvbs1_acq_workaround(demod);
		}
		else
			result->locked = 0;
	}

	return signal_type;
}

static enum stv0900_signal_type stv0900_optimize_bandwidth(struct stv0900_demod *demod, u32 bw_increment)
{
	struct stv0900_signal *result = &demod->result;
	
	enum stv0900_signal_type signal_type = STV0900_NOCARRIER;
	enum stv0900_algo saved_algo  = STV0900_BLIND_SEARCH;
	
	u8 saved_scan_mode;
	s32 saved_search_range;
	
	saved_algo         = demod->algo;
	saved_scan_mode    = demod->scan_mode;
	saved_search_range = demod->srch_range;
	
	demod->srch_range  = bw_increment;
	demod->freq        = result->frequency;
	demod->symbol_rate = result->symbol_rate;
	demod->rolloff     = result->rolloff;
	demod->fec         = STV0900_FEC_UNKNOWN;
	demod->iq_inv      = STV0900_IQ_AUTO;
	demod->algo        = STV0900_WARM_START;
	demod->scan_mode   = 0;
	
	signal_type = stv0900_algo(demod);
	
	demod->algo       = saved_algo;
	demod->scan_mode  = saved_scan_mode;
	demod->srch_range = saved_search_range;
	
	return signal_type;
}

#define BadCFsdev     2000000
#define BadSRsdev     2000000
#define UnknownDelta  500000
#define MaxCoarsePass 3
#define MaxTimingStep 5

static enum stv0900_signal_type stv0900_blind_search_algo(struct stv0900_demod *demod, s32 demod_if, s32 init_sr, int jump_dir)
{
	struct stv0900_signal *result = &demod->result;
	struct stv0900_blindscan *blind = &demod->blind;
	enum stv0900_signal_type signal_type = STV0900_NOCARRIER;
	struct dvb_frontend *fe = &demod->frontend;
	struct tuner_state *tuner = &fe->tuner;
	struct dtv_frontend_properties *c = &fe->dtv_property_cache;
	
	int	lock = 0, strikes = 0, unknowns = 0, jump = 0;
	s64 cf, cf_sum, cf_ave, cfs[MaxCoarsePass * MaxTimingStep];
	s32	i, j, n, fail_cpt, agc2_overflow, sr = 0, step, dir, agc2_th, saved_timeout, saved_locktime;
	s32 sr_sum, sr_ave, min_sr, max_sr, delta, max_delta, min_cf, max_cf, left, coarse_passes;
	s32 med_sr, limit_cf, limit_sr, k_ref_tmg, srs[MaxCoarsePass * MaxTimingStep];
	u16 agc2_int;
	u8	dstatus2, fast_lock;
	
	fast_lock     = c->algo == ALGO_FAST;
	coarse_passes = (fast_lock || !c->lock_extended_attempts) ? 1 : MaxCoarsePass;
	
	result->lock_attempts = 0;
	
	cf_sum   = 0;
	sr_sum   = 0;	
	min_cf   = blind->min_cf;
	max_cf   = blind->max_cf;
	min_sr   = blind->min_sr * 7 >> 3;
	max_sr   = blind->max_sr * 9 >> 3;
	limit_cf = blind->min_cf || blind->max_cf;
	limit_sr = blind->min_sr || blind->max_sr;
	
	if (jump_dir) {
		blind->jump = 0;	
		jump = jump_dir >= 0 ? 0x7fffffff : 0x80000000;
	}
	
	if (demod->chip_id == 0x10)
		stv0900_write_reg(demod, CORRELEXP, 0xaa);
	
	if (demod->chip_id <= 0x20) {
		stv0900_write_reg(demod, CARCFG, 0xc4);
		agc2_th = STV0900_BLIND_SEARCH_AGC2_TH;
	} else {
		stv0900_write_reg(demod, CARCFG, 0x06);
		agc2_th = STV0900_BLIND_SEARCH_AGC2_TH_CUT30;
	}
	
	if (demod->chip_id >= 0x20) {
		stv0900_write_reg(demod, CARHDR, 0x20);
		stv0900_write_reg(demod, EQUALCFG, 0x41);
		stv0900_write_reg(demod, FFECFG, 0x41);
		stv0900_write_reg(demod, VITSCALE, 0x82);
		stv0900_write_reg(demod, VAVSRVIT, 0x0);
	}
	else
		stv0900_write_reg(demod, CARHDR, 0x55);
	
	for (j = 0; j < coarse_passes && sr >= 0; j++) {
		if (demod->chip_id >= 0x20) {
			k_ref_tmg = (120 + 40) / 2;
			step      = 20;
			n         = (120 - 40) / step | 1;
		} else {
			k_ref_tmg = (233 + 153) / 2;
			step      = 20;
			n         = (233 - 153) / step | 1;
		}
		
		n   = fast_lock ? 3 : n;
		dir = -1;
		
		for (i = 0; i < n && !stv0900_check_early_exit(demod); i++) {			
			k_ref_tmg += i * dir * step;
			dir       *= -1;
			
			result->timing_ref = k_ref_tmg;
			stv0900_write_reg(demod, KREFTMG, k_ref_tmg);
			
			dprintk("%s: step %d k_ref_tmg %d pass %d\n", __func__, i, k_ref_tmg, j);
			
			if (!blind->coarse_init_done) {
				stv0900_setup_coarse_search(demod);
				stv0900_reset_symbol_rates(demod);
				
				blind->coarse_init_done = 1;
			}
			
			sr = blind->srch_range && !fast_lock ? stv0900_search_srate_coarse(demod, demod_if, blind->srch_range) : 
									               stv0900_search_coarse(demod, demod_if, init_sr);
			cf = result->coarse_cf_mean;
			
			if (sr > 0) {
				if ((limit_cf && (cf < min_cf || cf > max_cf)) || (limit_sr && (sr < min_sr || sr > max_sr)))
					continue;
				
				if (result->coarse_cf_sdev > BadCFsdev || result->coarse_sr_sdev > BadSRsdev) {
					/* don't waste time on grossly bad measurements */
					strikes++;
					dprintk("Bad coarse cf %lld %d sr %d %d rf %llu bw %d strike %d\n", 
							cf, result->coarse_cf_sdev, sr, result->coarse_sr_sdev, tuner->frequency, tuner->bandwidth, strikes);
					
					if (strikes < 3)
						continue;
					else
						break;
				}
				
				if (j == 0) {
					result->lock_attempts++;
					blind->coarse_init_done = 0;
					stv0900_search_srate_fine(demod);				
					stv0900_get_lock_timeout(demod, sr);
					lock = stv0900_get_demod_lock(demod);
					
					result->lock_timeout   = demod->demod_timeout;
					result->fine_lock_time = result->lock_time;					
					
					if (lock) {
						signal_type = stv0900_get_signal_params(demod);
						cf = (s64) result->frequency - (s64) tuner->frequency;
						sr = result->symbol_rate;
						
						if ((!limit_cf || (cf >= min_cf && cf <= max_cf)) && (!limit_sr || (sr >= min_sr && sr <= max_sr))) {
							demod->standard = result->standard;
							saved_timeout   = result->lock_timeout;
							saved_locktime  = result->fine_lock_time;
							
							if (blind->center_tune && !fast_lock) {
								signal_type = stv0900_optimize_bandwidth(demod, 5000000);
								dprintk("Result %d freq %llu sr %d\n", signal_type, result->frequency, result->symbol_rate);
							}
							else
								signal_type = stv0900_get_signal_type(demod, signal_type);
							
							result->lock_timeout   = saved_timeout;
							result->fine_lock_time = saved_locktime;												
							return STV0900_RANGEOK;
						}

						return STV0900_OUTOFRANGE;
					}
				}

				/* while there was no lock, there appears to be energy here. keep track in case this happens again */
				cf += tuner->frequency;
				
				cf_sum       += cf;
				sr_sum       += sr;
				cfs[unknowns] = cf;
				srs[unknowns] = sr;
				unknowns++;
				
				cf = (s64) result->bad_lock_freq - (s64) tuner->frequency;
				
				if (jump * jump_dir > cf * jump_dir)
					jump = cf;
			}
			else if (sr == 0) {
				/* no meaningful coarse measurements; check if there is any indication of energy here */
				fail_cpt      = 0;
				agc2_overflow = 0;
				
				for (i = 0; i < 10; i++) {
					agc2_int = stv0900_read_be(demod, AGC2I1, 2);
					
					if (agc2_int >= 0xff00 && ++agc2_overflow > 7)
						break;
					
					dstatus2 = stv0900_read_reg(demod, DSTATUS2);
					
					if ((dstatus2 & 0x1) && (dstatus2 >> 7) && ++fail_cpt > 7)
						break;
				}
				
				if (i < 10) {
					sr = -1;
					break;
				}
			}
			else
				break;
		}
	}
	
	if (sr >= 0 && unknowns >= 3 && !fast_lock) {
		/* consider what to do with a series of bad lock attempts within passband */
		result->timing_ref = 0;
		cf_ave = cf_sum / unknowns;
		sr_ave = 0;
		
		max_delta = 0;
		left      = 0;
		
		/* prune cf outliers */
		for (i = 0; i < unknowns; i++) {
			cf = cfs[i];
			sr = srs[i];
			
			delta = cf_ave - cf;
			delta = ABS(delta);
			
			if (max_delta < delta)
				max_delta = delta;
			
			if (delta > UnknownDelta) {
				cf_sum -= cf;
				sr_sum -= sr;
			}
			else
				srs[left++] = sr;
		}
		
		dprintk("Unknowns %d left %d cf_ave %lld max delta %d\n", unknowns, left, cf_ave, max_delta);
		
		if (left >= 3) {
			cf_ave = cf_sum / left;
			sr_ave = sr_sum / left;
			
			cf = cf_ave - (s64) tuner->frequency - demod_if;
			
			if (jump_dir * cf > init_sr) {
				/* this is a blindscan and there's a signal off-center that the demod can't lock - try centering it */
				demod->freq       = cf_ave;
				demod->srch_range = sr_ave;
				blind->srch_range = sr_ave;
				blind->jump       = cf + jump_dir * (stv0900_carrier_width(sr_ave, STV0900_20));
				
				stv0900_algo_setup(demod);
				
				return stv0900_blind_search_algo(demod, 0, init_sr, 0);
			}
			
			stv0900_shellsort(srs, left);
			
			n        = left >> 1;
			min_sr   = srs[n & ~1];
			max_sr   = srs[n];
			med_sr   = (min_sr + max_sr) >> 1;
			limit_sr = med_sr + (med_sr * 5 >> 6);
			
			/* prune sr outliers */
			for (i = left - 1; i > n; i--) {
				sr = srs[i];
				
				if (sr > limit_sr) {
					left--;
					sr_sum -= sr;
				}
				else if (max_sr < sr) {
					max_sr = sr;
					break;
				}
			}
			
			n       &= ~1;
			limit_sr = med_sr - (med_sr * 5 >> 6);
			
			for (i = 0; i < n; i++) {
				sr = srs[i];
				
				if (sr < limit_sr) {
					left--;
					sr_sum -= sr;
				}
				else if (min_sr > sr) {
					min_sr = sr;
					break;
				}
			}
			
			dprintk("Left %d cf %lld med sr %d min %d max %d\n", left, cf_ave, med_sr, min_sr, max_sr);
			
			if (left >= 3 && c->lock_extended_attempts) {
				/* looks like this is a consistent detection; try manual sr search */
				sr_ave = sr_sum / left;
				
//				sr   = (min_sr + max_sr) >> 1;
				sr   = sr_ave;
				step = sr >> 7;
				n    = (max_sr - min_sr) / step | 1;
				dir  = 1;
				
				if (n > c->lock_extended_attempts)
					n = c->lock_extended_attempts;
				
				stv0900_set_tuner(demod, cf_ave, stv0900_carrier_width(max_sr, STV0900_35) + 5000000);
				
				blind->skip_tune = 1;
				
				for (i = 0; i < n; i++) {
					sr  += dir * i * step;
					dir *= -1;
					
					result->lock_attempts++;
					
					result->frequency   = cf_ave;
					result->symbol_rate = sr;
					result->rolloff     = STV0900_35;
					result->standard    = STV0900_UNKNOWN_STANDARD;
					
					dprintk("Manual step %d freq %lld sr %d\n", i, cf_ave, sr);
					
					if (stv0900_optimize_bandwidth(demod, 5000000) == STV0900_RANGEOK || result->standard != STV0900_UNKNOWN_STANDARD) {
						signal_type = STV0900_RANGEOK;
						break;
					}
					
					if (stv0900_check_early_exit(demod))
						break;
				}
				
				blind->skip_tune = 0;
			}
			
			if (signal_type != STV0900_RANGEOK) {
				/* appears to be a real signal with a modulation that demod can't handle */
				result->locked       = 0;
				result->status       = stv0900_get_status(demod);
				result->standard     = STV0900_UNKNOWN_STANDARD;
				result->frequency    = cf_ave;
				result->symbol_rate  = sr_ave;
				result->modulation   = STV0900_MODCODE_UNKNOWN;
				result->rolloff      = STV0900_35;
				result->signal_level = stv0900_get_signal_level(demod);
				
				dprintk("Unknown modulation: freq %lld sr %d max_delta %d range %d %d num %d\n", 
						cf_ave, sr_ave, max_delta, min_sr, max_sr, unknowns);
			}
			
			return STV0900_RANGEOK;
		}
	}
	
	if (jump_dir)
		blind->jump = (jump_dir > 0 && jump < 0x7fffffff) || (jump_dir < 0 && jump > 0x80000000) ? jump - demod_if : 0;
	
	return STV0900_NOCARRIER;
}

#define TunerBW      (72 * 1000000)
#define SpectrumEdge (30 * 1000000)

static enum stv0900_signal_type stv0900_blind_scan_algo(struct stv0900_demod *demod)
{
	struct dvb_frontend *fe = &demod->frontend;
	struct tuner_state *tuner = &fe->tuner;
	struct dtv_frontend_properties *c = &fe->dtv_property_cache;
	struct stv0900_signal *result = &demod->result;
	struct dvb_frontend_ops *ops = &demod->frontend.ops;
	struct stv0900_blindscan *blind = &demod->blind;
	
	enum stv0900_signal_type signal_type;
	u64 start, stop, freq_min, freq_max, tuner_rf;
	s32 step, offs, min_sr, max_sr, srch_skip;
	s32 srch_range, demod_if, dir;
	int new_tuner_rf = 0, lock = 0;
	
	dprintk("%s scanning %llu %llu %llu\n", __func__, c->scan_freq_start, c->scan_freq_stop, c->scan_freq_step);
	
	if (!c->scan_active) {
		c->scan_active       = 1;
		c->scan_freq_current = c->scan_freq_start;
	}
	
	start    = c->scan_freq_current;
	stop     = c->scan_freq_stop;
	step     = c->scan_freq_step;
	min_sr   = c->scan_sr_min;
	max_sr   = c->scan_sr_max;
	freq_min = ops->info.frequency_min;
	freq_max = ops->info.frequency_max;
	dir      = step > 0 ? 1 : -1;
	
	c->lock_extended_attempts = 0;
	srch_range = SpectrumEdge * 2 - stv0900_carrier_width(c->scan_sr_max, demod->rolloff);
	
	if (srch_range < 0)
		srch_range = 0;
	
	offs      = srch_range >> 1;
	tuner_rf  = (s64) start - dir * srch_range;
	srch_skip = dir * srch_range - step;
	
	demod->symbol_rate = min_sr;
	demod->srch_range  = srch_range;
	
	for (; dir * start <= stop * dir; start += step) {
		if (dir > 0) {
			if (start > freq_max)
				start = freq_max;
			
			if (start > tuner_rf + offs) {
				tuner_rf = start + offs;
				new_tuner_rf = 1;
				
				if (tuner_rf > freq_max)
					tuner_rf = freq_max;
			}
		}
		else {
			if (start < freq_min)
				start = freq_min;
			
			if (start < tuner_rf - offs) {
				tuner_rf = start - offs;
				new_tuner_rf = 1;
				
				if (tuner_rf < freq_min)
					tuner_rf = freq_min;
			}
		}
		
		if (new_tuner_rf) {
			if (stv0900_check_early_exit(demod)) {
				start = stop;
				break;
			}
			
			demod->freq = tuner_rf;
			demod->bw   = TunerBW;
			
			signal_type = stv0900_algo_setup(demod);
			
			if (signal_type != STV0900_NOCARRIER) {
				c->scan_freq_current = start + dir * srch_range;
				c->frequency   = 0;
				c->symbol_rate = 0;
				return signal_type;
			}
			
			tuner_rf     = tuner->frequency;
			new_tuner_rf = 0;
			
			dprintk("Scan tuner to %llu\n", tuner_rf);
			
			if (stv0900_blind_check_agc2_min_level(demod)) {
				start += srch_skip;
				continue;
			}
		}
		
		demod_if = (s64) start - (s64) tuner_rf;
		
		if (dir > 0) {
			blind->min_cf = demod_if - step * 3 / 4;
			blind->max_cf = SpectrumEdge;
		}
		else {
			blind->min_cf = -SpectrumEdge;
			blind->max_cf = demod_if - step * 3 / 4;
		}

		blind->min_sr      = min_sr;
		blind->max_sr      = max_sr;
		blind->srch_range  = 0;
		blind->center_tune = 1;
		
		if ((signal_type = stv0900_blind_search_algo(demod, demod_if, min_sr, dir)) == STV0900_RANGEOK) {
			lock = 1;
			break;
		}
		
		tuner_rf     = tuner->frequency;
		blind->jump -= step;
		
		if (blind->jump * dir > 0) {
			dprintk("Jump %d step %d\n", blind->jump, step);
			start += blind->jump;
		}
	}
	
	if (lock)
		start = result->frequency + dir * stv0900_carrier_width(result->symbol_rate, result->rolloff) / 2;
	else {
		result->frequency   = 0;
		result->symbol_rate = 0;
		c->frequency        = 0;
		c->symbol_rate      = 0;
		c->scan_active      = 0;
		signal_type         = STV0900_OUTOFRANGE;
	}
	
	c->scan_freq_current = start;
	
	return signal_type;
}

/* 
 * this was a late 2009 attempt to get the autonomous chip blindscanning to work, 
 * and is only a snapshot of one particular experiment. it likely does not work 
 * well unmodified, if at all. when the effort was shelved, the scanning was slow 
 * and missed signals.the code below is here only for reference purposes, and to 
 * enable any future work to start from a similar point.
 */
static int stv0900_blind_scan_algo_aep(struct stv0900_demod *demod)
{
	struct dvb_frontend *fe = &demod->frontend;
	struct tuner_state *tuner = &fe->tuner;
	struct dtv_frontend_properties *c = &fe->dtv_property_cache;
	
	int lastRF = 0, thisRF, thisCNT, stat, dstate, flyw;
	int sr,cf,roll,modcode,type,fec, i, early_exit;
	u8 reg[2];
	u64 r;
	
	if (!demod->tuner_auto) {
		dprintk("%s: aep requires auto tuner\n", __func__);
		return STV0900_NOCARRIER;
	}
	
	stv0900_write_reg(demod, DMDISTATE, 0x5f);
	
	stv0900_write_reg(demod, DEMOD, 0x8);
	stv0900_write_reg(demod, CARHDR, 0x20);
	stv0900_write_reg(demod, CARCFG, 0xc4);
	stv0900_write_reg(demod, RTCS2, 0x44);
	stv0900_write_reg(demod, EQUALCFG, 0x41);
	stv0900_write_reg(demod, FFECFG, 0x41);
	stv0900_write_reg(demod, VITSCALE, 0x82);
	stv0900_write_reg(demod, VAVSRVIT, 0x0);
	stv0900_write_reg(demod, KREFTMG, 110);
	
	stv0900_write_reg(demod, TMGCFG, 0x12);
	stv0900_write_reg(demod, TMGCFG2, 0xc0);
	
	reg[0] = 0xf0;
	reg[1] = 0xe0;
	
	stv0900_write_regs(demod, TMGTHRISE, reg, 2);
	
	stv0900_write_bits(demod, AUTO_GUP, 1);
	stv0900_write_bits(demod, AUTO_GLOW, 1);
	
	stv0900_write_reg(demod, DMDT0M, 0x10);
	//stv0900_write_reg(demod, DMDT0M, 0x0); stops after first lock?
	stv0900_write_reg(demod, AGC2REF, 0x50);
	
	stv0900_write_reg(demod, CARFREQ, 0x6a);
	stv0900_write_reg(demod, SFRSTEP, 0x95);
	
	stv0900_write_bits(demod, S1S2_SEQUENTIAL, 0);
	
	stv0900_write_reg(demod, DMDCFGMD, 0xd9);
	stv0900_write_reg(demod, DMDCFG3, 0x00);
	
	stv0900_write_reg(demod, DMDRESCFG, 0x80);
	stv0900_write_reg(demod, DMDRESCFG, 0x00);
	
	stv0900_write_be(demod, CFRINIT1, 0, 2);
	
	stv0900_set_symbol_rate_bounds(demod, c->scan_sr_min, 1000000,  c->scan_sr_max);
	
	stv0900_write_reg(demod, DMDISTATE, 0x1b);
	
	for (i = 0;; i++) {
		thisCNT = stv0900_read_reg(demod, DMDRESADR);
		thisRF  = tuner->frequency;
		stat    = stv0900_read_reg(demod, DSTATUS);
		dstate  = stv0900_read_reg(demod, DMDSTATE);
		flyw    = stv0900_read_reg(demod, DMDFLYW);
		
		if (lastRF != thisRF) {
			dprintk("step %d RF %d CNT %d stat %x state %x flyw %x\n", i, thisRF, thisCNT, stat, dstate, flyw);
			lastRF = thisRF;
		}
		
		if(thisCNT & 0xf) {
			r = stv0900_read_be64(demod, DMDRESDATA7, 8);			
			stv0900_write_reg(demod, DMDRESDATA0, 0);
			
			if (r & 0x080000) {
				//	note conversion from samples is mclk * samples / 2^16
				sr  = (demod->mclk * (u64) ((r >> 48) & 0xffff)) >> 16;
				cf  = demod->mclk * (s64) ge2comp((r >> 32) & 0xffff, 16) / 65536000LL;
				cf += lastRF;
				roll = (r >> 16) & 0x3;
		
				if (r & 0x80) {
				//	DVB-S2
					modcode = (r >> 2) & 0x1f;
					type = r & 0x3;
					dprintk("DVB-S2 cf %d sr %d modc %d roll %d type %d\n",cf,sr,modcode,roll,type);
				}
				else if (r & 0x20) {
					fec = r & 0x7;
					
					if (r & 0x8) {
					//	DSS
						dprintk("DSS cf %d sr %d fec %d roll %d\n",cf,sr,fec,roll);
					}
					else {
					//	DVB-S
						dprintk("DVB-S cf %d sr %d fec %d roll %d\n",cf,sr,fec,roll);
					}
				}
				else{
					//undefined
					dprintk("Unknown cf %d sr %d\n",cf,sr);
				}
			}
		}
		
		early_exit = (i & 0xff) == 0 && stv0900_check_early_exit(demod);
		
		if (thisRF > c->scan_freq_stop || demod->frontend.dtv_property_cache.state == DTV_RETUNE || early_exit) {
			stv0900_write_reg(demod, DMDISTATE, 0x5f);
			dprintk("Scan exit\n");
			break;
		}
		
		usleep_release(10000);
	}

	return STV0900_NOCARRIER;
}

enum stv0900_signal_type stv0900_algo(struct stv0900_demod *demod)
{
	struct stv0900_signal *result = &demod->result;
	struct stv0900_blindscan *blind = &demod->blind;
	struct dtv_frontend_properties *c = &demod->frontend.dtv_property_cache;
	
	enum stv0900_signal_type signal_type = STV0900_NOCARRIER;
	int lock = 0;
	
	dprintk("%s\n", __func__);
	
	if (demod->scan_mode) {
		c->single_tune = 1;
		
		switch (c->algo) {
			case ALGO_BLINDSCAN:
			default:
				signal_type = stv0900_blind_scan_algo(demod);
				break;

			case ALGO_EXPERIMENTAL:
				signal_type = stv0900_blind_scan_algo_aep(demod);
				break;
				
		}
	}
	else {
		if (c->algo == ALGO_BLIND)
			demod->bw = 2 * 36000000;
		
		signal_type = stv0900_algo_setup(demod);
		
		if (signal_type != STV0900_NOCARRIER)
			return signal_type;
		
		stv0900_get_lock_timeout(demod, demod->symbol_rate);
		
		switch (demod->algo) {
			case STV0900_COLD_START:
				lock = stv0900_get_demod_cold_lock(demod);
				
				if (!lock && demod->symbol_rate >= 10000000) {
					if (stv0900_check_timing_lock(demod))
						lock = stv0900_sw_algo(demod);
				}
				
				if (lock) {
					signal_type = stv0900_get_signal_params(demod);
					signal_type = stv0900_optimize_bandwidth(demod, 5000000);
				}
				break;
				
			case STV0900_WARM_START:
				if (demod->demod_timeout > 1000)
					demod->demod_timeout = 1000;
				
				lock = stv0900_get_demod_lock(demod);
				
				result->lock_timeout   = demod->demod_timeout;
				result->fine_lock_time = result->lock_time;
				
				if (lock) {
					signal_type = stv0900_get_signal_params(demod);
					signal_type = stv0900_get_signal_type(demod, signal_type);
				}
				break;
				
			case STV0900_BLIND_SEARCH:
			default:
				blind->min_cf      = 0;
				blind->max_cf      = 0;
				blind->min_sr      = 0;
				blind->max_sr      = 0;
				blind->srch_range  = demod->srch_range;
				blind->center_tune = 1;
				
				if (!stv0900_blind_check_agc2_min_level(demod))
					signal_type = stv0900_blind_search_algo(demod, 0, demod->symbol_rate, 0);
				break;
		}
	}
	
	return signal_type;
}

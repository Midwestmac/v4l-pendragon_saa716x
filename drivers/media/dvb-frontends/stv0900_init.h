/*
 * stv0900_init.h
 *
 * Driver for ST STV0900 satellite demodulator IC.
 *
 * Copyright (C) ST Microelectronics.
 * Copyright (C) 2009 NetUP Inc.
 * Copyright (C) 2009 Igor M. Liplianin <liplianin@netup.ru>
 * Copyright (C) 2009-2016 Pendragon Sound
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef STV0900_INIT_H
#define STV0900_INIT_H

#include "stv0900_priv.h"

#define ENTRY(A,B)	{ ((A) * 1024 + 5) / 10, (B) * 16 }

/* DVBS1 and DSS C/N Look-Up table */
static const struct stv0900_point stv0900_s1_cn[] = {
	ENTRY(-1000,	   65536	),
	ENTRY(   00,		8917	),  /*C/N=0.0dB*/
	ENTRY(   05,		8801	),  /*C/N=0.5dB*/
	ENTRY(   10,		8667	),  /*C/N=1.0dB*/
	ENTRY(   15,		8522	),  /*C/N=1.5dB*/
	ENTRY(   20,		8355	),  /*C/N=2.0dB*/
	ENTRY(   25,		8175	),  /*C/N=2.5dB*/
	ENTRY(   30,		7979	),  /*C/N=3.0dB*/
	ENTRY(   35,		7763	),  /*C/N=3.5dB*/
	ENTRY(   40,		7530	),  /*C/N=4.0dB*/
	ENTRY(   45,		7282	),  /*C/N=4.5dB*/
	ENTRY(   50,		7026	),  /*C/N=5.0dB*/
	ENTRY(   55,		6781	),  /*C/N=5.5dB*/
	ENTRY(   60,		6514	),  /*C/N=6.0dB*/
	ENTRY(   65,		6241	),  /*C/N=6.5dB*/
	ENTRY(   70,		5965	),  /*C/N=7.0dB*/
	ENTRY(   75,		5690	),  /*C/N=7.5dB*/
	ENTRY(   80,		5424	),  /*C/N=8.0dB*/
	ENTRY(   85,		5161	),  /*C/N=8.5dB*/
	ENTRY(   90,		4902	),  /*C/N=9.0dB*/
	ENTRY(   95,		4654	),  /*C/N=9.5dB*/
	ENTRY(  100,		4417	),  /*C/N=10.0dB*/
	ENTRY(  105,		4186	),  /*C/N=10.5dB*/
	ENTRY(  110,		3968	),  /*C/N=11.0dB*/
	ENTRY(  115,		3757	),  /*C/N=11.5dB*/
	ENTRY(  120,		3558	),  /*C/N=12.0dB*/
	ENTRY(  125,		3366	),  /*C/N=12.5dB*/
	ENTRY(  130,		3185	),  /*C/N=13.0dB*/
	ENTRY(  135,		3012	),  /*C/N=13.5dB*/
	ENTRY(  140,		2850	),  /*C/N=14.0dB*/
	ENTRY(  145,		2698	),  /*C/N=14.5dB*/
	ENTRY(  150,		2550	),  /*C/N=15.0dB*/
	ENTRY(  160,		2283	),  /*C/N=16.0dB*/
	ENTRY(  170,		2042	),  /*C/N=17.0dB*/
	ENTRY(  180,		1827	),  /*C/N=18.0dB*/
	ENTRY(  190,		1636	),  /*C/N=19.0dB*/
	ENTRY(  200,		1466	),  /*C/N=20.0dB*/
	ENTRY(  210,		1315	),  /*C/N=21.0dB*/
	ENTRY(  220,		1181	),  /*C/N=22.0dB*/
	ENTRY(  230,		1064	),  /*C/N=23.0dB*/
	ENTRY(  240,		 960	),  /*C/N=24.0dB*/
	ENTRY(  250,		 869	),  /*C/N=25.0dB*/
	ENTRY(  260,		 792	),  /*C/N=26.0dB*/
	ENTRY(  270,		 724	),  /*C/N=27.0dB*/
	ENTRY(  280,		 665	),  /*C/N=28.0dB*/
	ENTRY(  290,		 616	),  /*C/N=29.0dB*/
	ENTRY(  300,		 573	),  /*C/N=30.0dB*/
	ENTRY(  310,		 537	),  /*C/N=31.0dB*/
	ENTRY(  320,		 507	),  /*C/N=32.0dB*/
	ENTRY(  330,		 483	),  /*C/N=33.0dB*/
	ENTRY(  400,		 398	),  /*C/N=40.0dB*/
	ENTRY(  450,		 381	),  /*C/N=45.0dB*/
	ENTRY(  500,		 377	),	/*C/N=50.0dB*/
	ENTRY( 1000,		  -1	)
};

/* DVBS2 C/N Look-Up table */
static const struct stv0900_point stv0900_s2_cn[] = {
	ENTRY(-1000,	   65536	),
	ENTRY(  -30,	   13348	), /*C/N=-3dB*/
	ENTRY(  -20,	   12640	), /*C/N=-2dB*/
	ENTRY(  -10,	   11883	), /*C/N=-1dB*/
	ENTRY(   00,	   11101	), /*C/N=-0dB*/
	ENTRY(   05,	   10718	), /*C/N=0.5dB*/
	ENTRY(   10,	   10339	), /*C/N=1.0dB*/
	ENTRY(   15,	    9947	), /*C/N=1.5dB*/
	ENTRY(   20,	    9552	), /*C/N=2.0dB*/
	ENTRY(   25,	    9183	), /*C/N=2.5dB*/
	ENTRY(   30,		8799	), /*C/N=3.0dB*/
	ENTRY(   35,		8422	), /*C/N=3.5dB*/
	ENTRY(   40,		8062	), /*C/N=4.0dB*/
	ENTRY(   45,		7707	), /*C/N=4.5dB*/
	ENTRY(   50,		7353	), /*C/N=5.0dB*/
	ENTRY(   55,		7025	), /*C/N=5.5dB*/
	ENTRY(   60,		6684	), /*C/N=6.0dB*/
	ENTRY(   65,		6331	), /*C/N=6.5dB*/
	ENTRY(   70,		6036	), /*C/N=7.0dB*/
	ENTRY(   75,		5727	), /*C/N=7.5dB*/
	ENTRY(   80,		5437	), /*C/N=8.0dB*/
	ENTRY(   85,		5164	), /*C/N=8.5dB*/
	ENTRY(   90,		4902	), /*C/N=9.0dB*/
	ENTRY(   95,		4653	), /*C/N=9.5dB*/
	ENTRY(  100,		4408	), /*C/N=10.0dB*/
	ENTRY(  105,		4187	), /*C/N=10.5dB*/
	ENTRY(  110,		3961	), /*C/N=11.0dB*/
	ENTRY(  115,		3751	), /*C/N=11.5dB*/
	ENTRY(  120,		3558	), /*C/N=12.0dB*/
	ENTRY(  125,		3368	), /*C/N=12.5dB*/
	ENTRY(  130,		3191	), /*C/N=13.0dB*/
	ENTRY(  135,		3017	), /*C/N=13.5dB*/
	ENTRY(  140,		2862	), /*C/N=14.0dB*/
	ENTRY(  145,		2710	), /*C/N=14.5dB*/
	ENTRY(  150,		2565	), /*C/N=15.0dB*/
	ENTRY(  160,		2300	), /*C/N=16.0dB*/
	ENTRY(  170,		2058	), /*C/N=17.0dB*/
	ENTRY(  180,		1849	), /*C/N=18.0dB*/
	ENTRY(  190,		1663	), /*C/N=19.0dB*/
	ENTRY(  200,		1495	), /*C/N=20.0dB*/
	ENTRY(  210,		1349	), /*C/N=21.0dB*/
	ENTRY(  220,		1222	), /*C/N=22.0dB*/
	ENTRY(  230,		1110	), /*C/N=23.0dB*/
	ENTRY(  240,		1011	), /*C/N=24.0dB*/
	ENTRY(  250,		 925	), /*C/N=25.0dB*/
	ENTRY(  260,		 853	), /*C/N=26.0dB*/
	ENTRY(  270,		 789	), /*C/N=27.0dB*/
	ENTRY(  280,		 734	), /*C/N=28.0dB*/
	ENTRY(  290,		 690	), /*C/N=29.0dB*/
	ENTRY(  300,		 650	), /*C/N=30.0dB*/
	ENTRY(  310,		 619	), /*C/N=31.0dB*/
	ENTRY(  320,		 593	), /*C/N=32.0dB*/
	ENTRY(  330,		 571	), /*C/N=33.0dB*/
	ENTRY(  400,		 498	), /*C/N=40.0dB*/
	ENTRY(  450,		 484	), /*C/N=45.0dB*/
	ENTRY(  500,		 481	), /*C/N=50.0dB*/
	ENTRY( 1000,		  -1	)
};

/* RF level Look-Up table for STB6100 */
static const struct stv0900_point stv0900_level_6100[] = {
	{   8,	0xFFFF }, 
	{   7,	0xE280 }, 
	{   6,	0xE270 }, 
	{   5,	0xE050 }, 
	{   4,	0xDD80 }, 
	{   3,	0xDB10 }, 
	{   2,	0xD8C0 }, 
	{   1,	0xD700 }, 
	{   0,	0xD522 }, 
	{  -1,	0xD380 }, 
	{  -2,	0xD1D0 }, 
	{  -3,	0xD060 }, 
	{  -4,	0xCEE0 }, 
	{  -5,	0xCD60 }, 
	{  -6,	0xCBF0 }, 
	{  -7,	0xCA80 }, 
	{  -8,	0xC900 }, 
	{  -9,	0xC7B8 }, 
	{ -10,	0xC670 }, 
	{ -11,	0xC510 }, 
	{ -12,	0xC3B6 }, 
	{ -13,	0xC260 }, 
	{ -14,	0xC10F }, 
	{ -15,	0xBFBF }, 
	{ -16,	0xBE60 }, 
	{ -17,	0xBD00 }, 
	{ -18,	0xBB90 }, 
	{ -19,	0xBA30 }, 
	{ -20,	0xB8D0 }, 
	{ -21,	0xB760 }, 
	{ -22,	0xB5E0 }, 
	{ -23,	0xB458 }, 
	{ -24,	0xB2C0 }, 
	{ -25,	0xB140 }, 
	{ -26,	0xAF90 }, 
	{ -27,	0xADE0 }, 
	{ -28,	0xAC10 }, 
	{ -29,	0xAA37 }, 
	{ -30,	0xA830 }, 
	{ -31,	0xA610 }, 
	{ -32,	0xA3D8 }, 
	{ -33,	0xA160 }, 
	{ -34,	0x9EC0 }, 
	{ -35,	0x9BE8 }, 
	{ -36,	0x9890 }, 
	{ -37,	0x9540 }, 
	{ -38,	0x9150 }, 
	{ -39,	0x8CE0 }, 
	{ -40,	0x8870 }, 
	{ -41,	0x82F0 }, 
	{ -42,	0x7CF0 }, 
	{ -43,	0x7640 }, 
	{ -44,	0x6EE0 }, 
	{ -45,	0x6650 }, 
	{ -46,	0x5D80 }, 
	{ -47,	0x5660 }, 
	{ -48,	0x5060 }, 
	{ -49,	0x4B60 }, 
	{ -50,	0x4738 }, 
	{ -51,	0x4440 }, 
	{ -52,	0x40F0 }, 
	{ -53,	0x3DE0 }, 
	{ -54,	0x3B00 }, 
	{ -55,	0x3870 }, 
	{ -56,	0x35F0 }, 
	{ -57,	0x3390 }, 
	{ -58,	0x3150 }, 
	{ -59,	0x2EF0 }, 
	{ -60,	0x2CA0 }, 
	{ -61,	0x2A80 }, 
	{ -62,	0x2840 }, 
	{ -63,	0x25FF }, 
	{ -64,	0x2380 }, 
	{ -65,	0x20C8 }, 
	{ -66,      -1 } 
};

/* RF level via I&Q Look-Up table for STB6100 */
static const struct stv0900_point stv0900_iq_6100[] = {
	{ -63,	512 }, 
	{ -64,	320 }, 
	{ -65,	300 }, 
	{ -66,	280 }, 
	{ -67,	250 }, 
	{ -68,	226 }, 
	{ -69,	200 }, 
	{ -70,	180 }, 
	{ -71,	160 }, 
	{ -72,	142 }, 
	{ -73,	130 }, 
	{ -74,	114 }, 
	{ -75,	100 }, 
	{ -76,	 92 }, 
	{ -77,	 82 }, 
	{ -78,	 72 }, 
	{ -79,	 68 }, 
	{ -80,	 62 }, 
	{ -81,	 56 }, 
	{ -82,	 50 }, 
	{ -83,	 45 }, 
	{ -84,	 41 }, 
	{ -85,	 37 }, 
	{ -86,	 33 }, 
	{ -87,	 29 }, 
	{ -88,	 27 }, 
	{-100,	 -1 }
};

/* RF level Look-Up table for STV6110 */
static const struct stv0900_point stv0900_level_6110[] = {
	{  13,	0xFFFF }, 
	{  12,	0xCFA0 }, 
	{  11,	0xCE7F }, 
	{  10,	0xCD5E }, 
	{   9,	0xCC20 }, 
	{   8,	0xCAF0 }, 
	{   7,	0xC9C0 }, 
	{   6,	0xC880 }, 
	{   5,	0xC74D }, 
	{   4,	0xC610 }, 
	{   3,	0xC4D0 }, 
	{   2,	0xC3A0 }, 
	{   1,	0xC350 }, 
	{   0,	0xC11F }, 
	{  -1,	0xBFF0 }, 
	{  -2,	0xBEB0 }, 
	{  -3,	0xBD70 }, 
	{  -4,	0xBBC0 }, 
	{  -5,	0xBAA0 }, 
	{  -6,	0xB970 }, 
	{  -7,	0xB83F }, 
	{  -8,	0xB6FF }, 
	{  -9,	0xB5AF }, 
	{ -10,	0xB45F }, 
	{ -11,	0xB3AF }, 
	{ -12,	0xB1D6 }, 
	{ -13,	0xB07F }, 
	{ -14,	0xAF21 }, 
	{ -15,	0xADC0 }, 
	{ -16,	0xAC6A }, 
	{ -17,	0xAAEE }, 
	{ -18,	0xA9A0 }, 
	{ -19,	0xA83F }, 
	{ -20,	0xA6C8 }, 
	{ -21,	0xA554 }, 
	{ -22,	0xA3DD }, 
	{ -23,	0xA270 }, 
	{ -24,	0xA0F0 }, 
	{ -25,	0x9F70 }, 
	{ -26,	0x9DD8 }, 
	{ -27,	0x9C50 }, 
	{ -28,	0x9AC0 }, 
	{ -29,	0x990F }, 
	{ -30,	0x9770 }, 
	{ -31,	0x95C0 }, 
	{ -32,	0x93F0 }, 
	{ -33,	0x923E }, 
	{ -34,	0x906E }, 
	{ -35,	0x8E88 }, 
	{ -36,	0x8C90 }, 
	{ -37,	0x8A90 }, 
	{ -38,	0x8855 }, 
	{ -39,	0x8630 }, 
	{ -40,	0x8400 }, 
	{ -41,	0x81A0 }, 
	{ -42,	0x7F40 }, 
	{ -43,	0x7CB0 }, 
	{ -44,	0x7A18 }, 
	{ -45,	0x7777 }, 
	{ -46,	0x7498 }, 
	{ -47,	0x7180 }, 
	{ -48,	0x6E90 }, 
	{ -49,	0x6B20 }, 
	{ -50,	0x6760 }, 
	{ -51,	0x6340 }, 
	{ -52,	0x5EC0 }, 
	{ -53,	0x59D0 }, 
	{ -54,	0x5400 }, 
	{ -55,	0x4DB0 }, 
	{ -56,	0x4580 }, 
	{ -57,	0x39C0 }, 
	{ -58,	0x2460 }, 
	{ -59,	    -1 }
};

/* RF level via I&Q Look-Up table for STV6110 */
static const struct stv0900_point stv0900_iq_6110[] = {
	{ -56,	512 }, 
	{ -57,	316 }, 
	{ -58,	296 }, 
	{ -59,	281 }, 
	{ -60,	151 }, 
	{ -61,	224 }, 
	{ -62,	197 }, 
	{ -63,	177 }, 
	{ -64,	157 }, 
	{ -65,	139 }, 
	{ -66,	126 }, 
	{ -67,	112 }, 
	{ -68,	100 }, 
	{ -69,	 90 }, 
	{ -70,	 80 }, 
	{ -71,	 71 }, 
	{ -72,	 65 }, 
	{ -73,	 57 }, 
	{ -74,	 51 }, 
	{ -75,	 45 }, 
	{ -76,	 41 }, 
	{ -77,	 37 }, 
	{ -78,	 33 }, 
	{ -79,	 29 }, 
	{ -80,	 27 }, 
	{-100,	 -1 }
};

/* AGC2 Look-Up table for spectrum scans */
static const struct stv0900_point stv0900_agc2_offset[] = {
	{  -54,	0xffff	},
	{  -53,	0xf678	},
	{  -52,	0xdbaa	},
	{  -51,	0xc3c7	},
	{  -50,	0xae7c	},
	{  -49,	0x9b83	},
	{  -48,	0x8a99	},
	{  -47,	0x7b87	},
	{  -46,	0x6e18	},
	{  -45,	0x621f	},
	{  -44,	0x5773	},
	{  -43,	0x4df1	},
	{  -42,	0x4577	},
	{  -41,	0x3de9	},
	{  -40,	0x372d	},
	{  -39,	0x312d	},
	{  -38,	0x2bd4	},
	{  -37,	0x2710	},
	{  -36,	0x22d1	},
	{  -35,	0x1f07	},
	{  -34,	0x1ba7	},
	{  -33,	0x18a6	},
	{  -32,	0x15f7	},
	{  -31,	0x1394	},
	{  -30,	0x1173	},
	{  -29,	0x0f8d	},
	{  -28,	0x0ddc	},
	{  -27,	0x0c5a	},
	{  -26,	0x0b02	},
	{  -25,	0x09d0	},
	{  -24,	0x08bf	},
	{  -23,	0x07cb	},
	{  -22,	0x06f2	},
	{  -21,	0x0631	},
	{  -20,	0x0585	},
	{  -19,	0x04eb	},
	{  -18,	0x0462	},
	{  -17,	0x03e8	},
	{  -16,	0x037b	},
	{  -15,	0x031a	},
	{  -14,	0x02c4	},
	{  -13,	0x0277	},
	{  -12,	0x0232	},
	{  -11,	0x01f5	},
	{  -10,	0x01bf	},
	{   -9,	0x018e	},
	{   -8,	0x0163	},
	{   -7,	0x013c	},
	{   -6,	0x011a	},
	{   -5,	0x00fb	},
	{   -4,	0x00e0	},
	{   -3,	0x00c8	},
	{   -2,	0x00b2	},
	{   -1,	0x009e	},
	{    0,	0x008d	},
	{    1,	0x007e	},
	{    2,	0x0070	},
	{    3,	0x0064	},
	{    4,	0x0059	},
	{    5,	0x004f	},
	{    6,	0x0047	},
	{    7,	0x003f	},
	{    8,	0x0038	},
	{    9,	0x0032	},
	{   10,	0x002d	},
	{   11,	0x0028	},
	{   12,	0x0023	},
	{   13,	0x0020	},
	{   14,	0x001c	},
	{   15,	0x0019	},
	{   16,	0x0016	},
	{   17,	0x0014	},
	{   18,	0x0012	},
	{   19,	0x0010	},
	{   20,	0x000e	},
	{   21,	0x000d	},
	{   22,	0x000b	},
	{   23,	0x000a	},
	{   30,     -1	}
};

struct stv0900_car_loop_optim {
	enum stv0900_modcode modcode;
	u8 car_loop_pilots_on_2;
	u8 car_loop_pilots_off_2;
	u8 car_loop_pilots_on_5;
	u8 car_loop_pilots_off_5;
	u8 car_loop_pilots_on_10;
	u8 car_loop_pilots_off_10;
	u8 car_loop_pilots_on_20;
	u8 car_loop_pilots_off_20;
	u8 car_loop_pilots_on_30;
	u8 car_loop_pilots_off_30;

};

struct stv0900_short_frames_car_loop_optim {
	enum stv0900_modulation modulation;
	u8 car_loop_cut12_2;    /* Cut 1.2,   SR<=3msps     */
	u8 car_loop_cut20_2;    /* Cut 2.0,   SR<3msps      */
	u8 car_loop_cut12_5;    /* Cut 1.2,   3<SR<=7msps   */
	u8 car_loop_cut20_5;    /* Cut 2.0,   3<SR<=7msps   */
	u8 car_loop_cut12_10;   /* Cut 1.2,   7<SR<=15msps  */
	u8 car_loop_cut20_10;   /* Cut 2.0,   7<SR<=15msps  */
	u8 car_loop_cut12_20;   /* Cut 1.2,   10<SR<=25msps */
	u8 car_loop_cut20_20;   /* Cut 2.0,   10<SR<=25msps */
	u8 car_loop_cut12_30;   /* Cut 1.2,   25<SR<=45msps */
	u8 car_loop_cut20_30;   /* Cut 2.0,   10<SR<=45msps */

};

struct stv0900_short_frames_car_loop_optim_vs_mod {
	enum stv0900_modulation modulation;
	u8 car_loop_2;	  /* SR<3msps      */
	u8 car_loop_5;	  /* 3<SR<=7msps   */
	u8 car_loop_10;   /* 7<SR<=15msps  */
	u8 car_loop_20;   /* 10<SR<=25msps */
	u8 car_loop_30;   /* 10<SR<=45msps */
};

/* Cut 1.x Tracking carrier loop carrier QPSK 1/2 to 8PSK 9/10 long Frame */
static const struct stv0900_car_loop_optim FE_STV0900_S2CarLoop[14] = {
	/*Modcod		2MPon 	2MPoff	5MPon 	5MPoff	10MPon
				10MPoff	20MPon 	20MPoff	30MPon 	30MPoff */
	{ STV0900_QPSK_12,	0x1C,	0x0D,	0x1B,	0x2C,	0x3A,
				0x1C,	0x2A,	0x3B,	0x2A,	0x1B },
	{ STV0900_QPSK_35,	0x2C,	0x0D,	0x2B,	0x2C,	0x3A,
				0x0C,	0x3A,	0x2B,	0x2A,	0x0B },
	{ STV0900_QPSK_23,	0x2C,	0x0D,	0x2B,	0x2C,	0x0B,
				0x0C,	0x3A,	0x1B,	0x2A,	0x3A },
	{ STV0900_QPSK_34,	0x3C,	0x0D,	0x3B,	0x1C,	0x0B,
				0x3B,	0x3A,	0x0B,	0x2A,	0x3A },
	{ STV0900_QPSK_45,	0x3C,	0x0D,	0x3B,	0x1C,	0x0B,
				0x3B,	0x3A,	0x0B,	0x2A,	0x3A },
	{ STV0900_QPSK_56,	0x0D,	0x0D,	0x3B,	0x1C,	0x0B,
				0x3B,	0x3A,	0x0B,	0x2A,	0x3A },
	{ STV0900_QPSK_89,	0x0D,	0x0D,	0x3B,	0x1C,	0x1B,
				0x3B,	0x3A,	0x0B,	0x2A,	0x3A },
	{ STV0900_QPSK_910,	0x1D,	0x0D,	0x3B,	0x1C,	0x1B,
				0x3B,	0x3A,	0x0B,	0x2A,	0x3A },
	{ STV0900_8PSK_35,	0x29,	0x3B,	0x09,	0x2B,	0x38,
				0x0B,	0x18,	0x1A,	0x08,	0x0A },
	{ STV0900_8PSK_23,	0x0A,	0x3B,	0x29,	0x2B,	0x19,
				0x0B,	0x38,	0x1A,	0x18,	0x0A },
	{ STV0900_8PSK_34,	0x3A,	0x3B,	0x2A,	0x2B,	0x39,
				0x0B,	0x19,	0x1A,	0x38,	0x0A },
	{ STV0900_8PSK_56,	0x1B,	0x3B,	0x0B,	0x2B,	0x1A,
				0x0B,	0x39,	0x1A,	0x19,	0x0A },
	{ STV0900_8PSK_89,	0x3B,	0x3B,	0x0B,	0x2B,	0x2A,
				0x0B,	0x39,	0x1A,	0x29,	0x39 },
	{ STV0900_8PSK_910,	0x3B,	0x3B, 	0x0B,	0x2B, 	0x2A,
				0x0B,	0x39,	0x1A,	0x29,	0x39 }
};


/* Cut 2.0 Tracking carrier loop carrier QPSK 1/2 to 8PSK 9/10 long Frame */
static const struct stv0900_car_loop_optim FE_STV0900_S2CarLoopCut20[14] = {
	/* Modcod		2MPon 	2MPoff	5MPon 	5MPoff	10MPon
				10MPoff	20MPon 	20MPoff	30MPon 	30MPoff */
	{ STV0900_QPSK_12,	0x1F,	0x3F,	0x1E,	0x3F,	0x3D,
				0x1F,	0x3D,	0x3E,	0x3D,	0x1E },
	{ STV0900_QPSK_35,	0x2F,	0x3F,	0x2E,	0x2F,	0x3D,
				0x0F,	0x0E,	0x2E,	0x3D,	0x0E },
	{ STV0900_QPSK_23,	0x2F,	0x3F,	0x2E,	0x2F,	0x0E,
				0x0F,	0x0E,	0x1E,	0x3D,	0x3D },
	{ STV0900_QPSK_34,	0x3F,	0x3F,	0x3E,	0x1F,	0x0E,
				0x3E,	0x0E,	0x1E,	0x3D,	0x3D },
	{ STV0900_QPSK_45,	0x3F,	0x3F,	0x3E,	0x1F,	0x0E,
				0x3E,	0x0E,	0x1E,	0x3D,	0x3D },
	{ STV0900_QPSK_56,	0x3F,	0x3F,	0x3E,	0x1F,	0x0E,
				0x3E,	0x0E,	0x1E,	0x3D,	0x3D },
	{ STV0900_QPSK_89,	0x3F,	0x3F,	0x3E,	0x1F,	0x1E,
				0x3E,	0x0E,	0x1E,	0x3D,	0x3D },
	{ STV0900_QPSK_910,	0x3F,	0x3F,	0x3E,	0x1F,	0x1E,
				0x3E,	0x0E,	0x1E,	0x3D,	0x3D },
	{ STV0900_8PSK_35,	0x3c,	0x0c,	0x1c,	0x3b,	0x0c,
				0x3b,	0x2b,	0x2b,	0x1b,	0x2b },
	{ STV0900_8PSK_23,	0x1d,	0x0c,	0x3c,	0x0c,	0x2c,
				0x3b,	0x0c,	0x2b,	0x2b,	0x2b },
	{ STV0900_8PSK_34,	0x0e,	0x1c,	0x3d,	0x0c,	0x0d,
				0x3b,	0x2c,	0x3b,	0x0c,	0x2b },
	{ STV0900_8PSK_56,	0x2e,	0x3e,	0x1e,	0x2e,	0x2d,
				0x1e,	0x3c,	0x2d,	0x2c,	0x1d },
	{ STV0900_8PSK_89,	0x3e,	0x3e,	0x1e,	0x2e,	0x3d,
				0x1e,	0x0d,	0x2d,	0x3c,	0x1d },
	{ STV0900_8PSK_910,	0x3e,	0x3e, 	0x1e,	0x2e, 	0x3d,
				0x1e,	0x1d,	0x2d,	0x0d,	0x1d },
};



/* Cut 2.0 Tracking carrier loop carrier 16APSK 2/3 to 32APSK 9/10 long Frame */
static const struct stv0900_car_loop_optim FE_STV0900_S2APSKCarLoopCut20[11] = {
	/* Modcod		2MPon 	2MPoff	5MPon 	5MPoff	10MPon
				10MPoff	20MPon 	20MPoff	30MPon 	30MPoff */
	{ STV0900_16APSK_23,	0x0C,	0x0C,	0x0C,	0x0C,	0x1D,
				0x0C,	0x3C,	0x0C,	0x2C,	0x0C },
	{ STV0900_16APSK_34,	0x0C,	0x0C,	0x0C,	0x0C,	0x0E,
				0x0C,	0x2D,	0x0C,	0x1D,	0x0C },
	{ STV0900_16APSK_45,	0x0C,	0x0C,	0x0C,	0x0C,	0x1E,
				0x0C,	0x3D,	0x0C,	0x2D,	0x0C },
	{ STV0900_16APSK_56,	0x0C,	0x0C,	0x0C,	0x0C,	0x1E,
				0x0C,	0x3D,	0x0C,	0x2D,	0x0C },
	{ STV0900_16APSK_89,	0x0C,	0x0C,	0x0C,	0x0C,	0x2E,
				0x0C,	0x0E,	0x0C,	0x3D,	0x0C },
	{ STV0900_16APSK_910,	0x0C,	0x0C,	0x0C,	0x0C,	0x2E,
				0x0C,	0x0E,	0x0C,	0x3D,	0x0C },
	{ STV0900_32APSK_34,	0x0C,	0x0C,	0x0C,	0x0C,	0x0C,
				0x0C,	0x0C,	0x0C,	0x0C,	0x0C },
	{ STV0900_32APSK_45,	0x0C,	0x0C,	0x0C,	0x0C,	0x0C,
				0x0C,	0x0C,	0x0C,	0x0C,	0x0C },
	{ STV0900_32APSK_56,	0x0C,	0x0C,	0x0C,	0x0C,	0x0C,
				0x0C,	0x0C,	0x0C,	0x0C,	0x0C },
	{ STV0900_32APSK_89,	0x0C,	0x0C,	0x0C,	0x0C,	0x0C,
				0x0C,	0x0C,	0x0C,	0x0C,	0x0C },
	{ STV0900_32APSK_910,	0x0C,	0x0C,	0x0C,	0x0C,	0x0C,
				0x0C,	0x0C,	0x0C,	0x0C,	0x0C },
};


/* Cut 2.0 Tracking carrier loop carrier QPSK 1/4 to QPSK 2/5 long Frame */
static const struct stv0900_car_loop_optim FE_STV0900_S2LowQPCarLoopCut20[3] = {
	/* Modcod		2MPon 	2MPoff	5MPon 	5MPoff	10MPon
				10MPoff	20MPon 	20MPoff	30MPon 	30MPoff */
	{ STV0900_QPSK_14,	0x0F,	0x3F,	0x0E,	0x3F,	0x2D,
				0x2F,	0x2D,	0x1F,	0x3D,	0x3E },
	{ STV0900_QPSK_13,	0x0F,	0x3F,	0x0E,	0x3F,	0x2D,
				0x2F,	0x3D,	0x0F,	0x3D,	0x2E },
	{ STV0900_QPSK_25,	0x1F,	0x3F,	0x1E,	0x3F,	0x3D,
				0x1F,	0x3D,	0x3E,	0x3D,	0x2E }
};


/* Cut 2.0 Tracking carrier loop carrier  short Frame, cut 1.2 and 2.0 */
static const
struct stv0900_short_frames_car_loop_optim FE_STV0900_S2ShortCarLoop[4] = {
	/*Mod		2Mcut1.2 2Mcut2.0 5Mcut1.2 5Mcut2.0 10Mcut1.2
			10Mcut2.0 20Mcut1.2 20M_cut2.0 30Mcut1.2 30Mcut2.0*/
	{ STV0900_QPSK,		0x3C,	0x2F,	0x2B,	0x2E,	0x0B,
				0x0E,	0x3A,	0x0E,	0x2A,	0x3D },
	{ STV0900_8PSK,		0x0B,	0x3E,	0x2A,	0x0E,	0x0A,
				0x2D,	0x19,	0x0D,	0x09,	0x3C },
	{ STV0900_16APSK,	0x1B,	0x1E,	0x1B,	0x1E,	0x1B,
				0x1E,	0x3A,	0x3D,	0x2A,	0x2D },
	{ STV0900_32APSK,	0x1B,	0x1E,	0x1B,	0x1E,	0x1B,
				0x1E,	0x3A,	0x3D,	0x2A,	0x2D }
};

static	const struct stv0900_car_loop_optim FE_STV0900_S2CarLoopCut30[14] = {
	/*Modcod		2MPon 	2MPoff	5MPon 	5MPoff	10MPon
				10MPoff	20MPon 	20MPoff	30MPon 	30MPoff	*/
	{ STV0900_QPSK_12,	0x3C,	0x2C,	0x0C,	0x2C,	0x1B,
				0x2C,	0x1B,	0x1C,	0x0B, 	0x3B },
	{ STV0900_QPSK_35,	0x0D,	0x0D,	0x0C,	0x0D,	0x1B,
				0x3C,	0x1B,	0x1C,	0x0B,	0x3B },
	{ STV0900_QPSK_23,	0x1D,	0x0D,	0x0C,	0x1D,	0x2B,
				0x3C,	0x1B,	0x1C,	0x0B,	0x3B },
	{ STV0900_QPSK_34,	0x1D,	0x1D,	0x0C,	0x1D,	0x2B,
				0x3C,	0x1B,	0x1C,	0x0B,	0x3B },
	{ STV0900_QPSK_45,	0x2D,	0x1D,	0x1C,	0x1D,	0x2B,
				0x3C,	0x2B,	0x0C,	0x1B,	0x3B },
	{ STV0900_QPSK_56,	0x2D,	0x1D,	0x1C,	0x1D,	0x2B,
				0x3C,	0x2B,	0x0C,	0x1B,	0x3B },
	{ STV0900_QPSK_89,	0x3D,	0x2D,	0x1C,	0x1D,	0x3B,
				0x3C,	0x2B,	0x0C,	0x1B,	0x3B },
	{ STV0900_QPSK_910,	0x3D,	0x2D,	0x1C,	0x1D,	0x3B,
				0x3C,	0x2B,	0x0C,	0x1B,	0x3B },
	{ STV0900_8PSK_35,	0x39,	0x19,	0x39,	0x19,	0x19,
				0x19,	0x19,	0x19,	0x09,	0x19 },
	{ STV0900_8PSK_23,	0x2A,	0x39,	0x1A,	0x0A,	0x39,
				0x0A,	0x29,	0x39,	0x29,	0x0A },
	{ STV0900_8PSK_34,	0x0B,	0x3A,	0x0B,	0x0B,	0x3A,
				0x1B,	0x1A,	0x0B,	0x1A,	0x3A },
	{ STV0900_8PSK_56,	0x0C,	0x1B,	0x3B,	0x2B,	0x1B,
				0x3B,	0x3A,	0x3B,	0x3A,	0x1B },
	{ STV0900_8PSK_89,	0x2C,	0x2C,	0x2C,	0x1C,	0x2B,
				0x0C,	0x0B,	0x3B,	0x0B,	0x1B },
	{ STV0900_8PSK_910,	0x2C,	0x3C,	0x2C,	0x1C,	0x3B,
				0x1C,	0x0B,	0x3B,	0x0B,	0x1B }
};

static	const
struct stv0900_car_loop_optim FE_STV0900_S2APSKCarLoopCut30[11] = {
	/*Modcod		2MPon 	2MPoff	5MPon 	5MPoff	10MPon
				10MPoff	20MPon 	20MPoff	30MPon 	30MPoff	*/
	{ STV0900_16APSK_23,	0x0A,	0x0A,	0x0A,	0x0A,	0x1A,
				0x0A,	0x3A,	0x0A,	0x2A,	0x0A },
	{ STV0900_16APSK_34,	0x0A,	0x0A,	0x0A,	0x0A,	0x0B,
				0x0A,	0x3B,	0x0A,	0x1B,	0x0A },
	{ STV0900_16APSK_45,	0x0A,	0x0A,	0x0A,	0x0A,	0x1B,
				0x0A,	0x3B,	0x0A,	0x2B,	0x0A },
	{ STV0900_16APSK_56,	0x0A,	0x0A,	0x0A,	0x0A,	0x1B,
				0x0A,	0x3B,	0x0A,	0x2B,	0x0A },
	{ STV0900_16APSK_89,	0x0A,	0x0A,	0x0A,	0x0A,	0x2B,
				0x0A,	0x0C,	0x0A,	0x3B,	0x0A },
	{ STV0900_16APSK_910,	0x0A,	0x0A,	0x0A,	0x0A,	0x2B,
				0x0A,	0x0C,	0x0A,	0x3B,	0x0A },
	{ STV0900_32APSK_34,	0x0A,	0x0A,	0x0A,	0x0A,	0x0A,
				0x0A,	0x0A,	0x0A,	0x0A,	0x0A },
	{ STV0900_32APSK_45,	0x0A,	0x0A,	0x0A,	0x0A,	0x0A,
				0x0A,	0x0A,	0x0A,	0x0A,	0x0A },
	{ STV0900_32APSK_56,	0x0A,	0x0A,	0x0A,	0x0A,	0x0A,
				0x0A,	0x0A,	0x0A,	0x0A,	0x0A },
	{ STV0900_32APSK_89,	0x0A,	0x0A,	0x0A,	0x0A,	0x0A,
				0x0A,	0x0A,	0x0A,	0x0A,	0x0A },
	{ STV0900_32APSK_910,	0x0A,	0x0A,	0x0A,	0x0A,	0x0A,
				0x0A,	0x0A,	0x0A,	0x0A,	0x0A }
};

static	const
struct stv0900_car_loop_optim FE_STV0900_S2LowQPCarLoopCut30[3] = {
	/*Modcod		2MPon 	2MPoff	5MPon 	5MPoff	10MPon
				10MPoff	20MPon 	20MPoff	30MPon 	30MPoff*/
	{ STV0900_QPSK_14,	0x0C,	0x3C,	0x0B,	0x3C,	0x2A,
				0x2C,	0x2A,	0x1C,	0x3A,	0x3B },
	{ STV0900_QPSK_13,	0x0C,	0x3C,	0x0B,	0x3C,	0x2A,
				0x2C,	0x3A,	0x0C,	0x3A,	0x2B },
	{ STV0900_QPSK_25,	0x1C,	0x3C,	0x1B,	0x3C,	0x3A,
				0x1C,	0x3A,	0x3B,	0x3A,	0x2B }
};

static	const struct stv0900_short_frames_car_loop_optim_vs_mod
FE_STV0900_S2ShortCarLoopCut30[4] = {
	/*Mod		2Mcut3.0 5Mcut3.0 10Mcut3.0 20Mcut3.0 30Mcut3.0*/
	{ STV0900_QPSK,		0x2C,	0x2B,	0x0B,	0x0B,	0x3A },
	{ STV0900_8PSK,		0x3B,	0x0B,	0x2A,	0x0A,	0x39 },
	{ STV0900_16APSK,	0x1B,	0x1B,	0x1B,	0x3A,	0x2A },
	{ STV0900_32APSK,	0x1B,	0x1B,	0x1B,	0x3A,	0x2A },

};

static const u16 STV0900_InitVal[181][2] = {
	{ R0900_OUTCFG		, 0x00	},
	{ R0900_AGCRF1CFG	, 0x11	},
	{ R0900_AGCRF2CFG	, 0x13	},
	{ R0900_TSGENERAL1X	, 0x14	},
	{ R0900_TSTTNR2		, 0x21	},
	{ R0900_TSTTNR4		, 0x21	},
	{ R0900_P2_DISTXCTL	, 0x22	},
	{ R0900_P2_F22TX	, 0xc0	},
	{ R0900_P2_F22RX	, 0xc0	},
	{ R0900_P2_DISRXCTL	, 0x00	},
	{ R0900_P2_TNRSTEPS	, 0x87	},
	{ R0900_P2_TNRGAIN	, 0x09	},
	{ R0900_P2_DMDCFGMD	, 0xF9	},
	{ R0900_P2_DEMOD	, 0x08	},
	{ R0900_P2_DMDCFG3	, 0xc4	},
	{ R0900_P2_CARFREQ	, 0xed	},
	{ R0900_P2_TNRCFG2	, 0x02	},
	{ R0900_P2_TNRCFG3	, 0x02	},
	{ R0900_P2_LDT		, 0xd0	},
	{ R0900_P2_LDT2		, 0xb8	},
	{ R0900_P2_TMGCFG	, 0xd2	},
	{ R0900_P2_TMGTHRISE	, 0x20	},
	{ R0900_P2_TMGTHFALL	, 0x00	},
	{ R0900_P2_FECSPY	, 0x88	},
	{ R0900_P2_FSPYDATA	, 0x3a	},
	{ R0900_P2_FBERCPT4	, 0x00	},
	{ R0900_P2_FSPYBER	, 0x10	},
	{ R0900_P2_ERRCTRL1	, 0x35	},
	{ R0900_P2_ERRCTRL2	, 0xc1	},
	{ R0900_P2_CFRICFG	, 0xf8	},
	{ R0900_P2_NOSCFG	, 0x1c	},
	{ R0900_P2_DMDT0M	, 0x20	},
	{ R0900_P2_CORRELMANT	, 0x70	},
	{ R0900_P2_CORRELABS	, 0x88	},
	{ R0900_P2_AGC2O	, 0x5b	},
	{ R0900_P2_AGC2REF	, 0x38	},
	{ R0900_P2_CARCFG	, 0xe4	},
	{ R0900_P2_ACLC		, 0x1A	},
	{ R0900_P2_BCLC		, 0x09	},
	{ R0900_P2_CARHDR	, 0x08	},
	{ R0900_P2_KREFTMG	, 0xc1	},
	{ R0900_P2_SFRUPRATIO	, 0xf0	},
	{ R0900_P2_SFRLOWRATIO	, 0x70	},
	{ R0900_P2_SFRSTEP	, 0x58	},
	{ R0900_P2_TMGCFG2	, 0x01	},
	{ R0900_P2_CAR2CFG	, 0x26	},
	{ R0900_P2_BCLC2S2Q	, 0x86	},
	{ R0900_P2_BCLC2S28	, 0x86	},
	{ R0900_P2_SMAPCOEF7	, 0x77	},
	{ R0900_P2_SMAPCOEF6	, 0x85	},
	{ R0900_P2_SMAPCOEF5	, 0x77	},
	{ R0900_P2_TSCFGL	, 0x20	},
	{ R0900_P2_DMDCFG2	, 0x3b	},
	{ R0900_P2_MODCODLST0	, 0xff	},
	{ R0900_P2_MODCODLST1	, 0xff	},
	{ R0900_P2_MODCODLST2	, 0xff	},
	{ R0900_P2_MODCODLST3	, 0xff	},
	{ R0900_P2_MODCODLST4	, 0xff	},
	{ R0900_P2_MODCODLST5	, 0xff	},
	{ R0900_P2_MODCODLST6	, 0xff	},
	{ R0900_P2_MODCODLST7	, 0xcc	},
	{ R0900_P2_MODCODLST8	, 0xcc	},
	{ R0900_P2_MODCODLST9	, 0xcc	},
	{ R0900_P2_MODCODLSTA	, 0xcc	},
	{ R0900_P2_MODCODLSTB	, 0xcc	},
	{ R0900_P2_MODCODLSTC	, 0xcc	},
	{ R0900_P2_MODCODLSTD	, 0xcc	},
	{ R0900_P2_MODCODLSTE	, 0xcc	},
	{ R0900_P2_MODCODLSTF	, 0xcf	},
	{ R0900_P1_DISTXCTL	, 0x22	},
	{ R0900_P1_F22TX	, 0xc0	},
	{ R0900_P1_F22RX	, 0xc0	},
	{ R0900_P1_DISRXCTL	, 0x00	},
	{ R0900_P1_TNRSTEPS	, 0x87	},
	{ R0900_P1_TNRGAIN	, 0x09	},
	{ R0900_P1_DMDCFGMD	, 0xf9	},
	{ R0900_P1_DEMOD	, 0x08	},
	{ R0900_P1_DMDCFG3	, 0xc4	},
	{ R0900_P1_DMDT0M	, 0x20	},
	{ R0900_P1_CARFREQ	, 0xed	},
	{ R0900_P1_TNRCFG2	, 0x82	},
	{ R0900_P1_TNRCFG3	, 0x02	},
	{ R0900_P1_LDT		, 0xd0	},
	{ R0900_P1_LDT2		, 0xb8	},
	{ R0900_P1_TMGCFG	, 0xd2	},
	{ R0900_P1_TMGTHRISE	, 0x20	},
	{ R0900_P1_TMGTHFALL	, 0x00	},
	{ R0900_P1_SFRUPRATIO	, 0xf0	},
	{ R0900_P1_SFRLOWRATIO	, 0x70	},
	{ R0900_P1_TSCFGL	, 0x20	},
	{ R0900_P1_FECSPY	, 0x88	},
	{ R0900_P1_FSPYDATA	, 0x3a	},
	{ R0900_P1_FBERCPT4	, 0x00	},
	{ R0900_P1_FSPYBER	, 0x10	},
	{ R0900_P1_ERRCTRL1	, 0x35	},
	{ R0900_P1_ERRCTRL2	, 0xc1	},
	{ R0900_P1_CFRICFG	, 0xf8	},
	{ R0900_P1_NOSCFG	, 0x1c	},
	{ R0900_P1_CORRELMANT	, 0x70	},
	{ R0900_P1_CORRELABS	, 0x88	},
	{ R0900_P1_AGC2O	, 0x5b	},
	{ R0900_P1_AGC2REF	, 0x38	},
	{ R0900_P1_CARCFG	, 0xe4	},
	{ R0900_P1_ACLC		, 0x1A	},
	{ R0900_P1_BCLC		, 0x09	},
	{ R0900_P1_CARHDR	, 0x08	},
	{ R0900_P1_KREFTMG	, 0xc1	},
	{ R0900_P1_SFRSTEP	, 0x58	},
	{ R0900_P1_TMGCFG2	, 0x01	},
	{ R0900_P1_CAR2CFG	, 0x26	},
	{ R0900_P1_BCLC2S2Q	, 0x86	},
	{ R0900_P1_BCLC2S28	, 0x86	},
	{ R0900_P1_SMAPCOEF7	, 0x77	},
	{ R0900_P1_SMAPCOEF6	, 0x85	},
	{ R0900_P1_SMAPCOEF5	, 0x77	},
	{ R0900_P1_DMDCFG2	, 0x3b	},
	{ R0900_P1_MODCODLST0	, 0xff	},
	{ R0900_P1_MODCODLST1	, 0xff	},
	{ R0900_P1_MODCODLST2	, 0xff	},
	{ R0900_P1_MODCODLST3	, 0xff	},
	{ R0900_P1_MODCODLST4	, 0xff	},
	{ R0900_P1_MODCODLST5	, 0xff	},
	{ R0900_P1_MODCODLST6	, 0xff	},
	{ R0900_P1_MODCODLST7	, 0xcc	},
	{ R0900_P1_MODCODLST8	, 0xcc	},
	{ R0900_P1_MODCODLST9	, 0xcc	},
	{ R0900_P1_MODCODLSTA	, 0xcc	},
	{ R0900_P1_MODCODLSTB	, 0xcc	},
	{ R0900_P1_MODCODLSTC	, 0xcc	},
	{ R0900_P1_MODCODLSTD	, 0xcc	},
	{ R0900_P1_MODCODLSTE	, 0xcc	},
	{ R0900_P1_MODCODLSTF	, 0xcf	},
	{ R0900_GENCFG		, 0x1d	},
	{ R0900_NBITER_NF4	, 0x37	},
	{ R0900_NBITER_NF5	, 0x29	},
	{ R0900_NBITER_NF6	, 0x37	},
	{ R0900_NBITER_NF7	, 0x33	},
	{ R0900_NBITER_NF8	, 0x31	},
	{ R0900_NBITER_NF9	, 0x2f	},
	{ R0900_NBITER_NF10	, 0x39	},
	{ R0900_NBITER_NF11	, 0x3a	},
	{ R0900_NBITER_NF12	, 0x29	},
	{ R0900_NBITER_NF13	, 0x37	},
	{ R0900_NBITER_NF14	, 0x33	},
	{ R0900_NBITER_NF15	, 0x2f	},
	{ R0900_NBITER_NF16	, 0x39	},
	{ R0900_NBITER_NF17	, 0x3a	},
	{ R0900_NBITERNOERR	, 0x04	},
	{ R0900_GAINLLR_NF4	, 0x0C	},
	{ R0900_GAINLLR_NF5	, 0x0F	},
	{ R0900_GAINLLR_NF6	, 0x11	},
	{ R0900_GAINLLR_NF7	, 0x14	},
	{ R0900_GAINLLR_NF8	, 0x17	},
	{ R0900_GAINLLR_NF9	, 0x19	},
	{ R0900_GAINLLR_NF10	, 0x20	},
	{ R0900_GAINLLR_NF11	, 0x21	},
	{ R0900_GAINLLR_NF12	, 0x0D	},
	{ R0900_GAINLLR_NF13	, 0x0F	},
	{ R0900_GAINLLR_NF14	, 0x13	},
	{ R0900_GAINLLR_NF15	, 0x1A	},
	{ R0900_GAINLLR_NF16	, 0x1F	},
	{ R0900_GAINLLR_NF17	, 0x21	},
	{ R0900_RCCFG2		, 0x20	},
	{ R0900_P1_FECM		, 0x01	}, /*disable DSS modes*/
	{ R0900_P2_FECM		, 0x01	}, /*disable DSS modes*/
	{ R0900_P1_PRVIT	, 0x2F	}, /*disable puncture rate 6/7*/
	{ R0900_P2_PRVIT	, 0x2F	}, /*disable puncture rate 6/7*/
	{ R0900_STROUT1CFG	, 0x4c	},
	{ R0900_STROUT2CFG	, 0x4c	},
	{ R0900_CLKOUT1CFG	, 0x50	},
	{ R0900_CLKOUT2CFG	, 0x50	},
	{ R0900_DPN1CFG		, 0x4a	},
	{ R0900_DPN2CFG		, 0x4a	},
	{ R0900_DATA71CFG	, 0x52	},
	{ R0900_DATA72CFG	, 0x52	},
	{ R0900_P1_TSCFGM	, 0x00	},
	{ R0900_P2_TSCFGM	, 0x00	},
	{ R0900_P1_TSCFGH	, 0xe0	}, /* DVB-CI timings */
	{ R0900_P2_TSCFGH	, 0xe0	}, /* DVB-CI timings */
	{ R0900_P1_TSSPEED	, 0x40	},
	{ R0900_P2_TSSPEED	, 0x40	},
};

static const u16 STV0900_Cut20_AddOnVal[32][2] = {
	{ R0900_P2_DMDCFG3	, 0xe8	},
	{ R0900_P2_DMDCFG4	, 0x10	},
	{ R0900_P2_CARFREQ	, 0x38	},
	{ R0900_P2_CARHDR	, 0x20	},
	{ R0900_P2_KREFTMG	, 0x5a	},
	{ R0900_P2_SMAPCOEF7	, 0x06	},
	{ R0900_P2_SMAPCOEF6	, 0x00	},
	{ R0900_P2_SMAPCOEF5	, 0x04	},
	{ R0900_P2_NOSCFG	, 0x0c	},
	{ R0900_P1_DMDCFG3	, 0xe8	},
	{ R0900_P1_DMDCFG4	, 0x10	},
	{ R0900_P1_CARFREQ	, 0x38	},
	{ R0900_P1_CARHDR	, 0x20	},
	{ R0900_P1_KREFTMG	, 0x5a	},
	{ R0900_P1_SMAPCOEF7	, 0x06	},
	{ R0900_P1_SMAPCOEF6	, 0x00	},
	{ R0900_P1_SMAPCOEF5	, 0x04	},
	{ R0900_P1_NOSCFG	, 0x0c	},
	{ R0900_GAINLLR_NF4	, 0x21	},
	{ R0900_GAINLLR_NF5	, 0x21	},
	{ R0900_GAINLLR_NF6	, 0x20	},
	{ R0900_GAINLLR_NF7	, 0x1F	},
	{ R0900_GAINLLR_NF8	, 0x1E	},
	{ R0900_GAINLLR_NF9	, 0x1E	},
	{ R0900_GAINLLR_NF10	, 0x1D	},
	{ R0900_GAINLLR_NF11	, 0x1B	},
	{ R0900_GAINLLR_NF12	, 0x20	},
	{ R0900_GAINLLR_NF13	, 0x20	},
	{ R0900_GAINLLR_NF14	, 0x20	},
	{ R0900_GAINLLR_NF15	, 0x20	},
	{ R0900_GAINLLR_NF16	, 0x20	},
	{ R0900_GAINLLR_NF17	, 0x21	}

};

#endif

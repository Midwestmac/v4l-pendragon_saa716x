/*
 * TBS PCIe card stubs for closed source components
 *
 * Copyright (C) 2016 Pendragon Sound
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __TBSPCIESTUB__
#define __TBSPCIESTUB__

int tbsdvbctl1(struct tbs_pcie_dev *dev, int a)
{
	printk(KERN_ERR "tbsdvbctl1 is disabled\n");
	return -1;
}

int tbsdvbctl2(struct tbs_pcie_dev *dev, int a, int b)
{
	printk(KERN_ERR "tbsdvbctl2 is disabled\n");
	return -1;
}

#endif